/*$(window).load(function() { 
	$('#status').fadeOut(); 
	$('#preloader').delay(350).fadeOut('slow');
	$('body').delay(350).css({'overflow':'visible'});
})*/

$(window).load(function() {
    // Animate loader off screen
    $('.page-loader').fadeOut('slow');
});

$('a.open_close').on("click",function() {
	$('.main-menu').toggleClass('show');
	$('.layer').toggleClass('layer-is-visible');
});
$('a.show-submenu').on("click",function() {
	$(this).next().toggleClass("show_normal");
});
$('a.show-submenu-mega').on("click",function() {
	$(this).next().toggleClass("show_mega");
});
if($(window).width() <= 480){
	$('a.open_close').on("click",function() {
	$('.cmn-toggle-switch').removeClass('active')
});
}

$('.tooltip-1').tooltip({html:true});
		
function toggleChevron(e) {
    $(e.target)
        .prev('.panel-heading')
        .find("i.indicator")
        .toggleClass('icon-angle-circled-up icon-angle-circled-down');
}
$('#accordion').on('hidden.bs.collapse shown.bs.collapse', toggleChevron);


new WOW().init();


$(function () {
'use strict';
$('.video').magnificPopup({type:'iframe'});	


$('.magnific-gallery').each(function() {
    $(this).magnificPopup({
        delegate: 'a', 
        type: 'image',
        gallery:{enabled:true}
    });
}); 

var toggles = document.querySelectorAll(".cmn-toggle-switch"); 

  for (var i = toggles.length - 1; i >= 0; i--) {
    var toggle = toggles[i];
    toggleHandler(toggle);
  }
    function toggleHandler(toggle) {
    toggle.addEventListener( "click", function(e) {
      e.preventDefault();
      (this.classList.contains("active") === true) ? this.classList.remove("active") : this.classList.add("active");
    });
  }
    $(window).scroll(function() {
		if($(this).scrollTop() != 0) {
			$('#toTop').fadeIn();	
		} else {
			$('#toTop').fadeOut();
		}
	});
	$('#toTop').on("click",function() {
		$('body,html').animate({scrollTop:0},500);
	});	
});

	
$(document).ready(function() {
  $('#quote-carousel').carousel({
    pause: true,
    interval: 6000
  });
});


$(document).ready(function() {
	$('input.date-pick-1').datepicker();
	$('input.date-pick').datepicker({startDate: "0d", todayHighlight: true});
});


$(function(){		
	$("#check_avail").validate({			
		rules:{
			session_date:{required: true,date: true},
			session_time:{required: true},
			ticket_qty:{required: true,digits: true}
		},			  
		messages:{
			session_date:{required: 'Please select your session date'},
			session_time:{required: 'Please select your session time'},
			ticket_qty:{required: 'Please enter number of tickets'}
		},
		errorPlacement: function(error, element){			
			error.insertAfter(element.parent());
		}
	});
	$("#get_direction").validate({			
		rules:{
			saddr:{required: true},
		},			  
		messages:{
			saddr:{required: 'Please enter your location area'}
		},
		errorPlacement: function(error, element){			
			error.insertAfter(element.parent());
		}
	});
});

