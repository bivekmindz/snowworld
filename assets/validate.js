// Jquery validate form contact
/*jQuery(document).ready(function()
{
	$('#contactform').submit(function()
	{
		'use strict';
		var action = $(this).attr('action');

		$("#message-contact").slideUp(750,function() {
		$('#message-contact').hide();

 		$('#submit-contact')
			.after('<i class="icon-spin4 animate-spin loader"></i>')
			.attr('disabled','disabled');
			
		$.post(action, {
			name_contact: $('#name_contact').val(),
			lastname_contact: $('#lastname_contact').val(),
			email_contact: $('#email_contact').val(),
			phone_contact: $('#phone_contact').val(),
			message_contact: $('#message_contact').val(),
			verify_contact: $('#verify_contact').val()
		},
		function(data){
			  document.getElementById('message-contact').innerHTML = data;
			  $('#message-contact').slideDown('slow');
			  $('#contactform .loader').fadeOut('slow',function(){$(this).remove()});
			  $('#submit-contact').removeAttr('disabled');
			  if(data.match('success') != null) $('#contactform').slideUp('slow');
		}
		);

		});

		return false;
	});
});*/


$(document).ready(function() 
{
/* ==============================================
Birthday FORM
=============================================== */	
   $('#birthday_form').submit( function( event )
   {
		event.preventDefault(); 
	 	var errorMessage="";
    	$(".error-div").html("");
		$(".success-div").html("");					
		
		var filter = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		var email = document.getElementById('email_address');
		
        if (document.bform.first_name.value === ""){
        	errorMessage+="Please enter your first name <br/>";
		}
		
		if (document.bform.last_name.value === ""){
        	errorMessage+="Please enter your last name <br/>";
		}
		       
        if (document.bform.mobile_number.value === ""){
        	errorMessage+="Please enter your mobile number <br/>";
		}
		
		if (document.bform.email_address.value === ""){
        	errorMessage+="Please enter your e-mail id <br/>";
		}
		
		if (!filter.test(email.value)) {
        	errorMessage+="Please enter valid e-mail id <br/>";
		}
					
        if (document.bform.session_date.value === ""){
        	errorMessage+="Please select visit date <br/>";
		}
				
        if (document.bform.session_time.selectedIndex===0) {
        	errorMessage+="Please select session time <br/>";
		}
					
        if (document.bform.number_people.value === ""){
        	errorMessage+="Please enter number of people <br/>";
		}
					
        if (document.bform.birthday_name.value === ""){
        	errorMessage+="Please enter the birthday boy/girl name <br/>";
		}
		
		if(errorMessage!==""){
			$(".error-div").html(errorMessage);
			//$('#danger').addClass('msg-visible');	
			$('#danger').show();		
			$("#danger").fadeTo(3000, 600).slideUp(600, function(){
		   		$("#danger").slideUp(600);
			});
		} else {	
			$('#danger').removeClass('msg-visible'); 
		}		
		
		
        if ((document.bform.first_name.value != "") && (document.bform.last_name.value != "") && (document.bform.mobile_number.value != "") &&
			(filter.test(email.value)) && (document.bform.session_date.value != "") && (document.bform.session_time.selectedIndex != 0) && 
			(document.bform.number_people.value != "") && (document.bform.birthday_name.value != "")) 
		{		
			var formInput = $("#birthday_form").serialize();
			$.post($("#birthday_form").attr('action'), formInput, function(data, textStatus)
			{ 						
				$(".success-div").html("Congratulations! Your request is recieved. We will get back to you.");
				//$('#success').addClass('msg-visible');	
				$('#success').show();			
				$("#success").fadeTo(2000, 500).slideUp(500, function(){
					$("#success").slideUp(500);
				});			
				$("#birthday_form")[0].reset();                   
			});	
		}
		/*else 
		{ 
			$(".error-div").html("Sorry! Your request could not be processed. Please try again after some time.");
			$('#danger').addClass('msg-visible');
		}*/		
		return false;	
  	});
/* ==============================================
School FORM
=============================================== */	
	$('#school_form').submit( function( event )
   	{
		event.preventDefault(); 
	 	var errorMessage="";
    	$(".error-div").html("");
		$(".success-div").html("");					
		
		var filter = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		var email = document.getElementById('email_address');
		
        if (document.sform.first_name.value === ""){
        	errorMessage+="Please enter your first name <br/>";
		}
		
		if (document.sform.last_name.value === ""){
        	errorMessage+="Please enter your last name <br/>";
		}
		       
        if (document.sform.mobile_number.value === ""){
        	errorMessage+="Please enter your mobile number <br/>";
		}
		
		if (document.sform.email_address.value === ""){
        	errorMessage+="Please enter your e-mail id <br/>";
		}
		
		if (!filter.test(email.value)) {
        	errorMessage+="Please enter valid e-mail id <br/>";
		}
					
        if (document.sform.session_date.value === ""){
        	errorMessage+="Please select visit date <br/>";
		}
				
        if (document.sform.session_time.selectedIndex===0) {
        	errorMessage+="Please select session time <br/>";
		}
					
        if (document.sform.number_people.value === ""){
        	errorMessage+="Please enter number of people <br/>";
		}
					
        if (document.sform.school_name.value === ""){
        	errorMessage+="Please enter the school name <br/>";
		}
					
        if (document.sform.school_address.value === ""){
        	errorMessage+="Please enter the school address <br/>";
		}
		
		if(errorMessage!==""){
			$(".error-div").html(errorMessage);
			//$('#danger').addClass('msg-visible');
			$('#danger').show();				
			$("#danger").fadeTo(3000, 600).slideUp(600, function(){
		   		$("#danger").slideUp(600);
			});
		} else {	
			$('#danger').removeClass('msg-visible'); 
		}		
		
		
        if ((document.sform.first_name.value != "") && (document.sform.last_name.value != "") && (document.sform.mobile_number.value != "") &&
			(filter.test(email.value)) && (document.sform.session_date.value != "") && (document.sform.session_time.selectedIndex != 0) && 
			(document.sform.number_people.value != "") && (document.sform.school_name.value != "") && (document.sform.school_address.value != "")) 
		{		
			var formInput = $("#school_form").serialize();
			$.post($("#school_form").attr('action'), formInput, function(data, textStatus)
			{ 						
				$(".success-div").html("Congratulations! Your request is recieved. We will get back to you.");
				//$('#success').addClass('msg-visible');
				$('#success').show();					
				$("#success").fadeTo(2000, 500).slideUp(500, function(){
					$("#success").slideUp(500);
				});		
				$("#school_form")[0].reset();                   
			});	
		}	
		return false;	
  	});
/* ==============================================
Corporate FORM
=============================================== */	
	$('#corporate_form').submit( function( event )
   	{		
		event.preventDefault(); 
	 	var errorMessage="";
    	$(".error-div").html("");
		$(".success-div").html("");						
		
		var filter = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		var email = document.getElementById('email_address');
		
        if (document.cform.first_name.value === ""){
        	errorMessage+="Please enter your first name <br/>";
		}
		
		if (document.cform.last_name.value === ""){
        	errorMessage+="Please enter your last name <br/>";
		}
		       
        if (document.cform.mobile_number.value === ""){
        	errorMessage+="Please enter your mobile number <br/>";
		}
		
		if (document.cform.email_address.value === ""){
        	errorMessage+="Please enter your e-mail id <br/>";
		}
		
		if (!filter.test(email.value)) {
        	errorMessage+="Please enter valid e-mail id <br/>";
		}
					
        if (document.cform.session_date.value === ""){
        	errorMessage+="Please select visit date <br/>";
		}
				
        if (document.cform.session_time.selectedIndex===0) {
        	errorMessage+="Please select session time <br/>";
		}
					
        if (document.cform.number_people.value === ""){
        	errorMessage+="Please enter number of people <br/>";
		}
					
        if (document.cform.company_name.value === ""){
        	errorMessage+="Please enter the company name <br/>";
		}
		
		if(errorMessage!==""){
			$(".error-div").html(errorMessage);
			//document.getElementById('danger').style.display="block";
			$('#danger').show();		
			$("#danger").fadeTo(3000, 600).slideUp(600, function(){
		   		$("#danger").slideUp(600);
			});
		} else {	
			$('#danger').removeClass('msg-visible'); 
		}		
		
		
        if ((document.cform.first_name.value != "") && (document.cform.last_name.value != "") && (document.cform.mobile_number.value != "") &&
			(filter.test(email.value)) && (document.cform.session_date.value != "") && (document.cform.session_time.selectedIndex != 0) && 
			(document.cform.number_people.value != "") && (document.cform.company_name.value != "")) 
		{		
			var formInput = $("#corporate_form").serialize();
			$.post($("#corporate_form").attr('action'), formInput, function(data, textStatus)
			{ 						
				$(".success-div").html("Congratulations! Your request is recieved. We will get back to you.");
				//$('#success').addClass('msg-visible');
				$('#success').show();				
				$("#success").fadeTo(2000, 500).slideUp(500, function(){
					$("#success").slideUp(500);
				});		
				$("#corporate_form")[0].reset();                   
			});	
		}	
		return false;	
  	});
/* ==============================================
Contact Us FORM
=============================================== */	
	$('#contact_form').submit( function( event )
   	{
		event.preventDefault(); 
	 	var errorMessage="";
    	$(".error-div").html("");
		$(".success-div").html("");					
		
		var filter = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		var email = document.getElementById('email_contact');
		
        if (document.cont_form.fname_contact.value === ""){
        	errorMessage+="Please enter your first name <br/>";
		}
		
		if (document.cont_form.lname_contact.value === ""){
        	errorMessage+="Please enter your last name <br/>";
		}
		
		if (document.cont_form.email_contact.value === ""){
        	errorMessage+="Please enter your e-mail id <br/>";
		}
		
		if (!filter.test(email.value)) {
        	errorMessage+="Please enter valid e-mail id <br/>";
		}
		       
        if (document.cont_form.phone_contact.value === ""){
        	errorMessage+="Please enter your mobile number <br/>";
		}
					
        if (document.cont_form.message_contact.value === ""){
        	errorMessage+="Write your message <br/>";
		}
		
		if(errorMessage!==""){
			$(".error-div").html(errorMessage);
			//$('#danger').addClass('msg-visible');	
			$('#danger').show();			
			$("#danger").fadeTo(3000, 600).slideUp(600, function(){
		   		$("#danger").slideUp(600);
			}); 
		} else {	
			$('#danger').removeClass('msg-visible'); 
		}		
		
		
        if ((document.cont_form.fname_contact.value != "") && (document.cont_form.lname_contact.value != "") && (filter.test(email.value)) && (document.cont_form.phone_contact.value != "") && (document.cont_form.message_contact.value != "")) 
		{		
			var formInput = $("#contact_form").serialize();
			$.post($("#contact_form").attr('action'), formInput, function(data)
			{ 						
				$(".success-div").html("Congratulations! Your request is recieved. We will get back to you.");
				//$('#success').addClass('msg-visible');
				$('#success').show();					
				$("#success").fadeTo(2000, 500).slideUp(500, function(){
					$("#success").slideUp(500);
				});
				$("#contact_form")[0].reset();                   
			});	
		}	
		return false;	
  	});
/* ==============================================
Career FORM
=============================================== */	
	/*$('#career_form13213').submit( function( event )
   	{
		event.preventDefault(); 
	 	var errorMessage="";
    	$(".error-div").html("");
		$(".success-div").html("");					
		
		var filter = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		var email = document.getElementById('email_address');
		
        if (document.car_form.first_name.value === ""){
        	errorMessage+="Please enter your first name <br/>";
		}
		
		if (document.car_form.last_name.value === ""){
        	errorMessage+="Please enter your last name <br/>";
		}
		       
        if (document.car_form.mobile_number.value === ""){
        	errorMessage+="Please enter your mobile number <br/>";
		}
		
		if (document.car_form.email_address.value === ""){
        	errorMessage+="Please enter your e-mail id <br/>";
		}
		
		if (!filter.test(email.value)) {
        	errorMessage+="Please enter valid e-mail id <br/>";
		}
					
        if (document.car_form.attachmentFile.value === ""){
        	errorMessage+="Choose your resume file <br/>";
		}
		
		if(errorMessage!==""){
			$(".error-div").html(errorMessage);
			$('#danger').addClass('msg-visible');
		} else {	
			$('#danger').removeClass('msg-visible'); 
		}		
		
		
        if ((document.car_form.first_name.value != "") && (document.car_form.last_name.value != "") && (document.car_form.mobile_number.value != "") && 
			(filter.test(email.value)) && (document.car_form.attachmentFile.value != "")) 
		{		
			//var formInput = $(this).serialize();
			var formInput = new FormData("#career_form");

			$.post($(this).attr('action'), formInput, function(data)
			{ 						
				$(".success-div").html("Congratulations! Your request is recieved. We will get back to you.");
				$('#success').addClass('msg-visible');				
				$("#career_form")[0].reset();                   
			});	
		}	
		return false;	
  	});*/
	
	$("#career_form").validate(
	{					
		rules: {
			first_name: { required: true },
			last_name: { required: true },
			mobile_number: { required: true },
			email_address: { required: true, email: true },
			attachmentFile: { required: true, extension: "docx|doc|pdf" }
		},
							
		// Messages for form validation
		messages: {
			first_name: { required: 'Please enter your first name.' },
			last_name: { required: 'Please enter your last name.' },
			email_address: {
				required: 'Please enter your email address.',
				email: 'Please enter a VALID email address.'
			},
			mobile_number: { required: 'Please enter your mobile number.' },
			attachmentFile: { required: 'Please select your resume file.', extension:"select valied input file format" }
		},

		// Ajax form submition
		submitHandler: function(form)
		{
			$(form).ajaxSubmit(
			{
				beforeSend: function() {
					$('#danger').removeClass('msg-visible');
					$('#career_form button[type="submit"]').addClass('button-uploading').attr('disabled', true);
				},
				uploadProgress: function(event, position, total, percentComplete) {
					$("#career_form .progress").text(percentComplete + '%');
				},
				success: function() {					
					$('#danger').removeClass('msg-visible');
					$(".success-div").html("Congratulations! Your request is recieved. We will get back to you.");
					$('#success').addClass('msg-visible');	
					$('#success').show();				
					$("#success").fadeTo(2000, 500).slideUp(500, function(){
						$("#success").slideUp(500);
					});			
					$("#career_form")[0].reset();  
					$('#career_form button[type="submit"]').removeClass('button-uploading').attr('disabled', false);
				}
			});
		},	
		
		// Do not change code below
		errorPlacement: function(error, element)
		{
			if(error.length == 0) {$('#danger').hide();}
			//error.insertAfter(element.parent());
			//$(".error-div").html(errorMessage);					
			error.appendTo('.error-div');			
			$('#danger').addClass('msg-visible');	
		}
	});
});