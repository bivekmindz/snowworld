<?php
//start session

if(!session_id()){
    session_start();
}

include_once 'src/twitteroauth.php';
$consumerKey = 'oLq3yik3afgSHsZvDq4fDaho7';
$consumerSecret = 'tunZ2NWyoRrFOfiPh7W1o8Nou2KKYmweOA0cJRMNWkF5pT3gUh';
$redirectURL = 'http://192.168.1.75/login-with-twitter/';


include_once 'User.php';

//If OAuth token not matched
if(isset($_REQUEST['oauth_token']) && $_SESSION['token'] !== $_REQUEST['oauth_token']){
	//Remove token from session
	unset($_SESSION['token']);
	unset($_SESSION['token_secret']);
}

//If user already verified 
if(isset($_SESSION['status']) && $_SESSION['status'] == 'verified' && !empty($_SESSION['request_vars'])){
	//Retrive variables from session
	$username 		  = $_SESSION['request_vars']['screen_name'];
	$twitterId		  = $_SESSION['request_vars']['user_id'];
	$oauthToken 	  = $_SESSION['request_vars']['oauth_token'];
	$oauthTokenSecret = $_SESSION['request_vars']['oauth_token_secret'];
	$profilePicture	  = $_SESSION['userData']['picture'];
	
	/*
	 * Prepare output to show to the user
	 */
	$twClient = new TwitterOAuth($consumerKey, $consumerSecret, $oauthToken, $oauthTokenSecret);
	
	//If user submits a tweet to post to twitter
	if(isset($_POST["updateme"])){
		$my_update = $twClient->post('statuses/update', array('status' => $_POST["updateme"]));
	}
	
	//Display username and logout link
	$output = '<div class="welcome_txt">Welcome <strong>'.$username.'</strong> (Twitter ID : '.$twitterId.'). <a href="logout.php">Logout</a>!</div>';
	
	//Display profile iamge and tweet form
	
	$output .= '<img src="'.$profilePicture.'" width="120" height="110"/>';
	
	
	
	
}elseif(isset($_REQUEST['oauth_token']) && $_SESSION['token'] == $_REQUEST['oauth_token']){
	//Call Twitter API
	$twClient = new TwitterOAuth($consumerKey, $consumerSecret, $_SESSION['token'] , $_SESSION['token_secret']);
	
	//Get OAuth token
	$access_token = $twClient->getAccessToken($_REQUEST['oauth_verifier']);
	
	//If returns success
	if($twClient->http_code == '200'){
		//Storing access token data into session
		$_SESSION['status'] = 'verified';
		$_SESSION['request_vars'] = $access_token;
		
		//Get user profile data from twitter
		$userInfo = $twClient->get('account/verify_credentials');

		//Initialize User class
		$user = new User();
		
		//Insert or update user data to the database
		$name = explode(" ",$userInfo->name);
		$fname = isset($name[0])?$name[0]:'';
		$lname = isset($name[1])?$name[1]:'';
		$profileLink = 'https://twitter.com/'.$userInfo->screen_name;
		$twUserData = array(
			'oauth_provider'=> 'twitter',
			'oauth_uid'     => $userInfo->id,
			'first_name'    => $fname,
			'last_name'     => $lname,
			'email'         => '',
			'gender'        => '',
			'locale'        => $userInfo->lang,
			'picture'       => $userInfo->profile_image_url,
			'link'          => $profileLink,
			'username'		=> $userInfo->screen_name
		);
		
		$userData = $user->checkUser($twUserData);
		
		//Storing user data into session
		$_SESSION['userData'] = $userData;
		
		//Remove oauth token and secret from session
		unset($_SESSION['token']);
		unset($_SESSION['token_secret']);
		
		//Redirect the user back to the same page
		header('Location: ./');
	}else{
		$output = '<h3 style="color:red">Some problem occurred, please try again.</h3>';
	}
}else{
	//Fresh authentication
	$twClient = new TwitterOAuth($consumerKey, $consumerSecret);
	$request_token = $twClient->getRequestToken($redirectURL);
	
	//Received token info from twitter
	$_SESSION['token']		 = $request_token['oauth_token'];
	$_SESSION['token_secret']= $request_token['oauth_token_secret'];
	
	//If authentication returns success
	if($twClient->http_code == '200'){
		//Get twitter oauth url
		$authUrl = $twClient->getAuthorizeURL($request_token['oauth_token']);
		
		//Display twitter login button
		$output = '<a href="'.filter_var($authUrl, FILTER_SANITIZE_URL).'"><img src="images/sign-in-with-twitter.png" width="151" height="24" border="0" /></a>';
	}else{
		$output = '<h3 style="color:red">Error connecting to twitter! try again later!</h3>';
	}
}
?>

	<!-- Display login button / profile information -->
	<?php echo $output; ?>
