<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js" crossorigin="anonymous"></script>

<?php //pend($vieww[0]); ?>
<!-- jQuery Form Validation code -->
<script>


    $(function() {
        $("#add_mail").validate({
            rules: {
                branchids: "required",
                mail_text1: "required",
                mail_text2: "required",
                mail_text3: "required",
                mail_text4: "required",
                mail_subject: "required",
                mail_from: "required",
                mail_branch_phone: "required",
                mail_branch_email: "required",
               
            },
            messages: {
                com_name: "Please enter name",
            },
            submitHandler: function(form) {
                form.submit();
            }
        });



        $("#update_com").validate({
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                com_name_update: "required",
            },
            // Specify validation error messages
            messages: {
                com_name_update: "Please enter name",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
    });


</script>
<style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
    .tbl_input textarea{        display: -webkit-box !important;}

</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>Mail Details Update</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?>
                                    <?php
                                    if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>


                        <form action="" name="add_mail" id="add_mail" method="post" enctype="multipart/form-data" >


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select Branch <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select name="branchids" required  id="branchids">
                                                    <?php foreach ($s_viewbranch as $key => $value) { ?>
                                                        <option <?php if($value->branch_id == $vieww[0]->bannerimage_branch){
                                                            echo "selected";
                                                        } ?> value="<?php echo $value->branch_id; ?>"><?php echo $value->branch_name; ?></option>
                                                    <?php }?>
                                                </select></div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Select LOGO <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="file" id="mail_image" value="<?= $vieww[0]->bannerimage_logo?>" name="mail_image" ><input type="hidden" name="mail_image_before_update" value="<?= $vieww[0]->bannerimage_logo?>">
                                                <strong>Prevoius Image :</strong>
                                                <img style="height: 50px;" src="<?php echo base_url('assets/admin/images/'.$vieww[0]->bannerimage_logo); ?>" >

                                            </div>
                                        </div>
                                    </div>`
                                </div>
                            </div>





                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Heading 1 <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <textarea  value=""  id="mail_text1" style="display: block; align: left" name="mail_text1" ><?= trim($vieww[0]->bannerimage_top1); ?> </textarea>
                                            </div>
                                        </div>
                                    </div>`
                                </div>


                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Heading 2 <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <textarea  id="mail_text2" style="display: block" name="mail_text2" ><?= trim($vieww[0]->bannerimage_top2); ?></textarea>
                                            </div>
                                        </div>
                                    </div>`
                                </div>


                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Heading 3 <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <textarea  id="mail_text3" style="display: block" name="mail_text3" ><?= trim($vieww[0]->bannerimage_top3); ?></textarea>
                                            </div>
                                        </div>
                                    </div>`
                                </div>

                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Heading 4 <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <textarea  id="mail_text4" style="display: block" name="mail_text4" ><?= trim($vieww[0]->bannerimage_top4); ?></textarea>
                                            </div>
                                        </div>
                                    </div>`
                                </div>
                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Subject <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="mail_subject" value="<?= trim($vieww[0]->bannerimage_subject); ?>" name="mail_subject" >
                                            </div>
                                        </div>
                                    </div>`
                                </div>
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> From <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" value="<?= trim($vieww[0]->bannerimage_from); ?>" id="mail_from" name="mail_from" >
                                            </div>
                                        </div>
                                    </div>`
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Branch phone <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" minlength="10" value="<?= trim($vieww[0]->bannerimage_branch_contact); ?>" id="mail_branch_phone" name="mail_branch_phone" >
                                            </div>
                                        </div>
                                    </div>`
                                </div>
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Branch Email <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="email" value="<?= trim($vieww[0]->bannerimage_branch_email); ?>" id="mail_branch_email" name="mail_branch_email" >
                                            </div>
                                        </div>
                                    </div>`
                                </div>
                            </div>

                            <div class="sep_box" >

                                <div class="col-lg-6" style="">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Branch Email<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">


                                                <input type="text"  minlength="1"  id="mail_branch_apicode" name="mail_branch_apicode"  value="<?= trim($vieww[0]->bannerimage_apikey); ?>" >
                                            </div>
                                        </div>
                                    </div>`
                                </div>



                                <div class="col-lg-6" style="display:none">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Internet Charge <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">


                                                <input type="text"  minlength="1"  id="mail_branch_internethandlingcharge" name="mail_branch_internethandlingcharge"  value="<?= trim($vieww[0]->internethandlingcharge); ?>" >
                                            </div>
                                        </div>
                                    </div>`
                                </div>
                                
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <div class="submit_tbl">
                                                <input id="submitBtn" class="btn btn-primary" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />  <a  href="<?= base_url('admin/managemail/viewmail'); ?>" class="btn btn-primary">CANCEL</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
