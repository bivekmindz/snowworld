<!DOCTYPE html>
<html>

<body>
<style>
body {
font-size: 13px !important;

}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>

<div ng-app="myApp" ng-controller="myCtrl" class="wrapper">
<div class="col-lg-10 col-lg-push-2">
<div class="row">

    <div class="page_contant">
        <div class="col-lg-12">
            <div class="page_name">
                <h2>Create Role</h2>
            </div>
            <div class="page_box">
                <div class="col-lg-12">
                    <p> In this section, admin can Assign Menu!
                        <a href="<?php echo base_url(); ?>">
                            <button type="button" style="float:right;">CANCEL</button>
                        </a></p>
                </div>
            </div>
            <div class="page_box">
                <div class="sep_box">
                    <div class="col-lg-12">
                        <div class='flashmsg'>
                            <?php echo validation_errors(); ?>
                            <?php
                            if ($this->session->flashdata('message')) {
                                echo $this->session->flashdata('message');
                            }
                            ?>
                        </div>

                        <form method="post" action="" name="adminmenuform" novalidate>
                           <!-- <div style="color: red" class="text-center"
                                 ng-show="adminmenuform.$dirty && adminmenuform.$invalid"> PLEASE FILL ALL
                                FIELDS
                            </div>-->
                            {{message}}
                            <div class="row">

                                <div class="sep_box">
                                    <div class="col-lg-10">
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="tbl_text">Select Role</div>
                                            </div>
                                            <div class="col-lg-9">
                                                <div class="tbl_input">
                                                    <select name="role" onchange="" id="rolename" ng-model="role_id"
                                                            ng-change="role_name()" required>
                                                        <option value="">Select</option>
                                                        <?php foreach ($vieww_role as $value) { ?>
                                                            <option value="<?php echo $value->role_id ?>"><?php echo $value->role_name ?></option>
                                                        <?php } ?>
                                                    </select>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="sep_box">
                                    <div class="col-lg-10">
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="tbl_text">EMPLOYEE NAME</div>
                                            </div>
                                            <div class="col-lg-9">
                                                <div class="tbl_input">
                                                    <select name="empid" ng-disabled="!role_id" id="empid"
                                                            ng-model="employee_id" ng-change="myFunc($event)"
                                                            required>
                                                        <option value="">Select</option>
                                                        <?php foreach ($vieww_employee as $value) { ?>
                                                            <option value="<?php echo $value->emp_tbl_id ?>"><?php echo $value->emp_name ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="sep_box">
                                    <div class="col-lg-10">
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="tbl_text">Modules</div>
                                            </div>
                                            <div class="col-lg-9">
                                                <div class="tbl_input firstmainmenu">
                                                    <select  name="mainmenu"
                                                            id="mainmenu" multiple="multiple"
                                                            ng-model="mainmenu" ng-change="submenu()" required>
                                                        <?php
                                                        foreach ($vieww_menu as $mainmenu) {
                                                            ?>
                                                            <option value="<?php echo $mainmenu->id; ?>"><?php echo $mainmenu->menuname; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>


                                                <div class="tbl_input secondmainmenu">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="sep_box">
                                    <div class="col-lg-10">
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="tbl_text"> SUB Modules</div>
                                            </div>
                                            <div class="col-lg-9">
                                                <div class="tbl_input firstmainmenu">
                                                    <select name="mainmenu_sub" id="mainmenu_sub"
                                                            ng-model="mainmenu_sub" multiple="multiple"

                                                            required>
                                                        <option ng-repeat="(x,y) in data"
                                                                value="{{y.id}}">{{y.menuname}}
                                                        </option>
                                                    </select>
                                                </div>


                                                <div class="tbl_input secondmainmenu">

                                                    `
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="sep_box">
                                    <div class="col-lg-10">
                                        <div class="row">
                                            <div class="col-lg-3">
                                            </div>
                                            <div class="col-lg-9">
                                                <div class="tbl_input">
                                                    <input type="submit"
                                                           ng-click="adminmenuform.$valid && formsub($event)"
                                                           value="Submit" name="addUserMenu" class="button_S"
                                                           id="buttonshow"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</body>

</html>

<style type="text/css">
input[type="text"] {
vertical-align: top;
width: 90% !important;
}

.field_wrapper div {
margin-bottom: 10px;
}

.add_button {
line-height: 38px;
margin-left: 10px;
}

.remove_button {
line-height: 38px;
margin-left: 10px;
}
</style>


<script>
$(function () {
$('#mainmenu,#mainmenu_sub').select2({
    allowClear: true
});
    $('#rolename').change(function(){
        $('#empid').prop('selectedIndex',0);
        $("#mainmenu").val([]).trigger('change');
        $("#mainmenu_sub").val([]).trigger('change');
    });
    $("#mainmenu_sub").change(function (e) {
        //alert('asd');
        $("#mainmenu").trigger('change');
    })
});



var app = angular.module('myApp', []);
app.controller('myCtrl', function ($scope, $http, $timeout) {
$scope.isProcessing = true;


$scope.submenu = function () {
    var data = $.param({
        mainmenuid: $scope.mainmenu
    });
    var config = {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        }
    };
    $http.post('<?php echo base_url('admin/managerole/getsubmenu'); ?>', data, config)
        .success(function (data, status, headers, config) {
            $scope.data = data.vieww_submenu;
        });
};

$scope.formsub = function (e) {

    e.preventDefault();
    var roleid = $scope.role_id;
    var empid = $scope.employee_id;
    var mainmenu = $scope.mainmenu;
    var submenu = $scope.mainmenu_sub;
    var data = $.param({
        roleid: $scope.role_id,
        empid: $scope.employee_id,
        mainmenu: $scope.mainmenu,
        submenu: $scope.mainmenu_sub,
    });
    var config = {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        }
    };
    $http.post('<?php echo base_url('admin/managerole/addasignrole_DELETE'); ?>', data, config)
        .success(function (data, status, headers, config) {
        });

    e.preventDefault();
    var roleid = $scope.role_id;
    var empid = $scope.employee_id;
    var mainmenu = $scope.mainmenu;
    var submenu = $scope.mainmenu_sub;
    var data = $.param({
        roleid: $scope.role_id,
        empid: $scope.employee_id,
        mainmenu: $scope.mainmenu,
        submenu: $scope.mainmenu_sub,
    });
    var config = {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        }
    };
    $http.post('<?php echo base_url('admin/managerole/addasignrole'); ?>', data, config)
        .success(function (data, status, headers, config) {
            alert("inserted sucessfully");
            location.reload();

        });
};


$scope.myFunc = function (e) {

    $(function () {
        $('#mainmenu option').attr('selected', false);
//alert($("#rolename").val());
    })


    var data = $.param({
        roleid: 100,
        empid: $scope.employee_id,
    });
    var config = {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        }
    };
    $http.post('<?php echo base_url('admin/managerole/get_menu_submenu'); ?>', data, config)
        .success(function (data, status, headers, config) {
//console.log(data);
            if (data != ""){
                if (data['0'].mainmenu) {

                    var mmenuu = (data['0'].mainmenu).split(',');
                    var subbmenuu = (data['0'].submenu).split(',');
//console.log(data);
                    for (var i = 0; i < mmenuu.length; i++) {
                        $('#mainmenu option[value="' + mmenuu[i] + '"]').attr('selected', true);
                    }

//alert();
                    var data = $.param({
                        mainmenuid: $("#mainmenu").val()
                    });
                    var config = {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                        }
                    };
                    $http.post('<?php echo base_url('admin/managerole/getsubmenu'); ?>', data, config)
                        .success(function (data, status, headers, config) {
                            //console.log(data);
                            $scope.data = data.vieww_submenu;
                            //console.log(data.vieww_submenu['0'].id);
                            for (var j = 0; j < subbmenuu.length; j++) {
                                //console.log(subbmenuu[j]);
                                //console.log($('#mainmenu_sub option'));
                                //$('#mainmenu_sub option[value="' + subbmenuu[j] + '"]').attr('selected', true);
                            }
                        });

                    $timeout(function () {
                        for (var j = 0; j < subbmenuu.length; j++) {
                            //console.log(subbmenuu[j]);
                            $('#mainmenu_sub option[value="' + subbmenuu[j] + '"]').attr('selected', true);
                        }
                        $('#mainmenu,#mainmenu_sub').select2();
                    }, 1000);


                }
        }

        });


};
});

</script>


<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?= base_url() ?>/js/jquery.table.hpaging.min.js"></script>

<script type="text/javascript">
$(function () {
$("#table1").hpaging({"limit": 10});
});

$("#btnApply").click(function () {
var lmt = $("#pglmt").val();
$("#table1").hpaging("newLimit", lmt);
});
</script>
<script type="text/javascript">

var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-36251023-1']);
_gaq.push(['_setDomainName', 'jqueryscript.net']);
_gaq.push(['_trackPageview']);

(function () {
var ga = document.createElement('script');
ga.type = 'text/javascript';
ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'https://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(ga, s);
})();

</script>
<style>
.pagination {
display: inline-block;
}

.pagination a {
color: black;
float: left;
padding: 8px 16px;
text-decoration: none;
}

.pagination a.active {
background-color: #4CAF50;
color: white;
}

.pagination a:hover:not(.active) {
background-color: #ddd;
}
</style>