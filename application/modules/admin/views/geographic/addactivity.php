<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js" crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<script>


    $(function() {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#add_act").validate({
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                act_name: "required",
                act_desc: "required",
                act_price: {required: true,
                    number: true}
            },
            // Specify validation error messages
            messages: {
                act_name: "Please enter name",
                act_desc: "Please enter description",
                act_price: {required: "Enter price",
                    number :"Number Only"}
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });



        $("#update_act").validate({
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                act_name_update: "required",
                act_desc_update: "required",
                act_price_update: {required: true,
                    number: true}
            },
            // Specify validation error messages
            messages: {
                act_name_update: "Please enter name",
                act_desc_update: "Please enter description",
                act_price_update: {required: "Enter price",
                    number :"Number Only"}
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });


    });


</script>
<style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>ADD Activity</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div style="text-align:right;">
                                    <a href=""><button>CANCEL</button></a>
                                </div>
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?>
                                    <?php
                                    if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>



                        <form style="display: none" action="<?= base_url('admin/geographic/addactivity') ?>" id="update_act" method="post" enctype="multipart/form-data" >
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Update Activity Name<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="act_name_update" name="act_name_update" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Update Activity Description<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <textarea  id="act_desc_update" name="act_desc_update" ></textarea>
                                                <input type="hidden" name="act_id" id="act_id">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Update Activity Price<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="act_price_update" name="act_price_update" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <div class="submit_tbl">
                                                <input id="submitBtn_update" type="submit" name="submit_update" value="Update" class="btn_button sub_btn" />

                                                <input id="submitBtn_update" type="button" onclick="return UpdateCancel()" name="submit_update" value="Cancel" class="btn_button sub_btn" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>


                        <form action="<?= base_url('admin/geographic/addactivity') ?>" name="add_act" id="add_act" method="post" enctype="multipart/form-data" >
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Activity Name<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="act_name" name="act_name" >
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Activity Description<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <textarea  id="act_desc" name="act_desc" ></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Activity Price<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="act_price" name="act_price" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <div class="submit_tbl">
                                                <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>


                        <div class="container">
                            <h2>Table</h2>
                            <div class="table-responsive" style="width: 100%;">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>S.NO</th>
                                        <th>Activity NAME</th>
                                        <th>DESCRIPTION</th>
                                        <th>PRICE</th>
                                        <th>EDIT/DELETE</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach($vieww as $v) {?>
                                        <tr>
                                            <td><?= $i;?></td>
                                            <td id="t_act_name"><?php echo $v->activity_name; ?></td>
                                            <td id="t_act_desc"><?php echo $v->activity_discription; ?></td>
                                            <td id="t_act_price"><?php echo $v->activity_price; ?></td>
                                            <td>

                                                <a class="btn btn-primary" onclick="return update_act('<?php echo $v->activity_name; ?>','<?php echo $v->activity_discription; ?>','<?php echo $v->activity_price; ?>','<?php echo $v->activity_id; ?>')">
                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>




                                                <a href="<?= base_url('admin/geographic/activity_delete_new')."/".$v->activity_id ?>"  onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </a>







                                            </td>

                                        </tr>

                                        <?php $i++;} ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>




                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>

    function update_act(a,b,c,d){
        //alert(a +b +c +d);return false;
        $("#act_name_update").val(a);
        $("#act_desc_update").val(b);
        $("#act_price_update").val(c);
        $("#act_id").val(d);
        $("#update_act").css('display','block');
        $("#add_act").css('display','none');
        $("#act_name_update").focus();

    }
    function UpdateCancel() {
        $("#add_act").css('display','block');
        $("#update_act").css('display','none');
        return false;
    }





</script>