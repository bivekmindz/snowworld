<script src="<?php echo base_url() ?>assets/js/jquery.validate.min.js"></script>
<!-- <div class="wrapper"> -->
<style>
.error {
color: red;

}
.tab-pane h4 b {
color: #1b78c7;
}
.modal-body {
width: 100%;
float: left;
position: relative;
}
</style>
<div class="col-lg-10">
<div class="row">
<div class="page_contant">
<div class="col-lg-12">
<div class="page_name">
<h2>Manage Agent Commission</h2>
</div>
<div class="page_box">
<div class="col-lg-12">
<div id="content">
<form id="addCont" action="" method="post" enctype="multipart/form-data">
<div id="my-tab-content" class="tab-content">
<!--  session flash message  -->
<div class='flashmsg'>
<?php echo validation_errors(); ?>
<?php
if ($this->session->flashdata('message')) {
echo $this->session->flashdata('message');
}
?>
</div>
<div id="agentdiv">
<table class="table table-bordered table-striped" id="table1">
<thead>
<tr>
<th>S:NO</th>
<th>Agency Name</th>
<th>User Name</th>
<th>Invoice ID</th>
<th>Contact</th>
<th>Mail id</th>
<th>Comission</th>
<th>Commistion Amt</th>
<th>Action</th>
</tr>
</thead>
<tbody>
<?php $i = 0;
//p($walletprices);

foreach ($agentvieww as $key => $value) { 
 $parameter2 = array( 'act_mode'=>'getwalletamount',
	'Param1'=>$value['agent_id'],
	'Param2'=>'',
	'Param3'=>'',
	'Param4'=>'',
	'Param5'=>'',
	'Param6'=>'',
	'Param7'=>'',
	'Param8'=>'',
	'Param9'=>'');
	$data['walletprices'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2); 

foreach ($data['walletprices'] as $key => $valuePrice) { 
$i++;
if($value['per_amount']){
	$per_ammount  = $value['per_amount'];
}else{
	$per_ammount = 0;
}
?>
<tr>
<td><?php echo $i; ?></td>
<td><?php echo $value['agent_agencyname']; ?></td>
<td><?php echo $value['agent_username']; ?></td>
<td><button style="background-color:orange "><?php echo $valuePrice->invoice_id; ?></button></td>
<td><?php echo $value['agent_mobile']; ?></td>
<td><?php echo $value['agent_email']; ?></td>
<td><a  title="Manage Comission Amount" href="#"><i
class="btn fa fa"><?php echo " " . $valuePrice->commission. " %"; ?>
</a></i></td>
<td>
    <a title="Manage Wallet Amount" href="#"><i
class="btn fa fa-inr"><?php //echo " " . $value->bank_creditamount; ?>
<?php if(!empty($valuePrice->commprice)){ echo $valuePrice->commprice; } else{ echo"0";}?>
</a></i>
</td>
<td>
<!-- <a onclick="return StatusCompany('<?php echo $value['agent_id'] ?>','<?php echo $value['agent_status']; ?>')"><?php if ($value['agent_status'] == '0') { ?>Send<?php } else { ?>Send<?php } ?></a> -->
<input type="button" onclick ="popmessage('<?php echo $value['agent_id'] ?>','<?php echo $value['agent_agencyname']; ?>','<?php echo $value['agent_mobile']; ?>','<?php echo $value['agent_email']; ?>')"  value="Send" />
</td>
</tr>
<?php } } ?>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/jquery-ui.js" type="text/javascript"></script>
<link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/blitzer/jquery-ui.css"
    rel="stylesheet" type="text/css" />

<script type="text/javascript">
    $(function () {
        $("#dialog").dialog({
            modal: true,
            autoOpen: false,
            title: "Agent Commistion Detail",
            width: 400,
            height: 350
        });
       
    });
</script>


<div id="dialog" style="display: none" align = "center">
    <table>
    <tr><th>Name:</th><td><p class="agent_name"></td></tr>
    <tr><th>Email:</th><td><p class="agent_email"></td></tr>
    <tr><th>Mobile:</th><td><p class="agent_mobile"></td></tr>
    </table>
    <input type="hidden" value="" class="agentid" id="agentid" name="agentid">
    <div id="showdetail"></div>
    </br>
    <button onclick="sendcomm()">Send</button>
</div>



<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script>
function popmessage(agentid,names,mobile,email){
	$('#dialog').dialog('open');
	var agentids =agentid;
	var name =names;
	var mobile =mobile;
	var email =email;
	 $('.agent_name').html(name);
	 $('.agent_mobile').html(mobile);
	 $('.agent_email').html(email);
	 $('.agentid').val(agentids);

	 $.ajax({
		type: "POST",
		url: "<?php echo base_url()?>admin/commission/agentDetail",
		//cache: false,
		//contentType: false,
		//processData: false,
		data: { 'agentids' : agentids},
		success: function (data) {
		$("#showdetail").html(data);
		},
		error: function (data) {
		// alert(data);
		}
	});
}

function sendcomm(){
	var agentid = $('#agentid').val();
	var totalamnt = $('#totalamnt').val();
	 $.ajax({
		type: "POST",
		url: "<?php echo base_url()?>admin/commission/savedetailcommission",
		data: { 'agentids' : agentid,'totalamnt':totalamnt},
		success: function (data) {
		location.reload(); 
		},
		error: function (data) {
		// alert(data);
		}
	});
}

function RemoveCompany(a) {
if (confirm("Are you sure to delete?") == true) {
$.ajax({
type: "POST",
url: "<?php echo base_url()?>admin/b2b/agentdel/" + a,
cache: false,
contentType: false,
processData: false,
success: function (data) {
// alert(data);
$("#agentdiv").load(location.href + " #agentdiv");
$("#msgdiv").html('deleted sucessfully');
},
error: function (data) {
// alert(data);
}
});
} else {
return false;
}
}
</script>
<script>
$(function(){
var table = $('#table1').DataTable({"scrollY": 400,"scrollX": true });
})
</script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>

