
<style>
    canvas{
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }
    .box12{ width: 100%; float: left; margin: 20px 0 10px 0; }
    .box12 input[type="text"]{
        margin: 0 5px 0 0;padding: 10px 3px;
        width: 40%;
    }
    .box12 input[type="submit"]{
        border:none;
        background: #1B78C7;
        color: #fff;
        font-size: 14px;
        padding: 10px 15px;
    }
</style>
<script src="<?php echo base_url(); ?>js/Chart.bundle.js"></script>
<script src="<?php echo base_url(); ?>js/utils.js"></script>
<!-- <div class="wrapper"> -->
<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="">
                <div class="col-lg-12">
                    <div class="page_name"><h2>Date Wise Inventory</h2></div>



                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div style="text-align:right;">
                                    <a  href="<?= base_url('admin/inventory/datewiseinventory'); ?>" class="btn btn-primary">CANCEL</a>
                                </div>
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?>
                                    <?php
                                    if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>




                        <form action="" name="add_com" id="add_com" method="post" enctype="multipart/form-data" >
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="tbl_text">Select Branch <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="tbl_input">
                                                <select name="branchids" id="branchids">
                                                    <?php foreach ($s_viewbranch as $key => $value) { ?>
                                                        <option value="<?php echo $value->branch_id; ?>"><?php echo $value->branch_name; ?></option>
                                                    <?php } ?>
                                                </select></div>
                                        </div>
                                    </div>
                                    </di v>
                                </div>

                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="tbl_text">Select Date <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="tbl_input">

                                                <input  class="date-pick form-control formmm1" type="text" id="datepicker1" name="departDate" placeholder="Pick Session Date"  title="Pick Session Time" value="<?php echo $Datevalue; ?>">


                                            </div>
                                        </div>
                                        </di v>
                                    </div>
                                </div>
                                <div class="sep_box">

                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-3"></div>
                                            <div class="col-lg-8">
                                                <div class="submit_tbl">
                                                    <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>




                        </form>


                        <div class="col-md-12">
                            <h2>Table</h2>
                            <div class="table-responsive" style="width: 100%;">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>S.NO</th>
                                        <th>From To</th>
                                        <th>Total Number Of Seat</th>
                                        <th>Total Booked Seat</th>
                                        <th>Canceled Seat</th>
                                        <th>Remaining Seat</th>
                                        <th>UPDATE</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach($s_viewtimeslot as $v) {?>
                                        <tr>

                                            <td><?= $i;?></td>
                                            <td id="t_com_name"><?php echo $v->dailyinventory_from; ?>:<?php echo $v->dailyinventory_minfrom; ?> Hrs - <?php echo $v->dailyinventory_to; ?>:<?php echo $v->dailyinventory_minto; ?> Hrs </td>
                                            <td> <input type="text" value="<?php echo $v->dailyinventory_seats; ?>" name="pre_date[]"> </td>
                                            <td> <?php echo $v->coun; ?> </td>
                                            <td> <?php echo $v->Aborted; ?> </td>
                                            <td> <?php echo ( $v->dailyinventory_seats-$v->coun); ?> </td>
                                            <td class="submit_tbl">

                                                <input type="hidden" value="<?php echo $v->dailyinventory_id; ?>" name="dailydate_id[]">
                                                <input type="submit" name="submit" value="UPDATE" class="btn_button sub_btn"></td>

                                        </tr>
                                        <?php $i++;} ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>



                    </div></div></div></div></div></div>

<script>
    var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var config = {
        type: 'line',
        data: {
            labels: [
                <?php foreach($data as $value){ ?>
                "<?php echo $value->monthname; ?>",
                <?php }
                ?>
            ],
            datasets: [{
                label: "Total Month-Wise Order",
                backgroundColor: window.chartColors.blue,
                borderColor: window.chartColors.blue,
                data: [
                    <?php foreach($data as $key=>$value)
                {
                    echo $value->coun.',';
                }
                    ?>
                ],
                fill: false,
            }]
        },
        options: {
            responsive: true,
            title:{
                display:true,
                // text:'Chart.js Line Chart'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Month'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Value'
                    }
                }]
            }
        }
    };
    window.onload = function() {
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myLine = new Chart(ctx, config);
    };
    document.getElementById('randomizeData').addEventListener('click', function() {
        config.data.datasets.forEach(function(dataset) {
            dataset.data = dataset.data.map(function() {
                return randomScalingFactor();
            });
        });
        window.myLine.update();
    });
    var colorNames = Object.keys(window.chartColors);
    document.getElementById('addDataset').addEventListener('click', function() {
        var colorName = colorNames[config.data.datasets.length % colorNames.length];
        var newColor = window.chartColors[colorName];
        var newDataset = {
            label: 'Dataset ' + config.data.datasets.length,
            backgroundColor: newColor,
            borderColor: newColor,
            data: [],
            fill: false
        };
        for (var index = 0; index < config.data.labels.length; ++index) {
            newDataset.data.push(randomScalingFactor());
        }
        config.data.datasets.push(newDataset);
        window.myLine.update();
    });
    document.getElementById('addData').addEventListener('click', function() {
        if (config.data.datasets.length > 0) {
            var month = MONTHS[config.data.labels.length % MONTHS.length];
            config.data.labels.push(month);
            config.data.datasets.forEach(function(dataset) {
                dataset.data.push(randomScalingFactor());
            });
            window.myLine.update();
        }
    });
    document.getElementById('removeDataset').addEventListener('click', function() {
        config.data.datasets.splice(0, 1);
        window.myLine.update();
    });
    document.getElementById('removeData').addEventListener('click', function() {
        config.data.labels.splice(-1, 1); // remove the label first
        config.data.datasets.forEach(function(dataset, datasetIndex) {
            dataset.data.pop();
        });
        window.myLine.update();
    });
</script>

<style>
    .count{
        width: 25%;
        float: left;
        padding: 0 15px;
        box-sizing: border-box;
    }
    .total-order{
        font-size: 18px;
        line-height: 24px;
        color: #393939;
    }
    .total-order{
        padding: 10px 0;
        border-bottom: 1px solid #c1c1c1;
    }
    .total-order span{
        color: #1B78C7;
        font-size: 22px;
        line-height: :24px;
        font-weight: 600;
    }
    .box1{
        width: 100%;
        float: left;
        text-align: right;
        background-color: #1272a5;
        color: white;
        padding:10px;
        box-sizing: border-box;
    }
    .left_bar {
        width: 100%;
        float: left;
        padding: 10px 0px;
    }
    .box2 {
        width: 100%;
        float: left;
        text-align: right;
        background-color: #278fc5;
        color: white;
        padding:10px;
        box-sizing: border-box;
    }
    .box3 {
        background-color: #3ca4da !important;
    }
    .box4 {
        background-color: #49aee2 !important;
    }
    .box5 {
        background-color: #56b5e6 !important;
    }
    .box6 {
        background-color: #71c5f1 !important;
    }
</style>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(function() {
        $.noConflict();
        $("#datepicker1").datepicker({
            dateFormat: 'dd-mm-yy',
             minDate: 0,
            maxDate: "<?php echo $s_viewdate->date; ?>D "
        });

        $("#datepicker2").datepicker({
            dateFormat: 'dd-mm-yy',
            // minDate: 0,
            //maxDate: "-1M "
        });
    });
</script>