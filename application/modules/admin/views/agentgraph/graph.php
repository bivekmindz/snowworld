<?php
//pend($result);

?>
<style>
    canvas{
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }
</style>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js"></script>
<script type="text/javascript">
    // Load the Visualization API and the line package.
    google.charts.load('current', {'packages':['corechart']});
    // Set a callback to run when the Google Visualization API is loaded.
    google.charts.setOnLoadCallback(drawChart);



    function drawChart()
    {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Agent');
        data.addColumn('number', 'Total order');
        data.addRows([
            <?php foreach($result as $key=>$value)  {?>
            ['<?php echo $value->agent ?>',<?php echo $value->totalorder  ?>],
            <?php  } ?>
        ]);

        var options = {
            legend: '',
            pieSliceText: 'label',
            title: 'Top Ten Orders By Agent',
            is3D: true,
            width: 900,
            height: 500,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);

    }



    function validatedate()
    {
        var startdate=$('#datepicker1').val();
        var enddate=$('#datepicker2').val();
      // alert(startdate);
        var eDate = new Date(enddate);
        var sDate = new Date(startdate);
        if(startdate!= '' && startdate!= '' && sDate> eDate)
        {
            alert("Please ensure that the End Date is greater than or equal to the Start Date.");
            return false;
        }

    }

</script><!--<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>-->
<!--<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>-->
<!-- <div class="wrapper"> -->
<div class="col-lg-10">
    <div class="row">
        <div class="page_contant">
            <div class="col-lg-12 col-xs-12">
                <div class="page_name">
                    <h2>Agent Graph</h2>
                </div>
                <div class="page_box">

                    <div class="count">
                        <div class="total-order">
                            Total Order : <span><?php echo $countorder->totalorder;  ?></span>
                        </div>
                        <div class="total-order">
                            Total Agent : <span><?php echo $countagent->totalagent;  ?></span>

                        </div>
                    </div>
                    <form id="target" method="post" action="">
                        <div class="col-md-8">
                            <div class="box12">
                                <input type="text" placeholder="From-date" required value ="<?php echo ($_POST['datepicker1']) ? $_POST['datepicker1'] : '' ?>" name="datepicker1" id="datepicker1">
                                <input type="text" placeholder="To-date" required value ="<?php echo ($_POST['datepicker2']) ? $_POST['datepicker2'] : '' ?>" name="datepicker2" id="datepicker2">


                                <input type="submit" value ="Search" name="search" id="search" onclick="validatedate();" class="btn btn-primary" >
                                <a  href="<?= base_url('admin/graphmanagement/agentgrapg'); ?>" class="btn btn-primary">Reset</a>
<!--                                <input type="submit" value ="Reset" name="reset" id="reset" onclick="window.location.reload(true);" >-->
                            </div>
                        </div>
                    </form>

                    <div style="width:75%; float: left;min-height: 450px;">
                        <div id="piechart" style="width: 900px; height: 500px;">

                        </div>
                        <!--                            <div id="chartContainer"></div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!--<script type="text/javascript">-->
<!--$(function () {-->
<!--var chart = new CanvasJS.Chart("chartContainer", {-->
<!--    theme: "theme2",-->
<!--    title:{-->
<!--        text: "Top 10 agent"-->
<!--    },-->
<!--    exportFileName: "Top 10 agent",-->
<!--    exportEnabled: true,-->
<!--    animationEnabled: true,     -->
<!--    data: [-->
<!--    {       -->
<!--        type: "pie",-->
<!--        showInLegend: true,-->
<!--        toolTipContent: "{name}: <strong>{y}</strong>",-->
<!--        indexLabel: "({name}) Amount:- {y}",-->
<!--        dataPoints: --><?php //echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
<!--    }]-->
<!--});-->
<!--chart.render();-->
<!--});-->
<!--</script>-->

<style>
    .count{
        width: 25%;
        float: left;
        padding: 0 15px;
        box-sizing: border-box;
    }
    .total-order{
        font-size: 18px;
        line-height: 24px;
        color: #393939;
    }
    .total-order{
        padding: 10px 0;
        border-bottom: 1px solid #c1c1c1;
    }
    .total-order span{
        color: #1B78C7;
        font-size: 22px;
        line-height: :24px;
        font-weight: 600;
    }
</style>

<script>
    $(function() {
        $.noConflict();
        $("#datepicker1").datepicker({
            dateFormat: 'yy-mm-dd',
            // minDate: 0,
//            maxDate: "-1M "
        });

        $("#datepicker2").datepicker({
            dateFormat: 'yy-mm-dd',
            // minDate: 0,
            //maxDate: "-1M "
        });
    });

//    $("#search").click(function(e) {
//        e.preventDefault();
//        drawChart();
//    });
</script>