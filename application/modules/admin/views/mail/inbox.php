
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js" crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<script>
$(function() {
// Initialize form validation on the registration form.
// It has the name attribute "registration"
$("#add_mail").validate({
// Specify validation rules
rules: {
// The key name on the left side is the name attribute
// of an input field. Validation rules are defined
// on the right side
subject: "required",
message: "required",
'emailid[]': "required"

},
// Specify validation error messages
messages: {
subject: "Please enter subject",
'emailid[]': "Please enter email",
message: "Please enter message",
},
// Make sure the form is submitted to the destination defined
// in the "action" attribute of the form when valid
submitHandler: function(form) {
form.submit();
}
});

});
</script>
<style>
input.error{border:1px solid red;}
label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
</style>
<meta name="viewport" content="width=device-width, initial-scale=1">
<div class="wrapper">
  <div class="col-lg-10 col-lg-push-2">
    <div class="row">
      <div class="page_contant">
        <div class="col-lg-12">
          <div class="page_name">
            <h2>Send Mail  </h2>
            <?php
if($this->session->flashdata('message')){
echo $this->session->flashdata('message');
}
?>
          </div>
          <div class="snd_mail_form">
            <div class="row">
              <div class="col-lg-8 col-md-8 ">
      <form action="" name="add_mail" id="add_mail"method="post" enctype="multipart/form-data">
                  <div class="form-group">
                    <div class="row">
                      <div class="col-lg-2">
                        <label>Email Id</label>
                      </div>
                      <div class="col-lg-5">

    <select id="emailid" name="emailid[]"  multiple="multiple" class="form-input" >
  <?php  
   
  foreach ($AgentEmail as $key => $value) {?>
     <option value="<?php echo $value->agent_email;?>"><?php echo $value->agent_email;?></option>
  <?php }?>
  </select>
                      </div>
                    </div>
                  </div>                 
                  <div class="form-group">
                    <div class="row">
                      <div class="col-lg-2">
                        <label>Subject</label>
                      </div>
                      <div class="col-lg-5">
        <input type="text" name="subject" class="form-input" >
                      </div>
                    </div>
                  </div>
                   <div class="form-group">
                    <div class="row">
                      <div class="col-lg-2">
                        <label>Message</label>
                      </div>
                      <div class="col-lg-5">
                        <textarea cols="10" name="message" ></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                       <div class="col-lg-2">
                       
                      </div>

                      <div class="col-lg-5 text-center">
                        <input type="submit" name="submit" class="sub-btn"/> 
                      </div>                      
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          
          
         <!--  <div class="inbox_dta">
            
            <div class="col-md-3">
              
              <div class="left_pane-box">
                
                <div class="compose_mail">
                  <a href="">Compose</a>
                </div>
                
                
                <div class="flo_de">
                  <div class="fol_der_he">
                    <h1> Folder Name <span><i class="fa fa-minus"></i></span></h1>
                  </div>
                  
                  <div class="in_vo">
                    <ul>
                      <li><a href="" class="open"><i class="fa fa-envelope-o" aria-hidden="true"></i> Inbox <span>226</span></a></li>
                      <li><a href="" ><i class=" fa fa-send-o" aria-hidden="true"></i> Send <span></span></a></li>
                      <li><a href="" ><i class="fa fa-envelope-open-o" aria-hidden="true"></i> Drafts <span>226</span></a></li>
                      <li><a href="" ><i class="fa fa-star-o" aria-hidden="true"></i> Starred <span>226</span></a></li>
                      <li><a href="" ><i class="fa fa-trash-o" aria-hidden="true"></i> Trash <span>226</span></a></li>
                    </ul>
                  </div>
                  
                  
                </div>
                
                
              </div>
              
            </div>
            
            <div class="col-md-9">
              
              
              
            </div>
            
            
            
          </div> -->
        </div>
      </div>
    </div>
  </div>
</div>
<script>
function update_com(a,b){
//alert(a +b);
$("#com_name_update").val(a);
$("#com_id").val(b);
$("#update_com").css('display','block');
$("#add_com").css('display','none');
$("#com_name_update").focus();
}
function UpdateCancel() {
$("#add_com").css('display','block');
$("#update_com").css('display','none');
return false;
}
</script>

  <script>
  $(document).ready(function(){
  $('#table1').dataTable();
  });
  </script>
  <style type="text/css">
    .snd_mail_form{
      width: 100%; float: left;
      border:1px solid #ddd;
      box-shadow: 1px 2px 3px #393939;
      padding: 30px;
      box-sizing: border-box;
    }
    .snd_mail_form .form-group label{
      font-weight: normal;
      line-height: 35px;
      font-size: 16px;
    }
      .snd_mail_form .form-group input{
        width: 100%; 
        float: left;
        padding: 8px;
      }
    .snd_mail_form .form-group textarea{
    width: 100%;
    height: 100px;
    resize: none;
    padding: 8px;
  }
  .snd_mail_form .form-group .sub-btn{
      padding: 10px 25px;
    color: #fff;
    font-size: 16px;
    font-weight: normal;
    letter-spacing: 1px;
  }
  </style>
  <link href="<?php echo base_url()?>assets/js/select2.min.css" rel="stylesheet" />
    <script src="<?php echo base_url()?>assets/js/select2.min.js"></script>   
    <script type="text/javascript"> 
     $('#emailid').select2();
    
    </script>

  