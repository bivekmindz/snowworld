<?php
class employeeModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function EmployeeModel()
    {
        parent::Model();
    }
 
   //set table name to be used by all functions
 var $table = 'testemployee';

 function fetch_record($limit, $start)
 {
  $this->db->limit($limit, $start);
  $query = $this->db->get($this->table);
  return ($query->num_rows() > 0)  ? $query->result() : FALSE;

 }

 function record_count()
 {
    $this->db->select('*');
    $this->db->from("testemployee");
    $q = $this->db->get();
    return $q->num_rows();
 }

public function jQuerypagination_countriesdata($per_page,$offset) { 
    $sdata = array();
    if($offset){ $offset = 0; }
    
    $this->db->select('*')->from('testemployee');    
    $this->db->limit($per_page,$offset);
    $query_result = $this->db->get();
    //echo $this->db->last_query(); // shows last executed query
    echo $per_page;

    echo "<br/>";
    echo $offset;
    echo "<br/>";
    
    if($query_result->num_rows() > 0) {
        foreach ($query_result->result_array() as $row)
        {
           //p($row);
            $sdata[] = array('id' => $row['id'],'name' => $row['name'],'phone' => $row['phone'],'email' => $row['email'],'address' => $row['address']);
        }     
    }
    return $sdata;
}
    public function designation(){
        $query = $this->db->query("select DesignationID,designation from tbldesignation where status='A'");
        $data = $query->result();
        return $data;
    }
    public function roleassign(){
    	//$query = $this->db->query("select e.Display_Text from tblenumtype et, tblenum e where et.Enum_Type_Description='Role' and et.Enum_Type_ID=e.Enum_Type");
        $query = $this->db->query("select id, rollName from tblrolename");
        $data = $query->result();
        return $data;
    }
    public function roleassigned($id){
    	$query = $this->db->query("select role_id from tblassignmenu where emp_id=$id");
        $data = $query->result();
      
        return $data;
    }
    public function getReportingMgr(){
        $query = $this->db->query("select F_Name, L_Name,tblemployee.EmployeeID from tblemployee where designation=(select DesignationID from tbldesignation where designation='Manager')");
        $data = $query->result();
      return $data;
    }

}

?>