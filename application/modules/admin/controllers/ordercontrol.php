<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Ordercontrol extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
        //$this->load->library('session');
 $this->load->helper('adminmenu_helper');
    }
public function sucessgorder_sess(){

        if($_POST['Clear']=='Show All Records')
        {
            $this->session->unset_userdata('order_filter');
        }


        $a = explode(":",$this->input->post('filter_date_session'));
        $b = explode(" ",$this->input->post('filter_date_session'));
//p($a);
//p($b);

        if($b['1'] == 'PM'){
            $session_time_param = $a['0'] + 12;
        }
        else{
            $session_time_param = $a['0'];
        }
        //p($_POST);
        //$this->session->unset_userdata('order_filter');
        $a = $this->input->post('filter_branch');
        $b = $this->input->post('filter_status');
        $c = $this->input->post('filter_tim');
        $d = $this->input->post('filter_sta');

        $branchids = implode(',',$a);
        $filterstatus = implode(',',$b);
        $filtertime = implode(',',$c);
        $filtersta = implode(',',$d);
        $array = array('branchids' =>$branchids,
            'filter_name' => $this->input->post('filter_name'),
            'filter_email' => $this->input->post('filter_email'),
            'filter_ticket' => $this->input->post('filter_ticket'),
            'filter_mobile' => $this->input->post('filter_mobile'),
            'filter_payment' => $this->input->post('filter_payment'),
            'filter_date_booking_from' => $this->input->post('filter_date_booking_from'),
            'filter_date_booking_to' => $this->input->post('filter_date_booking_to'),
            'filter_date_session_from' => $this->input->post('filter_date_session_from'),
            'filter_date_session_to' => $this->input->post('filter_date_session_to'),
            'filter_status' => $filterstatus,
            'filter_date_printed_from' => $this->input->post('filter_date_printed_from'),
            'filter_date_printed_to' => $this->input->post('filter_date_printed_to'),
            'filter_date_ticket' => $this->input->post('filter_date_ticket'),
            'filter_date_session' =>    $session_time_param,
            'filter_tim' => $filtertime,
            'filter_sta' => $filtersta
        );
        $this->session->set_userdata('order_filter',$array);
        redirect('admin/ordercontrol/sucessgorder?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));


    }

    /*listing filter */
    public function order_sess(){

        if($_POST['Clear']=='Show All Records')
        {
            $this->session->unset_userdata('order_filter');
        }
  

        $a = explode(":",$this->input->post('filter_date_session'));
        $b = explode(" ",$this->input->post('filter_date_session'));

        if($b['1'] == 'PM'){
           $session_time_param = $a['0'] + 12;
        }
        else{
           $session_time_param = $a['0'];
        }
        
        $a = $this->input->post('filter_branch');
        $b = $this->input->post('filter_status');
        $c = $this->input->post('filter_tim');
        $d = $this->input->post('filter_sta');

         $branchids = implode(',',$a);
         $filterstatus = implode(',',$b);
         $filtertime = implode(',',$c);
         $filtersta = implode(',',$d);
        $array = array('branchids' =>$branchids,
            'filter_name' => $this->input->post('filter_name'),
            'filter_email' => $this->input->post('filter_email'),
            'filter_ticket' => $this->input->post('filter_ticket'),
            'filter_mobile' => $this->input->post('filter_mobile'),
            'filter_payment' => $this->input->post('filter_payment'),
            'filter_date_booking_from' => $this->input->post('filter_date_booking_from'),
            'filter_date_booking_to' => $this->input->post('filter_date_booking_to'),
            'filter_date_session_from' => $this->input->post('filter_date_session_from'),
            'filter_date_session_to' => $this->input->post('filter_date_session_to'),
            'filter_status' => $filterstatus,
            'filter_date_printed_from' => $this->input->post('filter_date_printed_from'),
            'filter_date_printed_to' => $this->input->post('filter_date_printed_to'),
            'filter_date_ticket' => $this->input->post('filter_date_ticket'),
            'filter_date_session' =>    $session_time_param,
            'filter_tim' => $filtertime,
            'filter_sta' => $filtersta
            );
        $this->session->set_userdata('order_filter',$array);
        redirect('admin/ordercontrol/order?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));


    }


        /*view all orders*/
    public function order()
    {

if(getMemberId()==1)

{

}
else
{
redirect('admin/login/dashboard?empid='.$_GET['empid'].'&uid='.$_GET['uid'].'');
}
     //p($_POST);
    
       //$this->session->unset_userdata('order_filter');

        if($this->session->userdata('order_filter'))
        {
           //p($this->session->userdata('order_filter'));

            if($this->session->userdata('order_filter')['filter_tim']>12)
            {
                $filter_tim1=$this->session->userdata('order_filter')['filter_tim'] -12;
                $filter_tim='0'.$filter_tim1;

                $filter_aptim="PM";
            }
            if($this->session->userdata('order_filter')['filter_tim']==12)
            {
                $filter_tim=$this->session->userdata('order_filter')['filter_tim'] ;  $filter_aptim="PM";
            }
            if($this->session->userdata('order_filter')['filter_tim']<12)
            {
                $filter_tim=$this->session->userdata('order_filter')['filter_tim'] ;  $filter_aptim="AM";
            }
                 $parameter1 = array('act_mode' => 'S_vieworder',
                'Param1' => $this->session->userdata('order_filter')['branchids'],
                'Param2' => $this->session->userdata('order_filter')['filter_name'],
                'Param3' => $this->session->userdata('order_filter')['filter_email'],
                'Param4' => $this->session->userdata('order_filter')['filter_ticket'],
                'Param5' => $this->session->userdata('order_filter')['filter_mobile'],
                'Param6' => $this->session->userdata('order_filter')['filter_payment'],
                'Param7' => $this->session->userdata('order_filter')['filter_date_booking_from'],
                'Param8' => $this->session->userdata('order_filter')['filter_date_booking_to'],
                'Param9' => $this->session->userdata('order_filter')['filter_date_session_from'],
                'Param10' => $this->session->userdata('order_filter')['filter_date_session_to'],
                 'Param11' => $this->session->userdata('order_filter')['filter_status'],
                'Param12' => $this->session->userdata('order_filter')['filter_date_printed_from'],
                'Param13' => $this->session->userdata('order_filter')['filter_date_printed_to'],
                'Param14' => $this->session->userdata('order_filter')['filter_date_ticket'],
                'Param15' => $this->session->userdata('order_filter')['filter_date_session'],
                'Param16' => $filter_tim,
                'Param17' => $this->session->userdata('order_filter')['filter_sta'],
                'Param18' => '',
                'Param19' => '');
            foreach($parameter1 as $key=>$val){
                if($parameter1[$key] == '')
                {
                    $parameter1[$key] =-1;
                }
                //echo $parameter1[$key];
            }
           // p($parameter1);
            $response['vieww_order'] = $this->supper_admin->call_procedure('proc_order_filter_s', $parameter1);
            //p($response['vieww_order']);
            $this->session->unset_userdata('order_filter');


        }
        else {
            $parameter1 = array('act_mode' => 'S_vieworder',
                'Param1' => '',
                'Param2' => '',
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');
           
            $response['vieww_order'] = $this->supper_admin->call_procedure('proc_order_s', $parameter1);
         //pend($response['vieww_order']);

        }

        $parameter_time = array('act_mode' => 'branch_list_for_filter',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' =>'',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' =>'',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',
            'Param16' => '',
            'Param17' => '',
            'Param18' => '',
            'Param19' => '');
        $response['vieww_time'] = $this->supper_admin->call_procedure('proc_order_filter_s', $parameter_time);

        $parameter_status = array('act_mode' => 'status_list_for_filter',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' =>'',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' =>'',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',
            'Param16' => '',
            'Param17' => '',
            'Param18' => '',
            'Param19' => '');
        $response['vieww_status'] = $this->supper_admin->call_procedure('proc_order_filter_s',$parameter_status);
//pend($response['vieww_status']);
        $parameter2 = array( 'act_mode'=>'s_viewbranch',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);

		
		$siteurl = base_url();
        $parameterbranch = array(
            'act_mode' => 'selectbranch',
            'weburl' => $siteurl,


        );
        $response['branch'] = $this->supper_admin->call_procedurerow('proc_select_branch_v', $parameterbranch);
//select banner images
        $parameterbanner = array(
            'act_mode' => 'selectbannerimages',
            'branchid' =>  $response['branch']->branch_id,


        );
        $response['banner'] = $this->supper_admin->call_procedurerow('proc_select_banner_v', $parameterbanner);
     $parametertearms = array(
            'act_mode' => 'selecttearms',
            'branchid' => $response['branch']->branch_id,


        );
		
        $response['tearmsgatway'] = $this->supper_admin->call_procedure('proc_select_banner_v', $parametertearms);

       //pend($response);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('order/orderlist',$response);
    }

    /*for change status*/
    public function op_ticket_print_status()
    {
        $parameter1 = array('act_mode' => 'op_ticket_print_status',
            'Param1' => $this->input->post('pacorderid'),
            'Param2' => $this->input->post('op_ticket_print_status'),
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter1);
        $response['s'] = $this->supper_admin->call_procedure('proc_order_s', $parameter1);
        print_r(json_encode((array)$response));
    }

    /*for pending order*/
    public function pendingorder()
    {
		if(getMemberId()==1){

		}else{

			redirect('admin/login/dashboard?empid='.$_GET['empid'].'&uid='.$_GET['uid'].'');
		}
        $parameter1 = array('act_mode' => 'S_vieworder',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww_order'] = $this->supper_admin->call_procedure('proc_order_s', $parameter1);
    //  p( $response['vieww_order']);


        //pend($response);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('order/pendingorderlist',$response);
    }
	
	
	public function orderPrint() {
		
		
        $this->load->view('order/print_template',$data);
	}
	
	
	

    public function sucessgorder()
    {
if(getMemberId()==1)

{

}
else
{
  redirect('admin/login/dashboard?empid='.$_GET['empid'].'&uid='.$_GET['uid'].'');
}
    


 if($this->session->userdata('order_filter'))
        {
            //p($this->session->userdata('order_filter'));
            $parameter1 = array('act_mode' => 'S_vieworder',
                'Param1' => $this->session->userdata('order_filter')['branchids'],
                'Param2' => $this->session->userdata('order_filter')['filter_name'],
                'Param3' => $this->session->userdata('order_filter')['filter_email'],
                'Param4' => $this->session->userdata('order_filter')['filter_ticket'],
                'Param5' => $this->session->userdata('order_filter')['filter_mobile'],
                'Param6' => $this->session->userdata('order_filter')['filter_payment'],
                'Param7' => $this->session->userdata('order_filter')['filter_date_booking_from'],
                'Param8' => $this->session->userdata('order_filter')['filter_date_booking_to'],
                'Param9' => $this->session->userdata('order_filter')['filter_date_session_from'],
                'Param10' => $this->session->userdata('order_filter')['filter_date_session_to'],
                'Param11' => $this->session->userdata('order_filter')['filter_status'],
                'Param12' => $this->session->userdata('order_filter')['filter_date_printed_from'],
                'Param13' => $this->session->userdata('order_filter')['filter_date_printed_to'],
                'Param14' => $this->session->userdata('order_filter')['filter_date_ticket'],
                'Param15' => $this->session->userdata('order_filter')['filter_date_session'],
                'Param16' => $this->session->userdata('order_filter')['filter_tim'],
                'Param17' => $this->session->userdata('order_filter')['filter_sta'],
                'Param18' => '',
                'Param19' => '');
            foreach($parameter1 as $key=>$val){
                if($parameter1[$key] == '')
                {
                    $parameter1[$key] =-1;
                }
                //echo $parameter1[$key];
            }
            // p($parameter1);
            $response['vieww_order'] = $this->supper_admin->call_procedure('proc_order_filter_s', $parameter1);
            //p($response['vieww_order']);
            $this->session->unset_userdata('order_filter');


        }
        else {
          $parameter1 = array('act_mode' => 'S_vieworder',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww_order'] = $this->supper_admin->call_procedure('proc_order_s', $parameter1);

        }

$siteurl = base_url();
        $parameterbranch = array(
            'act_mode' => 'selectbranch',
            'weburl' => $siteurl,


        );
        $response['branch'] = $this->supper_admin->call_procedurerow('proc_select_branch_v', $parameterbranch);



//select banner images
        $parameterbanner = array(
            'act_mode' => 'selectbannerimages',
            'branchid' =>  $response['branch']->branch_id,


        );
        $response['banner'] = $this->supper_admin->call_procedurerow('proc_select_banner_v', $parameterbanner);

        $parametertearms = array(
            'act_mode' => 'selecttearms',
            'branchid' => $response['branch']->branch_id,


        );
        $response['tearmsgatway'] = $this->supper_admin->call_procedure('proc_select_banner_v', $parametertearms);


   
        //pend($response);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('order/sucessgorder',$response);
    }
    public function sucesspendingprinting()
    {
    if(getMemberId()==1)
            {

            }
        else
        {
          redirect('admin/login/dashboard?empid='.$_GET['empid'].'&uid='.$_GET['uid'].'');
        }
        $parameter1 = array('act_mode' => 'S_vieworder',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww_order'] = $this->supper_admin->call_procedure('proc_order_s', $parameter1);
        //pend($response);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('order/sucesspendingprinting',$response);
    }
    public function sucessgordercheck()
    {
        //pend($_POST);
        $parameter1 = array('act_mode' => 'update_tracking',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww_order'] = $this->supper_admin->call_procedure('proc_order_s', $parameter1);
        //pend($response);
    }
    
    public function s_print()
    {
    echo "<pre>";
	print_r($this->session);
//    $this->session->set_userdata('dsadasd','asdasdasd');
//    p($this->session->all_userdata());
        
    }
    


public function order_printpdf()
    {
        //Select branch
        $siteurl = base_url();
        $parameterbranch = array(
            'act_mode' => 'selectbranch',
            'weburl' => $siteurl,
        );
        $response['branch'] = $this->supper_admin->call_procedurerow('proc_select_branch_v', $parameterbranch);

//select banner images
        $parameterbanner = array(
            'act_mode' => 'selectbannerimages',
            'branchid' => $response['branch']->branch_id,
          );
        $response['banner'] = $this->supper_admin->call_procedurerow('proc_select_banner_v', $parameterbanner);


        $orderdisplay = array(
            'act_mode' => 'select_order',
            'orderid' => base64_decode($_GET['ordid']),
    );
 $response['orderdisplaydataval'] = $this->supper_admin->call_procedurerow('proc_order_v', $orderdisplay);





        $from_email = $response['banner']->bannerimage_from;
        //  $to_email = $this->input->post('email');




        $date_array1 = explode("-", $response['orderdisplaydataval']->addedon); // split the array
        $var_day1 = $date_array1[2]; //day seqment
        $var_month1 = $date_array1[1]; //month segment
        $var_year1 = $date_array1[0]; //year segment
        $new_date_format1 = strtotime("$var_year1-$var_month1-$var_day1"); // join them together

        $d1 = date(' jS F Y', $new_date_format1);


        $date = '19:24:15 ';
        $d2 = date('h:i:s a ');

        $date_array = explode("/", $response['orderdisplaydataval']->departuredate); // split the array

          $var_day = $date_array[0]; //day seqment
        $var_month = $date_array[1]; //month segment
        $var_year =  rtrim($date_array[2]," "); //year segment
        $new_date_format = strtotime("$var_year-$var_month-$var_day"); // join them together
        $input = ("$var_year$var_month$var_day");

        $d3 = date("D", strtotime($input)) . "\n";


        //$d4= date(' jS F Y', $new_date_format);

         $row['date'] = trim($var_year, " ") . "-" . $var_month . "-" . $var_day; // this is for example only - comment out when tested
        $d4 = date("F j, Y", strtotime($row['date']));

//echo date("H:i:s") . "\n";)

        $to_email = $response['orderdisplaydataval']->billing_email;
          $rrrval = $response['orderdisplaydataval']->pacorderid;
          $rproduct = $response['orderdisplaydataval']->packproductname;


     $mess = '<table width="90%" style="line-height: 28px; font-family: sans-serif;" >
<tr><td>Dear  ' . preg_replace('/[^A-Za-z0-9\-]/', '', $response['orderdisplaydataval']->billing_name) . ',</td></tr>

<tr><td>Greetings from ' . $response['banner']->bannerimage_top3 . '.!</td></tr>

<tr><td>
Thank you for choosing our Services. The  details are as follows:
Transaction ID:' . $response['orderdisplaydataval']->ticketid . '
</td></tr>
<tr><td>All guests are requested to report 30 minutes prior to the Session Time and collect your Entry Pass from the Ticket Counter.</td></tr>

<tr><td>We request the guest to carry a print out of this E &minus; voucher if possible. Should you have any queries, please feel free to write to us on ' . $response['banner']->bannerimage_branch_email . ' or call us on ' . $response['banner']->bannerimage_branch_contact . '.</td></tr>


<tr><td>We look forward to welcoming you the next time you visit us at ' . $response['banner']->bannerimage_top3 . '.</td></tr>




<tr><td>Yours sincerely,<br>
' . $response['banner']->bannerimage_top3 . ' Team</td></tr>
</table>
';

        $from_email = $response['banner']->bannerimage_from;

        $date_array1 = explode("-", $response['orderdisplaydataval']->addedon); // split the array
        $var_day1 = $date_array1[2]; //day seqment
        $var_month1 = $date_array1[1]; //month segment
        $var_year1 = rtrim($date_array1[0]," "); //year segment
        $new_date_format1 = strtotime("$var_year1-$var_month1-$var_day1"); // join them together

        $d1 = date(' jS F Y', $new_date_format1);


        $date = '19:24:15 ';
        $d2 = date('h:i:s a ');

        $date_array = explode("/", $response['orderdisplaydataval']->departuredate); // split the array
        $var_day = $date_array[0]; //day seqment
        $var_month = $date_array[1]; //month segment
        $var_year = $var_year1 = rtrim($date_array[2]," ");  //year segment
        $new_date_format = strtotime("$var_year-$var_month-$var_day"); // join them together

        $input = ("$var_year$var_month$var_day");

        $d3 = date("D", strtotime($input)) . "\n";


        $d4 = date(' jS F Y', $new_date_format);


        if ($response['timeslotses']->timeslot_from > 12) {
            $from_session_time = ($response['timeslotses']->timeslot_from - 12) . ":" . $response['timeslotses']->timeslot_minfrom . 'PM';
        } else {
            $from_session_time = $response['timeslotses']->timeslot_from . ":" . $response['timeslotses']->timeslot_minfrom . "AM";
        }

        if ($response['timeslotses']->timeslot_to > 12) {
            $to_session_time = ($response['timeslotses']->timeslot_to - 12) . ":" . $response['timeslotses']->timeslot_minto . $response['orderdisplaydataval']->txttod;
        } else {
            $to_session_time = $response['timeslotses']->timeslot_to . ":" . $response['timeslotses']->timeslot_minto . $response['orderdisplaydataval']->txttod;
        }

        $pdf_name=$response['orderdisplaydataval']->billing_pdf;
//echo (FCPATH . "assets/admin/pdfstore/" . $pdf_name);;
            //Load email library
        $this->load->library('email');
        $this->email->from($from_email, $response['banner']->bannerimage_top3);
        $this->email->reply_to($from_email, $response['banner']->bannerimage_top3);
        $this->email->to($to_email);
        $this->email->$response['banner']->bannerimage_apikey;
        $this->email->subject('' . $response['banner']->bannerimage_top3 . ' - Booking Confirmation Voucher ');
        $this->email->message($mess);
        $this->email->attach(FCPATH . "assets/admin/pdfstore/" . $pdf_name);
        //Send mail


        $this->email->send();

        redirect('admin/ordercontrol/sucessgorder?empid='.$_GET['empid'].'&uid='.$_GET['uid'].'');



       // $this->load->view('order/printingpdf',$response);

    }


 public function sendmail(){

    if($this->input->post('send') == 'Send'){
       // p($_POST);exit;
      $emailList =  $this->input->post('emailid');
       $agentid =  $this->input->post('agentid');
       $orderid = $this->input->post('orderid'); 
       foreach ($emailList as $emailid_to) {
       //Select branch
        $siteurl = base_url();
        $parameterbranch = array(
            'act_mode' => 'selectbranch',
            'weburl' => $siteurl,
        );
        $response['branch'] = $this->supper_admin->call_procedurerow('proc_select_branch_v', $parameterbranch);

        //select banner images
        $parameterbanner = array(
            'act_mode' => 'selectbannerimages',
            'branchid' => $response['branch']->branch_id,
          );
        $response['banner'] = $this->supper_admin->call_procedurerow('proc_select_banner_v', $parameterbanner);


        $orderdisplay = array(
            'act_mode' => 'select_order',
            'orderid' => $orderid,
       );
      $response['orderdisplaydataval'] = $this->supper_admin->call_procedurerow('proc_order_v', $orderdisplay);

        $from_email = $response['banner']->bannerimage_from; 
        //  $to_email = $this->input->post('email');\
        $date_array1 = explode("-", $response['orderdisplaydataval']->addedon); // split the array
        $var_day1 = $date_array1[2]; //day seqment
        $var_month1 = $date_array1[1]; //month segment
        $var_year1 = $date_array1[0]; //year segment
        $new_date_format1 = strtotime("$var_year1-$var_month1-$var_day1"); // join them together

        $d1 = date(' jS F Y', $new_date_format1);


        $date = '19:24:15 ';
        $d2 = date('h:i:s a ');

        $date_array = explode("/", $response['orderdisplaydataval']->departuredate); // split the array

          $var_day = $date_array[0]; //day seqment
        $var_month = $date_array[1]; //month segment
        $var_year =  rtrim($date_array[2]," "); //year segment
        $new_date_format = strtotime("$var_year-$var_month-$var_day"); // join them together
        $input = ("$var_year$var_month$var_day");

        $d3 = date("D", strtotime($input)) . "\n";


        //$d4= date(' jS F Y', $new_date_format);

         $row['date'] = trim($var_year, " ") . "-" . $var_month . "-" . $var_day; // this is for example only - comment out when tested
        $d4 = date("F j, Y", strtotime($row['date']));

        $to_email = $response['orderdisplaydataval']->billing_email;
          $rrrval = $response['orderdisplaydataval']->pacorderid;
          $rproduct = $response['orderdisplaydataval']->packproductname;

      $mess = '<table width="90%" style="line-height: 28px; font-family: sans-serif;" >
        <tr><td>Dear  ' . preg_replace('/[^A-Za-z0-9\-]/', '', $response['orderdisplaydataval']->billing_name) . ',</td></tr>
        <tr><td>Greetings from ' . $response['banner']->bannerimage_top3 . '.!</td></tr>
        <tr><td>
        Thank you for choosing our Services. The  details are as follows:
        Transaction ID:' . $response['orderdisplaydataval']->ticketid . '
        </td></tr>
        <tr><td>All guests are requested to report 30 minutes prior to the Session Time and collect your Entry Pass from the Ticket Counter.</td></tr>
        <tr><td>We request the guest to carry a print out of this E &minus; voucher if possible. Should you have any queries, please feel free to write to us on ' . $response['banner']->bannerimage_branch_email . ' or call us on ' . $response['banner']->bannerimage_branch_contact . '.</td></tr>
        <tr><td>We look forward to welcoming you the next time you visit us at ' . $response['banner']->bannerimage_top3 . '.</td></tr>
        <tr><td>Yours sincerely,<br>
        ' . $response['banner']->bannerimage_top3 . ' Team</td></tr>
        </table>
        ';

        $from_email = $response['banner']->bannerimage_from;

        $date_array1 = explode("-", $response['orderdisplaydataval']->addedon); // split the array
        $var_day1 = $date_array1[2]; //day seqment
        $var_month1 = $date_array1[1]; //month segment
        $var_year1 = rtrim($date_array1[0]," "); //year segment
        $new_date_format1 = strtotime("$var_year1-$var_month1-$var_day1"); // join them together

        $d1 = date(' jS F Y', $new_date_format1);


        $date = '19:24:15 ';
        $d2 = date('h:i:s a ');

        $date_array = explode("/", $response['orderdisplaydataval']->departuredate); // split the array
        $var_day = $date_array[0]; //day seqment
        $var_month = $date_array[1]; //month segment
        $var_year = $var_year1 = rtrim($date_array[2]," ");  //year segment
        $new_date_format = strtotime("$var_year-$var_month-$var_day"); // join them together

        $input = ("$var_year$var_month$var_day");

        $d3 = date("D", strtotime($input)) . "\n";


        $d4 = date(' jS F Y', $new_date_format);


        if ($response['timeslotses']->timeslot_from > 12) {
            $from_session_time = ($response['timeslotses']->timeslot_from - 12) . ":" . $response['timeslotses']->timeslot_minfrom . 'PM';
        } else {
            $from_session_time = $response['timeslotses']->timeslot_from . ":" . $response['timeslotses']->timeslot_minfrom . "AM";
        }

        if ($response['timeslotses']->timeslot_to > 12) {
            $to_session_time = ($response['timeslotses']->timeslot_to - 12) . ":" . $response['timeslotses']->timeslot_minto . $response['orderdisplaydataval']->txttod;
        } else {
            $to_session_time = $response['timeslotses']->timeslot_to . ":" . $response['timeslotses']->timeslot_minto . $response['orderdisplaydataval']->txttod;
        }
        $pdf_name=$response['orderdisplaydataval']->billing_pdf;

        $this->load->library('email');
        $this->email->from($from_email, $response['banner']->bannerimage_top3);
        $this->email->reply_to($from_email, $response['banner']->bannerimage_top3);
        $this->email->to($emailid_to);
        $this->email->$response['banner']->bannerimage_apikey;
        $this->email->subject('' . $response['banner']->bannerimage_top3 . ' - Booking Confirmation Voucher ');
        $this->email->message($mess);
        $this->email->attach(FCPATH . "assets/admin/pdfstore/" . $pdf_name);
        $this->email->send();
        
       }
       redirect('admin/ordercontrol/order?empid='.$_GET['empid'].'&uid='.$_GET['uid'].'');
   }
}


}// end class
?>