<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Timeslotspackages extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');


 $this->load->library('session');


    }

   



    public  function addTimeslotPackage(){
                
        if($this->input->post('submit'))
        {

            $branchid = $this->input->post('branchids');
            
            $start_date = $this->input->post('start_date');
            $from = $this->input->post('field_name1');
             $from_minute = $this->input->post('minute_name1');

            $to = $this->input->post('field_name2');
              $to_minute = $this->input->post('minute_name2');
             $package = $this->input->post('package');
             $no_of_seats = $this->input->post('no_of_seats');
      
      
 for ($i=0; $i<count($package); $i++) { 
 $va=explode('_', $package[$i]);
 foreach ($from as $key => $value) {
   if($key==$va[1])
   {
           
    $parameter=array(
        'act_mode'=>'addTimeslotPackage',
        'Param1'=>$va[0],
        'Param2'=>$from[$va[1]],
        'Param3'=>$from_minute[$va[1]],
        'Param4'=>$to[$va[1]],
        'Param5'=>$to_minute[$va[1]],
        'Param6'=>$start_date,
        'Param7'=>$no_of_seats[$va[1]],
        'Param8'=>$branchid,
        'Param9'=>'',
        'Param10'=>'',
        'Param11'=>'',
        'Param12'=>'',
        'Param13'=>'',
        'Param14'=>'',
        'Param15'=>'',
    );
    $response = $this->supper_admin->call_procedure('proc_timeslotspackages',$parameter);
            
                   
   }


}
       }             
                       
           

          
        }


        $parameter2 = array( 'act_mode'=>'s_viewbranch',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);

        $parameter3 = array( 'act_mode'=>'s_viewtimeslot',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewtimeslot'] = $this->supper_admin->call_procedure('proc_timeslot_s',$parameter3);
        //pend($response['s_viewtimeslot']);

        $parameter4 = array( 'act_mode'=>'s_viewpackage',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        //pend($parameter);
$response['vieww_pack'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter4);


 $parameter=array(
                        'act_mode'=>'ViewTimeslotPackage',
                        'Param1'=>$va[0],
                        'Param2'=>$from[$va[1]],
                        'Param3'=>$from_minute[$va[1]],
                        'Param4'=>$to[$va[1]],
                        'Param5'=>$to_minute[$va[1]],
                        'Param6'=>$start_date,
                        'Param7'=>$no_of_seats[$va[1]],
                        'Param8'=>$branchid,
                        'Param9'=>'',
                        'Param10'=>'',
                        'Param11'=>'',
                        'Param12'=>'',
                        'Param13'=>'',
                        'Param14'=>'',
                        'Param15'=>'',
                    );
    $response['ViewTimeslotPackage'] = $this->supper_admin->call_procedure('proc_timeslotspackages',$parameter);
    //p($response['ViewTimeslotPackage']);exit();

        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('timeslotspackages/addTimeslotPackage',$response);
    }






 public  function editTimeslotPackage(){

                
        if($this->input->post('submit'))
        {
$branchid = $this->input->post('branchids');
$start_date = $this->input->post('start_date');
$from = $this->input->post('field_name1');
$from_minute = $this->input->post('minute_name1');
$to = $this->input->post('field_name2');
$to_minute = $this->input->post('minute_name2');
$package = $this->input->post('package');
$no_of_seats = $this->input->post('no_of_seats');

 $parameter=array(
                        'act_mode'=>'UpdateTimeslotPackage',
                        'Param1'=>$package,
                        'Param2'=>$from,
                        'Param3'=>$from_minute,
                        'Param4'=>$to,
                        'Param5'=>$to_minute,
                        'Param6'=>$start_date,
                        'Param7'=>$no_of_seats,
                        'Param8'=>$branchid,
                        'Param9'=>$this->uri->segment(4),
                        'Param10'=>'',
                        'Param11'=>'',
                        'Param12'=>'',
                        'Param13'=>'',
                        'Param14'=>'',
                        'Param15'=>'',
                    );

 
$response = $this->supper_admin->call_procedure('proc_timeslotspackages',$parameter);
  $this->session->set_flashdata("message", "Your information was successfully update.");
  $url= '?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']);
        
      redirect("admin/timeslotspackages/addTimeslotPackage".$url);          
             
}
$parameter2 = array( 'act_mode'=>'s_viewbranch',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);

        $parameter3 = array( 'act_mode'=>'s_viewtimeslot',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewtimeslot'] = $this->supper_admin->call_procedure('proc_timeslot_s',$parameter3);
        //pend($response['s_viewtimeslot']);

        $parameter4 = array( 'act_mode'=>'s_viewpackage',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        //pend($parameter);
$response['vieww_pack'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter4);
//p($response['vieww_pack']);exit();


 $parameter=array(
                        'act_mode'=>'ViewTimeslotPackage',
                        'Param1'=>'',
                        'Param2'=>'',
                        'Param3'=>'',
                        'Param4'=>'',
                        'Param5'=>'',
                        'Param6'=>'',
                        'Param7'=>'',
                        'Param8'=>'',
                        'Param9'=>'',
                        'Param10'=>'',
                        'Param11'=>'',
                        'Param12'=>'',
                        'Param13'=>'',
                        'Param14'=>'',
                        'Param15'=>'',
                    );
    $response['ViewTimeslotPackage'] = $this->supper_admin->call_procedure('proc_timeslotspackages',$parameter);
 
 $parameter=array(
                        'act_mode'=>'EditTimeslotPackage',
                        'Param1'=>$this->uri->segment(4),
                        'Param2'=>'',
                        'Param3'=>'',
                        'Param4'=>'',
                        'Param5'=>'',
                        'Param6'=>'',
                        'Param7'=>'',
                        'Param8'=>'',
                        'Param9'=>'',
                        'Param10'=>'',
                        'Param11'=>'',
                        'Param12'=>'',
                        'Param13'=>'',
                        'Param14'=>'',
                        'Param15'=>''
                    );
 
    $response['EditTimeslotPackage'] = $this->supper_admin->call_procedureRow('proc_timeslotspackages',$parameter);
//p($response['EditTimeslotPackage']);exit();

        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('timeslotspackages/editTimeslotPackage',$response);
    }

    public function deleteTimeslotPackage(){

     $parameter=array(
                        'act_mode'=>'DeleteTimeslotPackage',
                        'Param1'=>'',
                        'Param2'=>'',
                        'Param3'=>'',
                        'Param4'=>'',
                        'Param5'=>'',
                        'Param6'=>'',
                        'Param7'=>'',
                        'Param8'=>'',
                        'Param9'=>$this->uri->segment(4),
                        'Param10'=>'',
                        'Param11'=>'',
                        'Param12'=>'',
                        'Param13'=>'',
                        'Param14'=>'',
                        'Param15'=>'',
                    );

 
$response = $this->supper_admin->call_procedure('proc_timeslotspackages',$parameter);
  $this->session->set_flashdata("message", "Your information was successfully update.");

  $url= '?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']);
      redirect("admin/timeslotspackages/addTimeslotPackage".$url);          
             
    }
}// end class
?>