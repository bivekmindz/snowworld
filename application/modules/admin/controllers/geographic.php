<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Geographic extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
 $this->load->library('session');
 $this->load->helper('adminmenu_helper');
    }

//---------------------- Country  -------------------------//

    public function add_country()
    {
        //pend($_POST);
        $name = $this->input->post('couname');
        $code = $this->input->post('coucode');
        $parameter = array('act_mode' => 'countryinsert', 'row_id' => '', 'counname' => $name, 'coucode' => $code, 'commid' => '');
        $response = $this->supper_admin->call_procedure('proc_geographic', $parameter);

    }

//---------------------- Country View  -------------------------//
    public function viewcountry()
    {
        $this->load->library('pagination'); // load pagination library.

        $parameter = array('act_mode' => 'viewcountry', 'row_id' => '', 'counname' => '', 'coucode' => '', 'commid' => '');
        $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);

        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('geographic/viewcountry', $response);

    }

//---------------------- Country Update -------------------------//
    public function countryupdate()
    {
        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $code = $this->input->post('code');
        $parameter = array('act_mode' => 'countryupdate', 'row_id' => $id, 'counname' => $name, 'coucode' => $code, 'commid' => '');
        //pend($parameter);
        $response = $this->supper_admin->call_procedure('proc_geographic', $parameter);
    }

//---------------------- Country Delete  -------------------------//

//---------------------- Country Status  -------------------------//
    public function countrystatus()
    {
        $rowid = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $act_mode = $status == '1' ? 'activeandinactive' : 'inactiveandinactive';
        $parameter = array('act_mode' => $act_mode, 'row_id' => $rowid, 'counname' => '', 'coucode' => '', 'commid' => '');
        $response = $this->supper_admin->call_procedure('proc_geographic', $parameter);
    }

//---------------------- Country Name Check  -------------------------//
    public function checkname()
    {
        $couname = $this->input->post('name');
        $parameter = array('act_mode' => 'checkcoun', 'row_id' => '', 'counname' => $couname, 'coucode' => '', 'commid' => '');
        $data = $this->supper_admin->call_procedure('proc_geographic', $parameter);
        echo json_encode($data);
    }
//---------------------- End Country  ----------------------------------//

//----------------------  City Add  ----------------------------------//
    public function add_city()
    {
        $stateid = $this->input->post('stateid_forcity');
        $cityname = $this->input->post('cityname_forcity');
        $parameter = array('act_mode' => 'cityinsert', 'row_id' => $stateid, 'counname' => $cityname, 'coucode' => '', 'commid' => '');
        $response = $this->supper_admin->call_procedure('proc_geographic', $parameter);

    }

//---------------------- Country State  -------------------------//
    public function countrystate()
    {
        $countryid = $this->input->post('countryid');
        $parameter = array('act_mode' => 'countrystate', 'row_id' => $countryid, 'counname' => '', 'coucode' => '', 'commid' => '');
        $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);
        //p($response['vieww']);exit();
        $str = '';

        foreach ($response['vieww'] as $k => $v) {
            $str .= "<option value=" . $v->stateid . ">" . $v->statename . "</option>";
        }
        echo $str;
    }

    public function statecity()
    {
        $stateid = $this->input->post('stateid');

        $response['city'] = $this->db->get_where(tbl_city, array('stateid' => $stateid, 'cstatus' => '1'))
            ->result();


// $response['vieww']  = $this->supper_admin->call_procedure('proc_geographic',$parameter);
        //p($response['vieww']);exit();
        $str = '';

        foreach ($response['city'] as $k => $v) {

            $str .= "<option value=" . $v->cityid . ">" . $v->cityname . "</option>";
        }

        echo $str;
    }

//---------------------- View City  -------------------------//
    public function viewcity()
    {

        $parameter = array('act_mode' => 'viewcity', 'row_id' => '', 'counname' => '', 'coucode' => '', 'commid' => '');
        $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);

        //----------------  start pagination ------------------------//

        $config['base_url'] = base_url() . "admin/geographic/viewcity?";
        $config['total_rows'] = count($response['vieww']);
        $config['per_page'] = 50;
        $config['use_page_numbers'] = TRUE;

        $this->pagination->initialize($config);
        if ($_GET['page']) {
            $page = $_GET['page'] - 1;
            $page = ($page * 50);
            $second = $config['per_page'];
        } else {
            $page = 0;
            $second = $config['per_page'];
        }

        $str_links = $this->pagination->create_links();
        $response["links"] = explode('&nbsp;', $str_links);

        $parameter = array('act_mode' => 'viewcity', 'row_id' => $page, 'counname' => '', 'coucode' => '', 'commid' => $second);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);

        //----------------  end pagination ------------------------//

        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('geographic/viewcity', $response);
    }

    public function countrydel_new($id)
    {
        $parameter = array('act_mode' => 'countrydelete', 'row_id' => $id, 'counname' => '', 'coucode' => '', 'commid' => '');
        $response = $this->supper_admin->call_procedure('proc_geographic', $parameter);
    }

//---------------------- City Delete  -------------------------//
    public function citydelete($id)
    {
        $parameter = array('act_mode' => 'citydelete', 'row_id' => $id, 'counname' => '', 'coucode' => '', 'commid' => '');
        $response = $this->supper_admin->call_procedure('proc_geographic', $parameter);
    }

//---------------------- City Status  -------------------------//
    public function citystatus()
    {
        $rowid = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $act_mode = $status == '1' ? 'activecity' : 'inactivecity';
        $parameter = array('act_mode' => $act_mode, 'row_id' => $rowid, 'counname' => '', 'coucode' => '', 'commid' => '');
        $response = $this->supper_admin->call_procedure('proc_geographic', $parameter);
    }

//---------------------- City Update -------------------------//
    public function cityupdate($id)
    {
        $parameter = array('act_mode' => 'cityupdate',
                            'row_id' => $this->input->post('id'),
                            'cname' =>  $this->input->post('cname'),
                            'coucode' =>  $this->input->post('sid'),
                            'commid' => '');
        //pend($parameter);
        $response = $this->supper_admin->call_procedure('proc_geographic', $parameter);
    }


    //---------------------- location Update -------------------------//
    public function location_update_modal($id)
    {
        $parameter = array('act_mode' => 'loc_update_modal',
            'Param1' => $this->input->post('id'),
            'Param2' => $this->input->post('cityid'),
            'Param3' => $this->input->post('lname'),
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response = $this->supper_admin->call_procedure('proc_location_v', $parameter);
    }

//---------------------- City Name Check  -------------------------//
    public function checkcityname()
    {
        $couname = $this->input->post('name');
        $parameter = array('act_mode' => 'checkcitycoun', 'row_id' => '', 'counname' => $couname, 'coucode' => '', 'commid' => '');
        $data = $this->supper_admin->call_procedure('proc_geographic', $parameter);
        echo json_encode($data);
    }

//---------------------- State Add  -------------------------//
    public function addstate()
    {
        $parameter = array(
            'act_mode' => 'stateexist',
            'row_id' => $this->input->post('countryid'),
            'counname' => $this->input->post('statename'),
            'coucode' => $this->input->post('statecode'),
            'commid' => ''
        );
        $record['record'] = $this->supper_admin->call_procedureRow('proc_geographic', $parameter);
        if ($record['record']->statecount > 0) {
            echo "0";
            exit();
        } else {
            $parameter = array(
                'act_mode' => 'stateinsert',
                'row_id' => $this->input->post('countryid'),
                'counname' => $this->input->post('statename'),
                'coucode' => $this->input->post('statecode'),
                'commid' => ''
            );

            $record = $this->supper_admin->call_procedure('proc_geographic', $parameter);
        }

    }

//---------------------- State List  -------------------------//
    public function statelist()
    {


        $parameter3 = array(
            'act_mode' => 'viewstate',
            'row_id' => '',
            'counname' => '',
            'coucode' => '',
            'commid' => ''
        );
        $record['data'] = $this->supper_admin->call_procedure('proc_geographic', $parameter3);

        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('geographic/statelist', $record);
    }

//---------------------- State Delete  -------------------------//
    public function statedelete($id)
    {
        $parameter = array('act_mode' => 'statedelete', 'row_id' => $id, 'counname' => '', 'coucode' => '', 'commid' => '');
        $response = $this->supper_admin->call_procedure('proc_geographic', $parameter);
        $this->session->set_flashdata('message', 'Your information was successfully deleted.');
        redirect('admin/geographic/statelist');
    }

//---------------------- State Status  -------------------------//
    public function statestatus()
    {
        $rowid = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $act_mode = $status == '1' ? 'stateactiveandinactive' : 'stateinactiveandinactive';
        $parameter = array('act_mode' => $act_mode, 'row_id' => $rowid, 'counname' => '', 'coucode' => '', 'commid' => '');
        //pend($parameter);
        $response = $this->supper_admin->call_procedure('proc_geographic', $parameter);
    }

//---------------------- State Update  -------------------------//
    public function stateupdate($id)
    {
        $parameter = array(
            'act_mode' => 'stateupdate',
            'row_id' => $this->input->post('id'),
            'counname' => $this->input->post('sid'),
            'coucode' => $this->input->post('scode'),
            'commid' => $this->input->post('cid'),
        );
        $record = $this->supper_admin->call_procedure('proc_geographic', $parameter);

    }
//---------------------- end state ----------------------------//
//---------------------- Location  List  -------------------------//
    public function locationlist()
    {


        $record['data'] = $this->db->get('tbl_location')->result();

        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('geographic/locationlist', $record);
    }

//----------------------  City Add  ----------------------------------//
    public function addlocation()
    {
        $data = array(
            'countryid' => $_POST['countryid_forlocation'],
            'stateid' => $_POST['stateid_forlocation'],
            'cityid' => $_POST['cityid_forlocation'],
            'createdon' => date("Y-m-d"),
            'lstatus' => '1',
            'locationname' => $_POST['name_forlocation']
        );
        $this->db->insert('tbl_location', $data);
    }

//---------------------- Location Status  -------------------------//
    public function locationstatus()
    {
        $rowid = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $act_mode = $status == '1' ? 'act_to_de' : 'de_to_act';
        $parameter = array('act_mode' => $act_mode, 'row_id' => $rowid, 'counname' => '', 'coucode' => '', 'commid' => '');
        $response = $this->supper_admin->call_procedure('proc_geographic', $parameter);
    }


    //---------------------- Location Update -------------------------//
    public function locationupdate($id)
    {
        if ($this->uri->segment('4') != '') {
            $parameter = array('act_mode' => 'vieweditlocation',
                'Param1' => '',
                'Param2' => '',
                'Param3' => $this->uri->segment('4'),
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');
            $response['update_data'] = $this->supper_admin->call_procedure('proc_location_v', $parameter);
        }

        if ($this->input->post('submit')) {
            $name = $this->input->post('locationname');
            $seat = $this->input->post('totalseatperslot');
            $parameter = array('act_mode' => 'locationupdatedata',
                'Param1' => $name,
                'Param2' => $seat,
                'Param3' => $id,
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '',


            );


            $response = $this->supper_admin->call_procedure('proc_location_v', $parameter);

            $this->session->set_flashdata('message', 'Your information was successfully Update.');
            redirect('admin/geographic/locationlist');
        }

        $parameter = array('act_mode' => 'vieweditlocation', 'Param1' => '', 'Param2' => '', 'Param3' => $id, 'Param4' => '', 'Param5' => '', 'Param6' => '', 'Param7' => '', 'Param8' => '', 'Param9' => '');

        $response['vieww'] = $this->supper_admin->call_procedureRow('proc_location_v', $parameter);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('geographic/editlocation', $response);
    }

//---------------------- location Delete  -------------------------//
    public function locationdelete($id)
    {
        $parameter = array('act_mode' => 'locationdelete', 'row_id' => $id, 'counname' => '', 'coucode' => '', 'commid' => '');
        $response = $this->supper_admin->call_procedure('proc_geographic', $parameter);
    }

//---------------------- end Location ----------------------------//

//............. Add city group Master ............... //
    public function addcitygroup()
    {

        if ($this->input->post('submit')) {
            foreach ($this->input->post('cityidd') as $key => $value) {
                $parameter = array('act_mode' => 'citygcheck', 'row_id' => '', 'counname' => $this->input->post('stateid'), 'coucode' => '', 'commid' => $value);
                $record['record'] = $this->supper_admin->call_procedureRow('proc_geographic', $parameter);
                if ($record['record']->citycount > 0) {
                    $this->session->set_flashdata("message", "City Already Exists");
                    redirect("admin/geographic/addcitygroup");
                }
                $parameter = array('act_mode' => 'cityginsert', 'row_id' => $this->input->post('citygroup'), 'counname' => $this->input->post('stateid'), 'coucode' => '', 'commid' => $value);

                $record['record'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);
            }

            $this->session->set_flashdata('message', 'Your information was successfully Updated.');
            redirect('admin/geographic/viewcitygroup');
        }

        $parameter = array('act_mode' => 'citymasterview', 'row_id' => '', 'counname' => '', 'coucode' => '', 'commid' => '');
        $responce['vieww'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);
        $parameter = array('act_mode' => 'citygview', 'row_id' => '', 'counname' => '', 'coucode' => '', 'commid' => '');
        $responce['cityvieww'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);
        $parameter = array('act_mode' => 'viewcountry', 'row_id' => '', 'counname' => '', 'coucode' => '', 'commid' => '');
        $responce['viewcountry'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('geographic/addcitygroup', $responce);
    }

//............. View Brand Master ............... //
    public function viewcitygroup()
    {

        if ($this->input->post('submit')) {
            foreach ($this->input->post('attdelete') as $key => $value) {
                $parameter = array('act_mode' => 'cgdelete', 'row_id' => $value, 'counname' => '', 'coucode' => '', 'commid' => '');
                $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);

            }
            $this->session->set_flashdata("message", "Your information was successfully delete.");
            redirect("admin/geographic/viewcitygroup");
        }
        $parameter = array('act_mode' => 'citgview', 'row_id' => '', 'counname' => '', 'coucode' => '', 'commid' => '');
        $responce['cityview'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);

        //----------------  start pagination ------------------------//

        $config['base_url'] = base_url() . "admin/geographic/viewcitygroup?";
        $config['total_rows'] = count($responce['cityview']);
        $config['per_page'] = 50;
        $config['use_page_numbers'] = TRUE;

        $this->pagination->initialize($config);
        if ($_GET['page']) {
            $page = $_GET['page'] - 1;
            $page = ($page * 50);
            $second = $config['per_page'];
        } else {
            $page = 0;
            $second = $config['per_page'];
        }

        $str_links = $this->pagination->create_links();
        $responce["links"] = explode('&nbsp;', $str_links);

        $parameter = array('act_mode' => 'citgview', 'row_id' => $page, 'counname' => '', 'coucode' => '', 'commid' => $second);
        $responce['cityview'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);

        //----------------  end pagination ------------------------//
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('geographic/viewcitygroup', $responce);

    }

//............. Delete Brand Master ............... //
    public function citygroupdelete($id)
    {
        $parameter = array('act_mode' => 'cgdelete', 'row_id' => $id, 'counname' => '', 'coucode' => '', 'commid' => '');
        $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);
        $this->session->set_flashdata("message", "Your information was successfully delete.");
        redirect("admin/geographic/viewcitygroup");
    }


//............. End Brand ............... //

    public function getlocation()
    {


        //$this->load->view('helper/header');

        $this->load->view('geographic/getlocation');

    }

    public function userlocation()
    {

        if (!empty($_POST['latitude']) && !empty($_POST['longitude'])) {
            //Send request and receive json data by latitude and longitude
            $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($_POST['latitude']) . ',' . trim($_POST['longitude']) . '&sensor=falsekey=AIzaSyDULV7j6QbDAPEpmQd76P-lFkHwwARmAQ4';
            $json = @file_get_contents($url);
            $data = json_decode($json);
            $status = $data->status;
            if ($status == "OK") {
                //Get address from json data
                $location = $data->results[0]->formatted_address;
            } else {
                $location = '';
            }
            //Print address
            echo $location;
        }

    }

    /*
    public function userlocation(){

      if(!empty($_POST['latitude']) && !empty($_POST['longitude'])){
        //Send request and receive json data by latitude and longitude
        $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($_POST['latitude']).','.trim($_POST['longitude']).'&sensor=false&key=AIzaSyDULV7j6QbDAPEpmQd76P-lFkHwwARmAQ4';
        $json = @file_get_contents($url);
        $data = json_decode($json);
        $status = $data->status;
        if($status=="OK"){
            //Get address from json data
            $location = $data->results[0]->formatted_address;
        }else{
            $location =  '';
        }
        //Print address
        echo $location;
    }

    }
    */


    public function s_promo()
    {
        $parameter = array('act_mode' => 'viewcountry', 'row_id' => '', 'counname' => '', 'coucode' => '', 'commid' => '');
        $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('geographic/s_promo', $response);


    }

    public function citylocation()
    {
        $parameter = array('act_mode' => 'citylocation',
            'Param1' => $this->input->post('cityid'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww'] = $this->supper_admin->call_procedure('proc_location_v', $parameter);
        //p($response['vieww']);

        foreach ($response['vieww'] as $a) {

            echo "<option value=" . $a->locationid . ">" . $a->locationname . "</option>";


        }

    }

    public function addpromo()
    {
        $this->form_validation->set_rules('locationid', 'locationid', 'required');
        $this->form_validation->set_rules('promocode_name', 'promocode_name', 'required');
        $this->form_validation->set_rules('promo', 'promo', 'required');
        $this->form_validation->set_rules('promonumber', 'promonumber', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('helper/header');
            $this->load->view('helper/nav');
            $this->load->view('geographic/s_promo');
        } else {
            $parameter = array('act_mode' => 'insert_promo',
                'Param1' => $this->input->post('locationid'),
                'Param2' => $this->input->post('promocode_name'),
                'Param3' => $this->input->post('promo'),
                'Param4' => $this->input->post('promonumber'),
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');
            //pend($parameter);
            $response['vieww'] = $this->supper_admin->call_procedure('proc_location_v', $parameter);
            $this->load->view('helper/header');
            $this->load->view('helper/nav');
            $this->load->view('geographic/s_viewpromo');
        }

    }

    public function s_viewpromo()
    {

        $parameter = array('act_mode' => 'view_promo',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_location_v', $parameter);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('geographic/s_viewpromo', $response);
    }

    public function promo_delete()
    {
        $parameter = array('act_mode' => 'delete_promo',
            'Param1' => $this->uri->segment('4'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_location_v', $parameter);
        redirect(base_url() . 'admin/geographic/s_viewpromo');
    }


    public function addactivity()
    {
        if ($this->input->post('submit')) {

            $this->form_validation->set_rules('act_name', 'name', 'required');
            $this->form_validation->set_rules('act_desc', 'description', 'required');
            $this->form_validation->set_rules('act_price', 'price', 'required|numeric|xss_clean');

            if ($this->form_validation->run() != FALSE) {
                $parameter = array('act_mode' => 's_addactivity',
                    'Param1' => $this->input->post('act_name'),
                    'Param2' => $this->input->post('act_desc'),
                    'Param3' => $this->input->post('act_price'),
                    'Param4' => '',
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '');
                //pend($parameter);
                $response = $this->supper_admin->call_procedure('proc_location_v', $parameter);
                $this->session->set_flashdata('message', 'inserted sucessfully');
            }
        }

        if ($this->input->post('submit_update')) {
            $this->form_validation->set_rules('act_name_update', 'name', 'required');
            $this->form_validation->set_rules('act_desc_update', 'description', 'required');
            $this->form_validation->set_rules('act_price_update', 'price', 'required|numeric');
            if ($this->form_validation->run() != FALSE) {
                $parameter = array('act_mode' => 's_addactivity_update',
                    'Param1' => $this->input->post('act_name_update'),
                    'Param2' => $this->input->post('act_desc_update'),
                    'Param3' => $this->input->post('act_price_update'),
                    'Param4' => $this->input->post('act_id'),
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '');
                //pend( $parameter);
                $response = $this->supper_admin->call_procedure('proc_location_v', $parameter);
                $this->session->set_flashdata('message', 'Updated sucessfully');
            }
        }

        $parameter1 = array('act_mode' => 's_viewactivity',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_location_v', $parameter1);
        //p($response['vieww']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('geographic/addactivity', $response);

    }

    public function activity_delete_new()
    {
        $parameter = array('act_mode' => 'delete_activity',
            'Param1' => $this->uri->segment('4'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_location_v', $parameter);
        redirect(base_url() . 'admin/geographic/addactivity');

    }


    public function packageDelete1()
    {
        $parameter = array('act_mode' => 'delete_package',
            'Param1' => $this->uri->segment('4'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_location_v', $parameter);
        redirect(base_url() . 'admin/geographic/addPackage');

    }


    public function branchdelete1()
    {
        $parameter = array('act_mode' => 'delete_branch',
            'Param1' => $this->uri->segment('4'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_location_v', $parameter);
        redirect(base_url() . 'admin/geographic/addbranch');

    }


    public function s_addcompany()
    {
        if ($this->input->post('submit')) {

            $this->form_validation->set_rules('com_name', 'name', 'required');
            if ($this->form_validation->run() != FALSE) {
                $parameter = array('act_mode' => 's_addcompany',
                    'Param1' => $this->input->post('com_name'),
                    'Param2' => '',
                    'Param3' => '',
                    'Param4' => '',
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '');
                //pend($parameter);
                $response = $this->supper_admin->call_procedure('proc_location_v', $parameter);
                $this->session->set_flashdata('message', 'inserted sucessfully');
            }
        }

        if ($this->input->post('submit_update')) {

            $this->form_validation->set_rules('com_name_update', 'name', 'required');
            if ($this->form_validation->run() != FALSE) {


                $parameter = array('act_mode' => 's_updatecompany',
                    'Param1' => $this->input->post('com_name_update'),
                    'Param2' => $this->input->post('com_id'),
                    'Param3' => '',
                    'Param4' => '',
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '');
                //p($parameter);
                $response = $this->supper_admin->call_procedure('proc_location_v', $parameter);
                $this->session->set_flashdata('message', 'Updated sucessfully');
            }
        }
        $parameter1 = array('act_mode' => 's_viewcompany',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_location_v', $parameter1);
        //pend($response['vieww']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('geographic/addcompany', $response);

    }

    public function company_delete()
    {
        $parameter = array('act_mode' => 's_deletetcompany',
            'Param1' => $this->uri->segment('4'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_location_v', $parameter);
        redirect(base_url() . 'admin/geographic/s_addcompany');

    }


    public function addbranch()
    {
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules('locationid', 'locationid', 'required');
            $this->form_validation->set_rules('companyid', 'companyid', 'required');
            $this->form_validation->set_rules('branch_name', 'branch_name', 'required');
            if ($this->form_validation->run() != FALSE) {

                $parameter11 = array('act_mode' => 's_addbranch',
                    'Param1' => $this->input->post('locationid'),
                    'Param2' => $this->input->post('companyid'),
                    'Param3' => $this->input->post('branch_name'),
                    'Param4' => '',
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '');
                //pend($parameter);
                $response = $this->supper_admin->call_procedure('proc_location_v', $parameter11);
                $this->session->set_flashdata('message', 'inserted sucessfully');

            }
        }

        $parameter1 = array('act_mode' => 's_viewcompany',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_company'] = $this->supper_admin->call_procedure('proc_location_v', $parameter1);
        $parameter2 = array('act_mode' => 's_viewbranch',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_location_v', $parameter2);
        //pend($response['s_viewbranch']);
        $parameter = array('act_mode' => 'viewcountry', 'row_id' => '', 'counname' => '', 'coucode' => '', 'commid' => '');
        $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('geographic/addbranch', $response);

    }

    public function addPackage()
    {
        //echo "hello";exit();
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules('package_name', 'package_name', 'required');
            $this->form_validation->set_rules('package_desc', 'package_desc', 'required');
            $this->form_validation->set_rules('branchids', 'branchids', 'required');
            $this->form_validation->set_rules('act_ids', 'act_ids', 'required');
            $this->form_validation->set_rules('package_price', 'package_price', 'required');
            if ($this->form_validation->run() != FALSE) {


                $parameter_pack_master = array('act_mode' => 's_addpackage_master',
                    'Param1' => $this->input->post('package_name'),
                    'Param2' => $this->input->post('package_desc'),
                    'Param3' => $this->input->post('package_price'),
                    'Param4' => '',
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '');
                //pend($parameter_pack_master);
                $response = $this->supper_admin->call_procedure('proc_location_v', $parameter_pack_master);
                // pend($response);
                if ($response[0]->pack_id != '') {
                    foreach ($this->input->post('branchids') as $branch) {
                        $parameter_branch = array('act_mode' => 's_addpackage_branch',
                            'Param1' => $response[0]->pack_id,
                            'Param2' => $branch,
                            'Param3' => '',
                            'Param4' => '',
                            'Param5' => '',
                            'Param6' => '',
                            'Param7' => '',
                            'Param8' => '',
                            'Param9' => '');
                        // pend($parameter_branch);
                        $response_branch = $this->supper_admin->call_procedure('proc_location_v', $parameter_branch);
                    }
                    foreach ($this->input->post('act_ids') as $act) {
                        $parameter_act = array('act_mode' => 's_addpackage_activity',
                            'Param1' => $response[0]->pack_id,
                            'Param2' => $act,
                            'Param3' => '',
                            'Param4' => '',
                            'Param5' => '',
                            'Param6' => '',
                            'Param7' => '',
                            'Param8' => '',
                            'Param9' => '');
                        //pend($parameter_branch);
                        $response_act = $this->supper_admin->call_procedure('proc_location_v', $parameter_act);
                    }
                    //pend($response_act);
                    $this->session->set_flashdata('message', 'inserted sucessfully');


                }


            }
        }


        $parameter2 = array('act_mode' => 's_viewbranch',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_location_v', $parameter2);
        //pend($response['s_viewbranch']);
        $parameter3 = array('act_mode' => 's_viewactivity',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww_act'] = $this->supper_admin->call_procedure('proc_location_v', $parameter3);
        $parameter4 = array('act_mode' => 's_viewpackage',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww_pack'] = $this->supper_admin->call_procedure('proc_location_v', $parameter4);
        $parameter = array('act_mode' => 'viewcountry', 'row_id' => '', 'counname' => '', 'coucode' => '', 'commid' => '');
        $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);
        //pend( $response['vieww_pack']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('geographic/addPackage', $response);

    }


    public function updatebranch()
    {
        echo $this->input->post('update_branch_id');
        echo $this->input->post('locationid_update');
        echo $this->input->post('companyid_update');
        echo $this->input->post('branch_name_update');

        if ($this->input->post('submit')) {
            $this->form_validation->set_rules('locationid_update', 'locationid', 'required');
            $this->form_validation->set_rules('companyid_update', 'companyid', 'required');
            $this->form_validation->set_rules('branch_name_update', 'branch_name', 'required');
            if ($this->form_validation->run() != FALSE) {

                $parameter11 = array('act_mode' => 's_updatebranch',
                    'Param1' => $this->input->post('locationid_update'),
                    'Param2' => $this->input->post('companyid_update'),
                    'Param3' => $this->input->post('branch_name_update'),
                    'Param4' => $this->input->post('update_branch_id'),
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '');
                //pend($parameter);
                $response = $this->supper_admin->call_procedure('proc_location_v', $parameter11);
                $this->session->set_flashdata('message', 'updated sucessfully');
                redirect(base_url() . 'admin/geographic/addbranch');

            } else {
                $this->session->set_flashdata('message', 'Not updated please try again');
                redirect(base_url() . 'admin/geographic/addbranch');
            }
        }
    }

    public function activityPrice()
    {
        $parameter4 = array('act_mode' => 'activityPrice',
            'Param1' => $this->input->post('act_id'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response = $this->supper_admin->call_procedure('proc_location_v', $parameter4);
        print_r(json_encode($response));
    }

    public function addAddon()
    {

        if ($this->input->post('submit')) {
            $this->form_validation->set_rules('addon_name', 'name', 'required');
            $this->form_validation->set_rules('addon_desc', 'description', 'required');
            $this->form_validation->set_rules('branchids', 'branch', 'required');
            $this->form_validation->set_rules('act_ids', 'activity', 'required');
            $this->form_validation->set_rules('addon_price', 'price', 'required');

            if ($this->form_validation->run() != FALSE) {

                foreach ($this->input->post('branchids') as $branch) {
                    $parameter_branch = array('act_mode' => 'addbranchAddon',
                        'Param1' => $this->input->post('addon_name'),
                        'Param2' => $branch,
                        'Param3' => $this->input->post('addon_desc'),
                        'Param4' => $this->input->post('addon_price'),
                        'Param5' => '',
                        'Param6' => '',
                        'Param7' => '',
                        'Param8' => '',
                        'Param9' => '');
                    //pend($parameter_branch);
                    $response_branch = $this->supper_admin->call_procedure('proc_location_v', $parameter_branch);
                }
                foreach ($this->input->post('act_ids') as $act) {
                    $parameter_act = array('act_mode' => 'addactivityAddon',
                        'Param1' => $this->input->post('addon_name'),
                        'Param2' => $act,
                        'Param3' => $this->input->post('addon_desc'),
                        'Param4' => $this->input->post('addon_price'),
                        'Param5' => '',
                        'Param6' => '',
                        'Param7' => '',
                        'Param8' => '',
                        'Param9' => '');
                    //pend($parameter_branch);
                    $response_act = $this->supper_admin->call_procedure('proc_location_v', $parameter_act);
                }
                //pend($response_act);
                $this->session->set_flashdata('message', 'inserted sucessfully');

            }
        }

        $parameter2 = array('act_mode' => 's_viewbranch',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_location_v', $parameter2);
        //pend($response['s_viewbranch']);
        $parameter3 = array('act_mode' => 's_viewactivity',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww_act'] = $this->supper_admin->call_procedure('proc_location_v', $parameter3);
        $parameter4 = array('act_mode' => 's_viewaddon',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww_addon'] = $this->supper_admin->call_procedure('proc_location_v', $parameter4);
        $parameter = array('act_mode' => 'viewcountry', 'row_id' => '', 'counname' => '', 'coucode' => '', 'commid' => '');
        $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);
        //pend( $response['vieww_addon']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('geographic/addAddon', $response);

    }

    public function geomaster()
    {
if(getMemberId()==1)

{

}
else
{
  redirect('admin/login/dashboard?empid='.$_GET['empid'].'&uid='.$_GET['uid'].'');
}


      //pend($this->session->userdata);
        $parameter3 = array('act_mode' => 'viewcity', 'row_id' => '', 'counname' => '', 'coucode' => '', 'commid' => '');
        $response['viewcity'] = $this->supper_admin->call_procedure('proc_geographic', $parameter3);

        $parameter2 = array(
            'act_mode' => 'viewstate',
            'row_id' => '',
            'counname' => '',
            'coucode' => '',
            'commid' => ''
        );
        $response['data'] = $this->supper_admin->call_procedure('proc_geographic', $parameter2);
        $parameter1 = array('act_mode' => 'viewcountry', 'row_id' => '', 'counname' => '', 'coucode' => '', 'commid' => '');
        $response['viewcountry'] = $this->supper_admin->call_procedure('proc_geographic', $parameter1);
        $parameter = array('act_mode' => 'viewcountry', 'row_id' => '', 'counname' => '', 'coucode' => '', 'commid' => '');
        $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);
        $parameter4 = array('act_mode' => 'viewlocation', 'row_id' => '', 'counname' => '', 'coucode' => '', 'commid' => '');
        $response['vieww_location'] = $this->supper_admin->call_procedure('proc_geographic', $parameter4);
        //pend($response['vieww_location']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('geographic/ViewGeoMaster', $response);
    }


}// end class
?>