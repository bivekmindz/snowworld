<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Packages extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
 $this->load->library('session');

    }

    /*Add package by zzz*/
    public function addPackage()
    {
        if ($this->input->post('submit')) {
            //pend($_POST);
            $this->form_validation->set_rules('package_name', 'package_name', 'required');
            $this->form_validation->set_rules('package_desc', 'package_desc', 'required');
            $this->form_validation->set_rules('branchids', 'branchids', 'required');
            //$this->form_validation->set_rules('act_ids', 'act_ids', 'required');
            $this->form_validation->set_rules('package_price', 'package_price', 'required');
            if ($this->form_validation->run() != FALSE) {

                $configUpload['upload_path'] = './assets/admin/images';              #the folder placed in the root of project
                $configUpload['allowed_types'] = 'gif|jpg|png|bmp|jpeg';       #allowed types description
                $configUpload['max_size'] = '0';                          #max size
                $configUpload['max_width'] = '0';                          #max width
                $configUpload['max_height'] = '0';                          #max height
                $configUpload['encrypt_name'] = true;                         #encrypt name of the uploaded file

                $this->load->library('upload', $configUpload);
                //pend($this->input->post('pack_image'));
                if (!$this->upload->do_upload('pack_image')) {
                    $uploadedDetails = $this->upload->display_errors();
                   // pend($uploadedDetails);
                    $this->session->set_flashdata('message', $uploadedDetails);
                } else {
                    $uploadedDetails = $this->upload->data();
                    $parameter_pack_master = array(
                        'act_mode' => 's_addpackage_master',
                        'Param1' => $this->input->post('package_name'),
                        'Param2' => $this->input->post('package_desc'),
                        'Param3' => $this->input->post('package_price'),
                        'Param4' => $uploadedDetails['file_name'],
                        'Param5' => '',
                        'Param6' => '',
                        'Param7' => '',
                        'Param8' => '',
                        'Param9' => '');
                    //pend($parameter_pack_master);
                    $response = $this->supper_admin->call_procedure('proc_packages_s', $parameter_pack_master);
                    // pend($response);
                    if ($response[0]->pack_id != '') {
                        foreach ($this->input->post('branchids') as $branch) {
                            $parameter_branch = array('act_mode' => 's_addpackage_branch',
                                'Param1' => $response[0]->pack_id,
                                'Param2' => $branch,
                                'Param3' => '',
                                'Param4' => '',
                                'Param5' => '',
                                'Param6' => '',
                                'Param7' => '',
                                'Param8' => '',
                                'Param9' => '');
                            // pend($parameter_branch);
                            $response_branch = $this->supper_admin->call_procedure('proc_packages_s', $parameter_branch);
                        }

                    }
                    $this->session->set_flashdata('message', 'inserted sucessfully');


                }
            }
        }


        $parameter2 = array( 'act_mode'=>'s_viewbranch',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);
        //pend($response['s_viewbranch']);
        $parameter3 = array( 'act_mode'=>'s_viewactivity',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        //pend($parameter);
        $response['vieww_act'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter3);
        $parameter4 = array( 'act_mode'=>'s_viewpackage',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        //pend($parameter);
        $response['vieww_pack'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter4);
        //pend( $response['vieww_pack']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('package/addPackage',$response);

    }

    public function packagestatus($a,$b)
    {
        $status=base64_decode($b)==1 ? 0:1;
        $param= array(
            'act_mode'=>'update_pack_status',
            'Param1'=>base64_decode($a),
            'Param2'=>$status,
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>''
        );

       // pend($param);

        $response=$this->supper_admin->call_procedure('proc_1',$param);
        redirect("admin/packages/addPackage?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");

    }

    public function packageviewupdate($id)
    {
        $pid= base64_decode($id);
      
        if ($this->input->post('submit'))
        {

        if ($_FILES['pack_image']['name'] == "")
        {
            $this->form_validation->set_rules('package_name', 'package_name', 'required');
            $this->form_validation->set_rules('package_desc', 'package_desc', 'required');
            $this->form_validation->set_rules('branchids', 'branchids', 'required');
            //$this->form_validation->set_rules('act_ids', 'act_ids', 'required');
            $this->form_validation->set_rules('package_price', 'package_price', 'required');
            if ($this->form_validation->run() != FALSE) {
                $param_pack = array(
                    'act_mode' => 'update_pack_info',
                    'Param1' => $pid,
                    'Param2' => '',
                    'Param3' => $this->input->post('package_name'),
                    'Param4' => $this->input->post('package_desc'),
                    'Param5' => $this->input->post('package_price'),
                    'Param6' => ''
                );
                $response = $this->supper_admin->call_procedure('proc_1', $param_pack);

                foreach ($this->input->post('branchids') as $branch) {
                    $parameter_branch = array(
                        'act_mode' => 'update_package_branch',
                        'Param1' => $pid,
                        'Param2' => $branch,
                        'Param3' => '',
                        'Param4' => '',
                        'Param5' => '',
                        'Param6' => ''
                    );
                }
                // pend($parameter_branch);
                $response_branch = $this->supper_admin->call_procedure('proc_1', $parameter_branch);
                $this->session->set_flashdata('message', 'Packages updated successfully');
                redirect("admin/packages/addPackage?empid=" . $_GET['empid'] . "&uid=" . str_replace(".html", "", $_GET['uid']) . "");


            }
        }
        else
        {
            $this->form_validation->set_rules('package_name', 'package_name', 'required');
            $this->form_validation->set_rules('package_desc', 'package_desc', 'required');
            $this->form_validation->set_rules('branchids', 'branchids', 'required');
            $this->form_validation->set_rules('package_price', 'package_price', 'required');
            if ($this->form_validation->run() != FALSE) {
                $configUpload['upload_path'] = './assets/admin/images';              #the folder placed in the root of project
                $configUpload['allowed_types'] = 'gif|jpg|png|bmp|jpeg';       #allowed types description
                $configUpload['max_size'] = '0';                          #max size
                $configUpload['max_width'] = '0';                          #max width
                $configUpload['max_height'] = '0';                          #max height
                $configUpload['encrypt_name'] = true;                         #encrypt name of the uploaded file
                $this->load->library('upload', $configUpload);                  #init the upload class
                if (!$this->upload->do_upload('pack_image'))
                {
                    $uploadedDetails = $this->upload->display_errors();
                    $this->session->set_flashdata('message', $uploadedDetails);
                } else
                    {
                    $uploadedDetails = $this->upload->data();
                    $this->session->set_flashdata('message', 'updated sucessfully');
                        $param_pack = array(
                            'act_mode' => 'update_pack_img_info',
                            'Param1' => $pid,
                            'Param2' => '',
                            'Param3' => $this->input->post('package_name'),
                            'Param4' => $this->input->post('package_desc'),
                            'Param5' => $this->input->post('package_price'),
                            'Param6' => $uploadedDetails['file_name']
                        );
                    $response = $this->supper_admin->call_procedure('proc_1', $param_pack);

                        $this->session->set_flashdata('message', 'Packages updated successfully');
                        redirect("admin/packages/addPackage?empid=" . $_GET['empid'] . "&uid=" . str_replace(".html", "", $_GET['uid']) . "");

              }


            }
        }//end of else
        }//end of if


        $param=array('act_mode'=>'get_pack_info',
            'Param1' => $pid,
            'Param2' => '',
            'Param3' =>'',
            'Param4' =>'',
            'Param5' => '',
            'Param6' => ''
        );
        //p($param);
        $response['view']= $this->supper_admin->call_procedureRow('proc_1',$param);

        $parameter2 = array( 'act_mode'=>'s_viewbranch',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);
        //pend($response['s_viewbranch']);
        $parameter3 = array( 'act_mode'=>'s_viewactivity',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        //pend($parameter);
        $response['vieww_act'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter3);
        $parameter4 = array( 'act_mode'=>'s_viewpackage',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        //pend($parameter);
        $response['vieww_pack'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter4);


        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('package/updatePackage',$response);

    }

    /*get price of total activities by zzz*/
    public function activityPrice()
    {
        $parameter4 = array( 'act_mode'=>'activityPrice',
            'Param1'=> $this->input->post('act_id'),
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        //pend($parameter);
        $response = $this->supper_admin->call_procedure('proc_packages_s',$parameter4);
        print_r(json_encode($response));
    }


    public function update_package_data()
    {
        $a =  $_POST['pack_id'];
        $parameter4 = array( 'act_mode'=>'update_package_data',
            'Param1'=> $a,
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        //pend($parameter4);
        $response = $this->supper_admin->call_procedure('proc_packages_s',$parameter4);
        print_r(json_encode($response));
    }

    /*Delete package By zzz*/
    public function packageDelete1()
    {
        $parameter = array('act_mode' => 'delete_package',
            'Param1' => $this->uri->segment('4'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
//pend($parameter);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_packages_s', $parameter);

        redirect("admin/packages/addPackage?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");


    }

    public function updatePackage()
    {
        //pend($_POST);
        $this->form_validation->set_rules('update_package_name', 'package_name', 'required');
        $this->form_validation->set_rules('update_package_desc', 'package_desc', 'required');
        $this->form_validation->set_rules('update_branchids', 'branchids', 'required');
        //$this->form_validation->set_rules('act_ids', 'act_ids', 'required');
        $this->form_validation->set_rules('update_package_price', 'package_price', 'required');
        if ($this->form_validation->run() != FALSE) {
            $parameter_pack_master = array('act_mode' => 's_addpackage_master_update',
                'Param1' => $this->input->post('update_package_name'),
                'Param2' => $this->input->post('update_package_desc'),
                'Param3' => $this->input->post('update_package_price'),
                'Param4' => $this->input->post('pack_update_hidden_id'),
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');
            //pend($parameter_pack_master);
            $response = $this->supper_admin->call_procedure('proc_packages_s', $parameter_pack_master);
            // pend($response);

                foreach ($this->input->post('update_branchids') as $branch) {
                    $parameter_branch = array('act_mode' => 's_addpackage_branch_update',
                        'Param1' => $this->input->post('pack_update_hidden_id'),
                        'Param2' => $branch,
                        'Param3' => '',
                        'Param4' => '',
                        'Param5' => '',
                        'Param6' => '',
                        'Param7' => '',
                        'Param8' => '',
                        'Param9' => '');
                    // pend($parameter_branch);
                    $response_branch = $this->supper_admin->call_procedure('proc_packages_s', $parameter_branch);
                }
                /*foreach ($this->input->post('act_ids') as $act) {
                    $parameter_act = array('act_mode' => 's_addpackage_activity',
                        'Param1' => $response[0]->pack_id,
                        'Param2' => $act,
                        'Param3' => '',
                        'Param4' => '',
                        'Param5' => '',
                        'Param6' => '',
                        'Param7' => '',
                        'Param8' => '',
                        'Param9' => '');
                    //pend($parameter_branch);
                    $response_act = $this->supper_admin->call_procedure('proc_packages_s', $parameter_act);
                }*/
                //pend($response_act);
                $this->session->set_flashdata('message', 'updated sucessfully');
            redirect("admin/packages/addPackage?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");

        }
        else{
            $this->session->set_flashdata('message', 'not updated');
            redirect("admin/packages/addPackage?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");
        }



    }



}// end class
?>