<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Fblogin extends MX_Controller
{
  public function __construct() 
  {
      $this->load->model("supper_admin");
      $this->load->helper("my_helper");
  }


    public function facebook()
    {
        parse_str($_SERVER['QUERY_STRING'], $_REQUEST);
        $CI = &get_instance();
        $CI->config->load("facebook", TRUE);
        $config = $CI->config->item('facebook');
        $this->load->library('facebook/Facebook', $config);
        $userId = $this->facebook->getUser();
// If user is not yet authenticated, the id will be zero
        if ($userId == 0)
        {
// Generate a login url
            $data['login_url'] = $this->facebook->getLoginUrl(array('scope' => 'email', 'user_about_me'));
            $this->load->view('main_index', $data);
        }
        else
        {
// Get user’s data and print it
            $user = $this->facebook->api('/ me');
           // echo ‘< pre>’;
            print_r($user);
        }
    }


    public function login()
    {
        $fb = new Facebook\Facebook([
            'app_id' =>'140829839964979', // Replace {app-id} with your app id
            'app_secret' => 'ff917e77769ddc83917bc9f13b10a67d',
            'default_graph_version' => 'v2.2',
        ]);

        $helper = $fb->getRedirectLoginHelper();

        $permissions = ['email']; // Optional permissions
        $loginUrl = $helper->getLoginUrl('https://localhost/snowworldnew/webapp/Fblogin/fbcallback', $permissions);

        header("location: ".$loginUrl);

        // $this->load->library('facebook', array(
        //    'appId' => '151196238861717',
        //    'secret' => 'f1df714037ed4ed3af13a35514d107f1',
        //    ));

    }

    public function fbcallback()
    {
        $fb = new Facebook\Facebook([
            'app_id' => '151196238861717',
            'app_secret' => 'f1df714037ed4ed3af13a35514d107f1',
            'default_graph_version' => 'v2.5',
        ]);

        $helper = $fb->getRedirectLoginHelper();

        try {

            $accessToken = $helper->getAccessToken();

        }catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }


        try {
            // Get the Facebook\GraphNodes\GraphUser object for the current user.
            // If you provided a 'default_access_token', the '{access-token}' is optional.
            $response = $fb->get('/me?fields=id,name,email,first_name,last_name,birthday,location,gender', $accessToken);
            // print_r($response);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'ERROR: Graph ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'ERROR: validation fails ' . $e->getMessage();
            exit;
        }

        // User Information Retrival begins................................................
        $me = $response->getGraphUser();

        $location = $me->getProperty('location');
        echo "Full Name: ".$me->getProperty('name')."<br>";
        echo "First Name: ".$me->getProperty('first_name')."<br>";
        echo "Last Name: ".$me->getProperty('last_name')."<br>";
        echo "Gender: ".$me->getProperty('gender')."<br>";
        echo "Email: ".$me->getProperty('email')."<br>";
        echo "location: ".$location['name']."<br>";
        echo "Birthday: ".$me->getProperty('birthday')->format('d/m/Y')."<br>";
        echo "Facebook ID: <a href='https://www.facebook.com/".$me->getProperty('id')."' target='_blank'>".$me->getProperty('id')."</a>"."<br>";
        $profileid = $me->getProperty('id');
        echo "</br><img src='//graph.facebook.com/$profileid/picture?type=large'> ";
        echo "</br></br>Access Token : </br>".$accessToken;



    }


}

?>