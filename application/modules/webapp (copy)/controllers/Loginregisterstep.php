<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Loginregisterstep extends MX_Controller {

    public function __construct() {
        $this->load->model("supper_admin");
        $this->load->library('session');
         session_start();
    }



    public function step4(){
		
		//p( $this->session->userdata('final_selected_addon_data') );

        if($this->session->userdata('packageval')==''){
            redirect(base_url());
        } else {
            //Select branch
            $siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);



//select banner images
            $parameterbanner=array(
                'act_mode' =>'selectbannerimages',
                'branchid' =>$data['branch']->branch_id,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/banner/format/json/';
            $data['banner']=curlpost($parameterbanner,$path);




            $parametertimeslot=array(
                'act_mode' =>'selectsestimeslot',
                'branchid' =>$data['branch']->branch_id,
                'destinationType' =>$this->session->userdata('destinationType'),
                'type'=>'web',

            );

            $path=api_url().'selecttimesloturl/timeslotses/format/json/';
            $data['timeslotses']=curlpost($parametertimeslot,$path);


//continue Login
            if($this->input->post('submit')=='Sign In')
            {

                //Login
                $parameter3=array(
                    'act_mode' =>'memlogin',
                    'username' =>$this->input->post('emailmob'),
                    'password'=>$this->input->post('pwd'),
                    'type'=>'web',

                );

                $path=api_url().'userapi/userlogin/format/json/';
                $data['login']=curlpost($parameter3,$path);



                if($data['login']->scalar=='Something Went Wrong')
                {

                    $data['message']="Invalid Login";
                    //header("location:".base_url()."step4");
                }
                else
                {
			
					

                    // Userlog
                    $parameter4=array(
                        'act_mode' =>'memuserlog',
                        'user_id' =>$data['login']->user_id,
                        'type'=>'web',
                    );

                    $path=api_url().'userapi/userlog/format/json/';
                    $data['userlog']=curlpost($parameter4,$path);


                    $data = array(
                        'skiindia' => $data['login']->user_id,
						 'user_regdatafill' =>$data['login']->user_regdatafill,
                    );

                    $this->session->set_userdata($data);

                    $data['skiindases'] =($data['login']->user_id);


                    header("location:".base_url()."summary");
                }

//echo ($this->session->userdata->skiindia);

            }

//continue Register
            if($this->input->post('submit')=='Sign up')
            {


                //check user
                $parameter9=array(
                    'act_mode' =>'checkuser',
                    'email'=>$this->input->post('email'),
                    'type'=>'web',
                );

                $path=api_url().'userapi/usercheck/format/json/';
                $data['checkuser']=curlpost($parameter9,$path);


                if($data['checkuser']->scalar=='0')
                {
                    $this->form_validation->set_rules('fname', 'Enter First Name', 'required');
                    $this->form_validation->set_rules('lastName', 'Enter Last Name ', 'required');
                    $this->form_validation->set_rules('mobileno', 'Enter Mobile No', 'required');
                    $this->form_validation->set_rules('email', 'Enter Email id ', 'required');
                    $this->form_validation->set_rules('password', 'Enter Password', 'required');                 $this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|matches[password]');

                    /*if ($this->form_validation->run() != FALSE) {*/







                        //Select branch
                        $siteurl= base_url();
                        $parameterbranch=array(
                            'act_mode' =>'selectbranch',
                            'weburl' =>$siteurl,
                            'type'=>'web',

                        );

                        $path=api_url().'selectsiteurl/branch/format/json/';
                        $data['branch']=curlpost($parameterbranch,$path);



                        //select banner images
                        $parameterbanner = array(
                            'act_mode' => 'selectbannerimages',
                            'branchid' => $data['branch']->branch_id,
                            'type' => 'web',

                        );

                        $path = api_url() . 'selectsiteurl/banner/format/json/';
                        $data['banner'] = curlpost($parameterbanner, $path);

//pend($data['banner']);
                        $parametersms = array(
                            'act_mode' => 'selectsms',
                            'branchid' => $data['branch']->branch_id,
                            'type' => 'web',

                        );

                        $path = api_url() . 'selectsiteurl/banner/format/json/';
                        $data['smsgatway'] = curlpost($parametersms, $path);




                        $message_new = '<table width="96%" style="line-height: 28px; font-family: sans-serif;" >
<tr><td>Dear '.$data['updateacc']->user_title.' '. ucfirst($data['updateacc']->user_firstname).' '.$data['updateacc']->user_lastname.',</td></tr>

<tr><td>Greetings from ' . $data['banner']->bannerimage_top3 . '.!</td></tr>

<tr><td>
You have received this communication in response to your request for your ' . $data['banner']->bannerimage_top3 . ' Account password to be sent to you via e-mail and sms.
</td></tr>
<tr><td>Your temporary password is: '.($this->input->post('password')).' </td></tr>
<tr><td>Please use the password exactly as it appears above.</td></tr>


<tr><td>Use this password to login to your ' . $data['banner']->bannerimage_top3 . ' account.</td></tr>

<tr><td>We request you immediately change your password by logging on to '.base_url().'</td></tr>


<tr><td>Should you have any queries, please feel free to write to us on '.$data['banner']->bannerimage_branch_email.' or call us on '.$data['banner']->bannerimage_branch_contact.'.</td></tr>



<tr><td>&nbsp;</td></td>
<tr><td>&nbsp;</td></td>
<tr><td>Yours sincerely,<br>
'.$data['banner']->bannerimage_top3.' Team</td></tr>
</table>
';


                        $from_email = $data['banner']->bannerimage_from ;;
                        $to_email = $this->input->post('email');
                        //Load email library
                        $this->load->library('email');
                        $this->email->from($from_email,  $data['banner']->bannerimage_top3 );
                        $this->email->to($to_email);
                        $this->email->subject('' . $data['banner']->bannerimage_top3 . ' - Your Registration Details');
                        $this->email->message($message_new);
                        //Send mail
                        $this->email->send();
                        //pend($this->email->print_debugger());
                        $emaildata=$this->input->post('email');
                        $mob=$this->input->post('mobileno');
                        $registersms=urlencode("Thank you for registering with us. You can use your email-id ".$emaildata." for managing your ".$data['banner']->bannerimage_top3." account.");
                        /*sms api*/
                        $sms_url = "http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?feedid=".$data['smsgatway']->sms_feedid."&username=".$data['smsgatway']->sms_username."&password=".$data['smsgatway']->sms_password."&To=".$this->input->post('mobile_number')."&Text=".$data;
                        $sms = file_get_contents($sms_url);
                        /*sms api*/











                        //Register

                        $parameter6=array(
                            'act_mode'=>'memregister',
                            'Param1'=>$this->input->post('regtitle'),
                            'Param2'=>$this->input->post('fname'),
                            'Param3'=>$this->input->post('lastName'),
                            'Param4'=>$this->input->post('mobileno'),
                            'Param5'=>$this->input->post('email'),
                            'Param6'=>base64_encode($this->input->post('password')),
                            'Param7'=>$this->input->post('reg_address'),
                            'Param8'=>$this->input->post('reg_city'),
                            'Param9'=>$this->input->post('reg_state'),
                            'Param10'=>$this->input->post('reg_zip'),
                            'Param11'=>$this->input->post('reg_country'),
                            'Param12'=>'',
                            'Param13'=>'',
                            'Param14'=>'',
                            'Param15'=>'',
                        );


                        $data['register'] = $this->supper_admin->call_procedure('proc_memberregister_v',$parameter6);




                        $arr = (array)$data['register'];

// Userlog

                    $parameter4=array(
                            'act_mode' =>'memuserlog',
                            'user_id' =>$arr[0]->ls,
                            'type'=>'web',
                        );

                        $path=api_url().'userapi/userlog/format/json/';
                        $data['userlog']=curlpost($parameter4,$path);

                        $data = array(
                            'skiindia' => $arr[0]->ls,
                        );

                        $this->session->set_userdata($data);

                        $data['skiindases'] =($arr[0]->ls);
                        redirect(base_url().'summary');

                     //   header("location:".base_url().'summary');
                   /* }*/
                }
                else
                {
                    $data['msg'] ="Email Already Exist";
                    // header("location:".base_url()."step4");
                }
            }






          $parameter=array(
                        'act_mode'=>'ViewCountrys',
                        'Param1'=>'',
                        'Param2'=>'',
                        'Param3'=>'',
                        'Param4'=>'',
                        'Param5'=>'',
                        'Param6'=>'',
                        'Param7'=>'',
                        'Param8'=>'',
                        'Param9'=>'',
                        'Param10'=>'',
                        'Param11'=>'',
                        'Param12'=>'',
                        'Param13'=>'',
                        'Param14'=>'',
                        'Param15'=>'',
                    );

 
$data['Countrys'] = $this->supper_admin->call_procedure('proc_timeslotspackages',$parameter);
       
       $parameter = array('act_mode'=>'select_order',
        'orderid'=>$this->session->userdata('orderlastinsertid'),
        'type'=>'web',

        );
        $path1 = api_url()."Cart/getpayment_package/format/json/";
        $data['paymentpac']= curlpost($parameter,$path1);


            $this->load->view("helper/header");
            $this->load->view("helper/topbar",$data);
            $this->load->view("loginregister",$data);
            $this->load->view("helper/footer");
        }
    }


    public function adduser(){





        // Userlog
        $parameter4=array(
            'act_mode' =>'orderuserupdate',
            'user_id' =>$this->input->post('user_id'),
            'order_id'=>$this->input->post('order_idval'),
            'title' =>$this->input->post('title'),

            'billing_name' =>$this->input->post('billing_name'),
            'billing_email' =>$this->input->post('billing_email'),
            'billing_tel'=>$this->input->post('billing_tel'),
            'billing_address' =>$this->input->post('billing_address'),
            'billing_city'=>$this->input->post('billing_city'),
            'billing_state' =>$this->input->post('billing_state'),

            'billing_zip'=>$this->input->post('billing_zip'),
            'billing_country' =>$this->input->post('billing_country'),

            'type'=>'web',
        );

        $path=api_url().'userapi/userorderupdate/format/json/';
        $data['userregister']=curlpost($parameter4,$path);


    }


}//end of class
?>