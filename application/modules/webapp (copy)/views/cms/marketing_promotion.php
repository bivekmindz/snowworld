
  <div class="container margin_60">
  <h3 class="text-center text-uppercase">Marketing &amp; Promotion</h3>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="row">
                  <div class="col-md-4">
                        <img SRC="img/marketing/1.jpg" alt="img" class="img-responsive styled">
                        <h3>Ashwin Kamble</h3>
            <p class="designation">(Head - Business development & Strategic alliance)</p>
                        <p>For marketing & promotion you can contact to <br>Mr. Ashwin Kamble on his <br>E-mail Id: ashwin@chiliadprocons.in</p>
                    </div>
                    
                    <div class="col-md-4">
                        <img SRC="img/marketing/2.jpg" alt="img" class="img-responsive styled">
                        <h3>Paramjit Singh Gupta</h3>
            <p class="designation">(Manager - Business Development & Marketing)</p>
                        <p>For marketing & promotion you can contact to <br>Mr. Paramjit Singh Gupta on his <br>E-mail Id: paramjit@chiliadprocons.in</p>
                    </div>
                    
                    <div class="col-md-4">
                        <img SRC="img/marketing/3.jpg" alt="img" class="img-responsive styled">
                        <h3>Priyanka Jain</h3>
            <p class="designation">(Director - Business Development)</p>
                        <p>For marketing & promotion you can contact to <br>Miss. Priyanka Jain on his <br>E-mail Id: priyanka@chiliadprocons.in</p>
                    </div>                    
                </div><!-- End row -->
            </div><!-- End col-lg-9 -->            
        </div><!-- End row -->
    </div><!-- End Container -->
    