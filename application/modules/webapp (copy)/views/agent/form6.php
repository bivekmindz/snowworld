<style type="text/css">

    .regiter_step {
        width: 100%;
        float: left;
        background-color: azure;
    }

    .regiter_step .stpe_con_all {
        width: 100%;
        float: left;
        padding: 20px 0px 20px 0px;
    }

    .regiter_step .stpe_con_all .left_contyi {
        width: 100%;
        float: left;
    }

    .regiter_step .stpe_con_all .left_contyi ul {
        width: 100%;
        float: left;
        list-style: none;
    }

    .regiter_step .stpe_con_all .left_contyi ul li {
        padding: 3px;
        float: left;
        width: 100%;
    }

    .regiter_step .stpe_con_all .left_contyi ul li a {
        text-decoration: none;
        border-left: 3px solid #d40735;
        background-color: #2657a6;
        padding: 10px 15px 10px 15px;
        color: #fff;
        font-weight: 700;
        width: 100%;
        float: left;
    }

    .acty {
        color: #fff !important;
        background-color: #d40735 !important;
        border-left: 3px solid #6fc27f !important;
    }

    .regiter_step .stpe_con_all .left_contyi ul li a:hover {
        color: #fff;
        background-color: #d40735;
        border-left: 3px solid #d40735;
    }

    .regiter_step .stpe_con_all .right_contyi {
        width: 100%;
        float: left;
        background-color: #fff;
    }

    .regiter_step .stpe_con_all .right_contyi .heading_tab {
        width: 100%;
        float: left;
        padding: 0px 20px 10px 20px;
        border-bottom: 1px dotted #CCC;
    }

    .regiter_step .stpe_con_all .right_contyi .heading_tab h1 {
        width: 100%;
        float: left;
        font-size: large;
    }

    .regiter_step .stpe_con_all .right_contyi .heading_tab h1 span {
        color: #2657a6;
        font-size: medium;
    }

    .regiter_step .stpe_con_all .right_contyi .form_details {
        width: 100%;
        float: left;
        padding: 25px 20px 10px 20px;
    }

    .regiter_step .stpe_con_all .right_contyi .form_details .form-group {
        width: 100%;
        float: left;
    }

    .regiter_step .stpe_con_all .right_contyi .form_details .form-group select {
        min-height: 35px;
    }

    .regiter_step .stpe_con_all .right_contyi .form_details .next_button {
        width: 100%;
        float: left;
        padding: 20px 0px 20px 0px;
        text-align: center;
    }

    .regiter_step .stpe_con_all .right_contyi .form_details .next_button a {

        background-color: #2657a6;
        padding: 7px 15px 7px 15px;
        line-height: 35px;
        border-radius: 2px;
        color: #fff;
    }

</style>

<section class="regiter_step">
    <div class="container">

        <div class="row">
            <div class="stpe_con_all">
                <div class="col-md-4">
                    <div class="left_contyi">
                        <ul>
                            <li><a href="agentform1"> Identity Verification</a></li>
                            <li><a href="agentform2" >Seller Details </a></li>
                            <li><a href="agentform3">Official Details</a></li>
                            <li><a href="agentform4" >Statutory Requirements</a></li>
                            <li><a href="agentform5" >Bank Details</a></li>
                            <li><a href="agentform6" class="acty">Documents Required</a></li>

                        </ul>

                    </div>
                </div>

                <div class="col-md-8">

                    <div class="right_contyi">
                        <div class="heading_tab">
                            <h1>Documents Required </h1>
                        </div>
                        <form action="" method="post" enctype="multipart/form-data">
                             <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="control-label"> Copy of PAN CARD</label>
                                        <input  type="file" name="copypancart" class="form-control-1"  required>	</div>


                                    <div class="col-md-6">
                                        <label class="control-label">  Copy of Service Tax/ Sales Tax/ VAT</label>
                                        <input  type="file" name="copyservicetax" class="form-control-1" required>	</div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="control-label"> Copy of TAN</label>
                                        <input  type="file" name="copytan" class="form-control-1" required>	</div>


                                    <div class="col-md-6">
                                        <label class="control-label"> Address Proof</label>
                                        <input  type="file" name="copyaddressproof" class="form-control-1" required>	</div>
                                </div>
                            </div>







                            <div class="next_button">
                                <input type="submit" value="Submit" name="Submit" class="btn_1">
                            </div>


                        </form>
                    </div>

                </div>

            </div>

        </div>
    </div>
</section>