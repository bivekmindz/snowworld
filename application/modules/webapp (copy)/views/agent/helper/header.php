<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="author" content="Durgesh Chauhan">
        <title>Snow World - Mumbai's Largest Snow Park</title>
        <meta http-equiv="description" name="description" content="Ski India- Snow theme amusement park complete entertainment and hangout places to go with friends and family, India's first & biggest snow park Ski Dubai now in Delhi/NCR"/>
        <meta name="keywords" content="skiing in india, amusment park, snow theme park, indoor amusement park">
    <link rel="icon" href="assets/agent/images/favicon.ico">
    <!-- Favicons-->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>img/favicon.png" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="<?php echo base_url(); ?>img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?php echo base_url(); ?>img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?php echo base_url(); ?>img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?php echo base_url(); ?>img/apple-touch-icon-144x144-precomposed.png">
    <!-- Google web fonts -->
  
    <link rel="stylesheet" href="assets/agent/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css" id="style-resource-1">
    <link rel="stylesheet" href="assets/agent/css/entypo.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic" id="style-resource-3">
    <link rel="stylesheet" href="assets/agent/css/bootstrap.css" >
    <link rel="stylesheet" href="assets/agent/css/neon-core.css">
    <link rel="stylesheet" href="assets/agent/css/neon-theme.css" >
    <link rel="stylesheet" href="assets/agent/css/neon-forms.css" >
    <link rel="stylesheet" href="assets/agent/css/daterangepicker-bs3.css" >
    <link rel="stylesheet" href="assets/agent/css/font-awesome.min.css" >


    <script src="assets/agent/js/jquery-1.11.3.min.js"></script>
    <!--[if lt IE 9]><script src="http://demo.neontheme.com/assets/js/ie8-responsive-file-warning.js"></script><![endif]--><!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --><!--[if lt IE 9]> <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script> <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script> <![endif]--><!-- TS1498641546: Neon - Responsive Admin Template created by Laborator -->
</head>
<body class="page-body page-fade" data-url="">
<div class="page-container">
    <div class="sidebar-menu fixed">
        <div class="sidebar-menu-inner">
            <header class="logo-env">
                <!-- logo -->
                <div class="logo"> <a href=""> <img src="assets/agent/images/logo.png" width="80" alt="" /> </a> </div>
                <!-- logo collapse icon -->
                <div class="sidebar-collapse">
                    <a href="#" class="sidebar-collapse-icon">
                        <!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition --> <i class="entypo-menu"></i>
                    </a>
                </div>
                <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                <div class="sidebar-mobile-menu visible-xs">
                    <a href="#" class="with-animation">
                        <!-- add class "with-animation" to support animation --> <i class="entypo-menu"></i>
                    </a>
                </div>
            </header>
