
    
    <section class="login_rege">
    <div class="container">
    
    <div class="row">
    
    <div class="login box_style_1">
    
 
        <h3 class="form-signin-heading to_p">
          <i class="icon-user"></i> User Registration Form
        </h3><span id="updateRegister">
  <form name="register-form" class="formm" id="registration-form" method="POST" action="">
                                                                    <div class="guest-details">
                                                                        <div class="widget-header">
                                                                            <div class="widget-title"></div>
                                                                        </div>
                                                                        <div class="widget-content clearfix">
                       <div class='flashmsg'>
                                    <?php
                                    echo $emsg;
                                    echo validation_errors(); ?>
                                    <?php
                                    if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message');
                                    }
                                    ?>
                                </div>
                                                      <div class="row">
                                                               <div class="col-xs-2">
                                                                                    <div class="form-group">
                                                                                        <select class="form-control " id="sel1"  required style=" min-width: 72px;" name="regtitle">
                                                                                            <option>Mr</option>
                                                                                            <option>Mrs</option>
                                                                                            <option>Ms</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-10">
                                                                                    <div class="form-group">
                                                                                        <input type="text" name="fname" class="form-control-1 form-control_width" placeholder="Enter Your Name" pattern="[A-Za-z\s]+" required title="First Name should only contain  Alphabets. e.g. John" maxlength="60" value="">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-xs-6 ">
                                                                                    <div class="form-group">
                                                                                        <input type="email" name="email" class="form-control-1" placeholder="Enter Your Email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required title="The input is not a valid email address" maxlength="50" value="">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-6 ">
                                                                                    <div class="form-group">
                                                                                        <input type="tel" name="mobileno" class="form-control-1" placeholder="Enter Your Phone Number" pattern="^\d{10}$" required title="Your Phone Number Should Only Contain  Numbers. e.g. 9999999999" maxlength="10" value="">
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                             <div class="row">
                                                                                <div class="col-xs-6 ">
                                                                                    <div class="form-group">
<div id="validationErrorMessage" class="has-error"><span class="help-block"></span></div>
                           <input type="password" id="password" name="password" class="form-control-1" placeholder="Enter Password" required title="Enter Password" maxlength="35" minlength="6" >
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-6 ">
                                                                                    <div class="form-group">
                                                                                       <input type="password" id="confirm_password" name="cpassword" class="form-control-1" placeholder="Enter Confirm Password" required title="Enter Confirm Password" maxlength="35" minlength="6" >
                              <span class="err00"  style="color: red;display: none">PASSWORD DON'T MATCH</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>


                                                                            <div class="row">

                                                                                <div class="col-xs-12 form-group">
                                                                                    <textarea required id="message_contact" name="reg_address" class="form-control-1" placeholder="Write Your Address"  ></textarea>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row"><span id="j_idt211:updateDetails">
                                                                                <div class="col-xs-6 ">
                                                                                    <div class="form-group">
                                                                                        <input type="text" name="reg_city" class="form-control-1" required placeholder="Enter Your City" pattern="[A-Za-z\s]+"  title="City Name should only contain  Alphabets. e.g. Delhi" maxlength="20" value="">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-6 ">
                                                                                    <div class="form-group">
                                                                                        <input type="text" name="reg_state" class="form-control-1" required placeholder="Enter Your State" pattern="[A-Za-z\s]+"  title="State Name should only contain  Alphabets. e.g. Delhi" maxlength="20" value="">
                                                                                    </div>
                                                                                </div></span>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-xs-6 ">
                                                                                    <div class="form-group">
                                                                                        <input type="text" name="reg_zip"required  class="form-control-1" placeholder="Enter Your Zipcode"   title="Zip should only contain  Number. e.g. 110014" maxlength="6" value="">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-6 ">
                                                                                    <div class="form-group">

                                                                                        <select class="form-control " id="sel1"  required style="min-width: 72px;"
                                                                                                name="reg_country">
                                                                                            <option value="">Enter Your Country</option>
                                                                                            <?php foreach ($Countrys as $key => $value) {?>
                                                                                                <option value="<?php echo $value->name?>"><?php echo $value->name?></option>
                                                                                            <?php  }?>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <input type="checkbox" id="creditTermsCheck" name="checkbox" required title="Accept Tearm And Condition">
                                                                                    &nbsp;I Agree to the <a href="tearmcondition" target="_blank"> Terms Conditions</a>
                                                                                </div></div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="sub_div">

                                                                            <div class="continue">
                                                                               <button name="submit" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only btn btn-custom btn-prime" type="submit"  onclick="return functi()" role="button" aria-disabled="false" value="Register"><span class="ui-button-text ui-c">Register</span></button>&nbsp;
                                 <script>

                     function functi() {
                         var password = document.getElementById("password");
                         var confirm_password = document.getElementById("confirm_password");

                         if(password.value != confirm_password.value) {
                             $(".err00").css('display','block').fadeOut(3000);

                             return false;
                         } else {
                             confirm_password.setCustomValidity('');
                         }
                     }
                  </script>
                                                                            </div>  </div>
                                                                        </form>
      </div>
    <div>
    
    
    </div>
    
    
    </div>
    </div>
    
    
    
    </section>
    
    <!-- End Container -->
    
  