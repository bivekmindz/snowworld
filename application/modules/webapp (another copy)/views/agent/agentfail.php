<div class="fail-order block-width">
	<div class="top_heading text-center">
		<h3>
		<span>SORRY FOR THE INCONVENIENCE. YOUR BOOKING IS FAILED</span>
		</h3>
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-xs-12">
			<div class="p-note">
				<p><span class="warning"> Please note:</span> Below is your booking ID for all communication with Snow world. Please save and store the ID mentioned.</p>
			</div>
		</div>
	</div>
	<div class="order-sucess-tab">
		<div class="order-success-tab-heading text-center">
			<h5>Booking Details - </h5>
		</div>
		<div class="tab-inner-section">
			<div class="payment-detail-tab">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-xs-12">
						<div class="type-id">
							<h4><span class="heading-text">Ticket Type :</span></h4>
							<h4><span class="heading-text">Booking ID :</span></h4>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-package-detail">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="tab-package-detail-left border-right">
							<table>
								<tbody>
									<tr>
										<td>Session Date</td>
										<td>:</td>
										<td>1st Jan 1970 (04:55:44 PM)</td>
									</tr>
									<tr>
										<td>Start Time</td>
										<td>:</td>
										<td></td>
									</tr>
									<tr>
										<td>Visitor</td>
										<td>:</td>
										<td></td>
									</tr>
									<tr>
										<td>Addons</td>
										<td>:</td>
										<td>N/A</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="package-payment-detail-right block-width">
							<table>
								<tbody>
									<tr>
										<td>Ticket price</td>
										<td><i class="fa fa-inr" aria-hidden="true"></i></td>
										
									</tr>
									<tr>
										<td>Addons</td>
										<td><i class="fa fa-inr" aria-hidden="true"></i></td>
										<td></td>
									</tr>
									<tr>
										<td>Discount (Promocode)</td>
										<td><i class="fa fa-inr" aria-hidden="true"></i></td>
										<td></td>
									</tr>
									<tr>
										<td>Internet Handling Charge</td>
										<td><i class="fa fa-inr" aria-hidden="true"></i></td>
										<td>N/A</td>
									</tr>
									<tr class="total-payment">
										<td>PAYABLE PRICE</td>
										<td><i class="fa fa-inr" aria-hidden="true"></i></td>
										<td>N/A</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>			
		</div>
		<div class="custmer-info block-width">
			<div class="top_heading block-width text-center">
				<h3>CUSTOMER INFORMATION</h3>
			</div>
			<div class="custmer-info-inner block-width">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="custmer-info-inner-sec comman-sec block-width">
							<table>
								<tbody>
									<tr>
										<td>Name</td>
										<td>:</td>
										<td>NA</td>
									</tr>
									<tr>
										<td>Contact</td>
										<td>:</td>
										<td>NA</td>
									</tr>
									<tr>
										<td>Email </td>
										<td>:</td>
										<td>NA</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="custmer-info-inner-sec comman-sec block-width">
							<table>
								<tbody>
									<tr>
										<td>Address</td>
										<td>:</td>
										<td>NA</td>
									</tr>
									<tr>
										<td>Town/City</td>
										<td>:</td>
										<td>NA</td>
									</tr>
									<tr>
										<td>Town/City </td>
										<td>:</td>
										<td>NA</td>
									</tr>
									<tr>
										<td>Country </td>
										<td>:</td>
										<td>NA</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="address-row block-width text-center">
			<span class="green-heading">VENUE ADDRESS</span>
			<p>Phoenix Market City, Lower Ground Level 58 - 61, Between Atrium 2 & 6, 15 L.B.S. Marg, Kamani Jn., Kurla (W), Mumbai 400 070, India..</p>
		</div>
			</div>
</div>
<style type="text/css">
.block-width{ width: 100%; float: left; }
.fail-order{ width: 100%; float: left; padding: 15px 0; }
.top_heading{ width: 100%; float: left; }
.top_heading h3 span{ display: inline-block; background:rgb(132, 39, 39); color:#fff;padding: 10px 35px;border-radius: 5px; }
.p-note{width: 100%;float: left;padding: 10px 20px;box-sizing: border-box;border: 1px solid #a5a5a5; border-radius: 5px;margin: 20px 0;}
.p-note p{ margin: 0px;color: #777676;font-size: 15px;line-height: 22px; }
.p-note .warning{ color: #fd3535;padding: 0 10px 0 0 }
.order-sucess-tab{ width: 100%; float: left; }
.order-sucess-tab .order-success-tab-heading{ width: 100%; float: left; background:#303235; padding: 10px 0;}
.order-sucess-tab .order-success-tab-heading h5{ color: #fff; font-size: 22px; line-height: 25px; }
.tab-inner-section{ width: 100%; float: left; border: 1px solid #a5a5a5;}
.payment-detail-tab{ width: 100%; float: left; }
.payment-detail-tab{ width: 100%; float: left; padding: 15px 20px; box-sizing: border-box; }
.type-id{ width: 100%; float: left; }
.type-id h4 { width: 100%; float: left; margin: 12px 0; font-size: 16px; }
.type-id h4 .heading-text{ color: #0098da; font-weight: 600;}
.border-right{border-right:1px solid #a5a5a5; }
.tab-package-detail{ width: 100%; float: left; border-top: 1px solid #a5a5a5;border-bottom: 1px solid #a5a5a5; margin: 0 0 20px 0;  }
.tab-package-detail-left, .package-payment-detail-right{ width: 100%; float: left;padding: 30px 15px; box-sizing: border-box;}
.tab-package-detail-left table,.package-payment-detail-right table { width: 100%; max-width: 100%; border-collapse: collapse; display: table; }
table { width: 100%; max-width: 100%; border-collapse: collapse; display: table; }
table tr td{padding: 5px 0; font-size: 16px; line-height: 18px; border-top:1px solid #a5a5a5; line-height: 25px; color: #393939; }
.package-payment-detail-right table tr td{ border-top: none; line-height: 18px; }
.package-payment-detail-right table tr td:first-child{ width: 50%; }
.tab-package-detail-left table tr:last-child td{border-bottom:1px solid #ddd;}
.package-payment-detail-right table tr.total-payment td{color: #4871b1;font-weight: 500;font-size: 20px;}

.custmer-info .top_heading h3, .disclaim .top_heading h3{ color: #393939; font-weight: 600; }
.custmer-info-inner{ margin: 25px 0px;border-bottom: 1px solid #a5a5a5;border-top: 1px solid #a5a5a5; }
.custmer-info-inner .comman-sec{ padding: 30px 15px; box-sizing: border-box; }
.custmer-info-inner-sec table tr:first-child td{ width:35%;}

.green-heading{font-weight: bold;font-size: 18px;text-transform: uppercase;color: #8BC34A;line-height: 20px;}
.address-row{ padding: 20px 0;border-bottom:1px solid #a5a5a5; }
.address-row p{
	font-size: 14px;
    line-height: 36px;
    margin: 10px 0 0 0;}

</style>
