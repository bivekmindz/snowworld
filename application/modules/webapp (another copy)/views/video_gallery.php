 
        
    <div class="container margin_30 ">        
        <div class="main_title">
            <h2>Video Gallery</h2>
        </div>
        <hr>
        <div class="row magnific-gallery">
            <div class="col-md-3 col-sm-3">
                 <a href="https://www.youtube.com/watch?v=u2DcVN1Zg9c" class="video">
                 <img src="https://i.ytimg.com/vi/u2DcVN1Zg9c/mqdefault.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a href="https://www.youtube.com/watch?v=BS9zDepp238" class="video">
                <img src="https://i.ytimg.com/vi/BS9zDepp238/mqdefault.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a href="https://www.youtube.com/watch?v=TdI1MKw3JOk" class="video">
                <img src="https://i.ytimg.com/vi/TdI1MKw3JOk/mqdefault.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a href="https://www.youtube.com/watch?v=y1g-YjiDPgA" class="video">
                <img src="https://i.ytimg.com/vi/y1g-YjiDPgA/mqdefault.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a href="https://www.youtube.com/watch?v=DrAbBlaq6cI" class="video">
                <img src="https://i.ytimg.com/vi/DrAbBlaq6cI/mqdefault.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a href="https://www.youtube.com/watch?v=KWwG_nh75Vg" class="video">
                <img src="https://i.ytimg.com/vi/KWwG_nh75Vg/mqdefault.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a href="https://www.youtube.com/watch?v=h-aVnFoKF7o" class="video">
                <img src="https://i.ytimg.com/vi/h-aVnFoKF7o/mqdefault.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a href="https://www.youtube.com/watch?v=G3jGCNlOcvs" class="video">
                <img src="https://i.ytimg.com/vi/G3jGCNlOcvs/mqdefault.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a href="https://www.youtube.com/watch?v=a9xB5l1q-Dk" class="video">
                <img src="https://i.ytimg.com/vi/a9xB5l1q-Dk/mqdefault.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a href="https://www.youtube.com/watch?v=yGxGnu2P7uc" class="video">
                <img src="https://i.ytimg.com/vi/yGxGnu2P7uc/mqdefault.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a href="https://www.youtube.com/watch?v=6-B-d7RLrs8" class="video">
                <img src="https://i.ytimg.com/vi/6-B-d7RLrs8/mqdefault.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a href="https://www.youtube.com/watch?v=4v91B-3BCQo" class="video">
                <img src="https://i.ytimg.com/vi/4v91B-3BCQo/mqdefault.jpg" alt="" class="img-responsive styled"></a>
            </div>            
            <div class="col-md-3 col-sm-3">
                <a href="https://www.youtube.com/watch?v=lmMn72eoH_E" class="video">
                <img src="https://i.ytimg.com/vi/lmMn72eoH_E/mqdefault.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a href="https://www.youtube.com/watch?v=xjOeVdXURp8" class="video">
                <img src="https://i.ytimg.com/vi/xjOeVdXURp8/mqdefault.jpg" alt="" class="img-responsive styled"></a>
            </div>          
        </div><!-- End row -->
        <hr>
    </div><!-- End container -->
 
 
   