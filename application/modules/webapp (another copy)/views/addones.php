<style>

    .incere {
        width: 28px;
        height: 28px;
        background: linear-gradient(#fff,#f9f9f9);
        display: inline-block;
        border: 1px solid #c2c2c2;
        cursor: pointer;
        font-size: 16px;
        border-radius: 50%;
        padding-top: 1px;
        line-height: 1;
    }

    .addOnsList input {
		width: 28px;
        text-align: center;
        padding: 0px !important;
        margin-top: 0px !important;
        border-radius: 50%;
        border: 1px solid #cdcdcd; 
	}
	
	.addnew-all-btn{ float: right; }

	.addnew-all-btn .addnew-all-toggle button {
	background: #2fb5dc;
    border: none;
    padding: 6px 9px;
    color: #fff;
    font-size: 15px;
        border-radius: 7px;
	}

	.addnew .ui-c button {
	background: #2fb5dc;
    border: none;
    padding: 6px 9px;
    color: #fff;
    font-size: 15px;
    border-radius: 7px;
	}
	
</style>



<section class="step_payment">


    <div class="container main-container">


        <div id="default-screen" class="content-part"><span id="updateFullPage">


          <section class="row block" id="ticketsAddOnesBlock">
            <div id="searchResult"><span id="ticketsAddOnsOpen"></span><span id="step1Done">
                <div id="ticketsAddOnsDone" class="ticketsAddOnsDone doneBlock aboveAll">
                  <div class="stepHeader stepDone stepDoneHere relative row" id="ticketsAddOnsInfo">
                    <?php require_once('helper/sessiondatetimenoofvisiter.php') ?>

                      <form id="j_idt135" name="j_idt135" method="post" action="" enctype="application/x-www-form-urlencoded">
								<input type="hidden" name="j_idt135" value="j_idt135">
								<a id="j_idt135:step1Edit" href="packages" class="ui-commandlink ui-widget edit" onclick="">
                        <span class="icon-pencil"></span></a>
								<input type="hidden" name="javax.faces.ViewState" value="4914754771058361795:4671725937336393998" autocomplete="off"></form>
                    <h2 class="relative">
                      <span class="steps"><span class="stepNum">Step
                          1</span></span>
                    </h2>
                    <p class="steps-title title1">Search</p>
                  </div>

                </div></span>

            </div>
          </section>



                      <section class="row block" id="ticketsAddOnesBlock">
            <div id="searchResult"><span id="ticketsAddOnsOpen"></span><span id="step1Done">
                <div id="ticketsAddOnsDone" class="ticketsAddOnsDone doneBlock aboveAll">
                  <div class="stepHeader stepDone stepDoneHere relative row" id="ticketsAddOnsInfo">
                  <?php require_once('helper/ticketcost.php') ?>

                      <form id="j_idt135" name="j_idt135" method="post" action="" enctype="application/x-www-form-urlencoded">
							<input type="hidden" name="j_idt135" value="j_idt135">
						  		<a id="j_idt135:step1Edit" href="packagesstep" class="ui-commandlink ui-widget edit" onclick="">
                        <span class="icon-pencil"></span>
						  		</a>
							<input type="hidden" name="javax.faces.ViewState" value="4914754771058361795:4671725937336393998" autocomplete="off"></form>
                    <h2 class="relative">
                      <span class="steps"><span class="stepNum">Step
                          2</span></span>
                    </h2>
                    <p class="steps-title title2">Package
</p>
                  </div>

                </div></span>

            </div>
          </section>

         <form name="formName" id="formName" action="" method="post">
          <section class="row block" id="ticketsAddOnesBlock">
            <div id="searchResult"><span id="ticketsAddOnsOpen">
                  <div id="ticketsAddOnsOpen" class="openBlock aboveAll"><span id="step1Header">
                      <div class="stepHeader stepOpenHere row stepOpen">
                        <p class="fRight subHead">Which addons would you like to add?</p>
                        <h2 class="relative">
                          <span class="steps"><span class="stepNum">Step
                              3</span></span>
                        </h2>
                        <p class="steps-title title3">Addons</p>
                      </div></span><span id="refreshStep1Panel"><span id="step1AddonDisp">
<!-- loop start here --->

					<div id="AddOnsBlock" class="infoBlock relative addOnsBlock">
						
							  <div class="ticketsWithAddOns">
<!-- package loop start here -->								  
					<?php foreach($tmp_array as $v ){ 
								  
								  ?>
								  
								<div class="row ticketdescription mart20">
								  <div class="col-xs-1">      
                  				<?php  $subtotal=0;
									  if( $v->package_image != '') { ?> 
                     						<img style="max-width: 65px;" src="assets/admin/images/<?php echo $v->package_image; ?>">
									  <?php } else { ?>
									  	  <img style="max-width: 65px;" src="img/1463739380583.jpeg">
									  <?php } ?>
									  <input type="hidden" name="cartpackageimage" value="<?php echo $v->package_image; ?>">
								  </div>
								  <div class="col-xs-4">
									<div class="ticket-type "><?php echo $v->package_name; ?></div>
								  </div>
								   <input type="hidden" name="cartpackagename" value="<?php echo $v->package_name; ?>">


								  <div class="col-xs-3 txtcenter visitorsCount mobile-visitorsCount">
									No. Of Visitors: 
									  <span><?php echo $v->qty ? $v->qty : 0; ?></span>
								  </div>
								  <div class="col-xs-4 txtright ">

								  	<p class="reupespps2 mobile-visitorsCount2">
								     <span>
									   <i class="icon-rupee" aria-hidden="true"></i>
									  </span>
									  <?php echo ( $v->package_price * $v->qty ) ? ( $v->package_price * $v->qty ) : 0;  ?>
									</p>
								   	<input type="hidden" name="cartpackageprice" value="<?php echo ( $v->package_price * $v->qty ) ? ( $v->package_price * $v->qty ) : 0; ?>">
									  
								  </div>
									
								</div>  
					<?php } ?>
<!-- package loop end here -->  

<!-- addon loop start here -->								  
					<?php  if($addonsdisplay->scalar!='Something Went Wrong') {   
							foreach ($addonsdisplay as $key => $value) {  ?>
							<div class="row ticketdescription mart20">
										  
								  <div class="col-xs-1">
									  
									   <?php if($value['addonimage']!='') {
										   ?>  <img style="max-width: 52px;height:52px;" src="assets/admin/images/<?php echo $value['addonimage']; ?>">
									   <?php } else { ?>
										   <img style="max-width: 52px; height:52px;" src="img/1463739380583.jpeg">
									   <?php } ?>
									  <input type="hidden" name="cartaddoneimage[]" value="<?php echo $value['addonimage']; ?>">
									  <input type="hidden" name="cartaddoneid[]" value="<?php echo $value['addonid']; ?>">
									  
								  </div>
										  
								  <div class="col-xs-4">
									<div class="ticket-type "><?php echo $value['addonname']; ?>
										
										<input type="hidden" name="cartaddonname[]" value="<?php echo $value['addonname']; ?>">
									</div>
								  </div>
										  
								  <div class="col-xs-3 txtcenter visitorsCount">
									Qty: <span><?php echo $value['addonqty'] ; ?></span>
									<input type="hidden" name="cartaddonqty[]" value="<?php echo $value['addonqty']; ?>">
								  </div>
										  
								  <div class="col-xs-4 txtright">
								   	<input type="hidden" name="cartaddonprice[]" value="<?php echo $value['addonprice']; ?>">
								 		<form name="formName" id="formName" action="" method="post">
										  <p class="reupespps">
											  <span>
												  <i class="icon-rupee" aria-hidden="true"></i>
											  </span>
											  <?php echo $value['addonprice'];$subtotal+= $value['addonprice'];?>
											  <span class="close_but">
												  <button id="" name="delete" class=""  type="submit" role="button" aria-disabled="false" value = "<?php echo $value['tempaddon']; ?>" onclick="this.form.submit();"><i class="fa fa-close" aria-hidden="true"></i></button>
											  </span>
										  </p>
									  </form>
								  <div>
							</div>
						</div>
					</div>
					 <?php }} ?>

<!-- addon loop end here -->

<!--package total price start here --> 								  
  <span id="selectedAddonPanel">


		  <div class="row ticketTotalPrice">
			<div class="col-xs-6" style="padding-left: 15px;">
			  <div class="ticket-type">Package</div>
			</div>
			<div class="col-xs-6 txtright" style="padding-right:6px;">


				<p class="reupespps2 reupespps2ski" >
					<span><i class="icon-rupee" aria-hidden="true"></i></span>
					<?php echo $package_addon_total; ?>
					<input type="hidden" name="cartsubtotal" value="<?php //echo getTotalPrice($tmp_price_array); ?>">      
				</p>
			
			</div>
		  </div>
		  
	</span>
								  
<!-- package price end here--> 		
								  
<!-- addon price start here--> 									  
								  
      <span id="selectedAddonPanel">								  
								  
		  <div class="row ticketTotalPrice">
			<div class="col-xs-6" style="padding-left: 15px;">
			  <div class="ticket-type">Addons </div>
			</div>
			<div class="col-xs-6 txtright" style="padding-right:6px;">

				
					<p class="reupespps2 reupespps2ski">
						<span class="addon_total_price" ><i class="icon-rupee" aria-hidden="true"></i> 0</span>
					</p>
				
			</div>
		  </div>					
		  
		</div>									  
<!-- addon price end here--> 
								  
<!--package & addon total price start here --> 								  
  <span id="selectedAddonPanel">


		  <div class="row ticketTotalPrice">
			<div class="col-xs-6" style="padding-left: 15px;">
			  <div class="ticket-type">Total</div>
			</div>
			<div class="col-xs-6 txtright" style="padding-right:6px;">


				<p class="reupespps2 reupespps2ski" >
					<span class="package_addon_total_price"><i class="icon-rupee" aria-hidden="true"></i></span>
				</p>
			
			</div>
		  </div>
		  
	</span>
								  
<!-- package & addon price end here--> 										  
								  
<!--  addon session loop start here -->	
<div id="addOnsListBlock" class="infoBlock relative addOnsListBlock">								  
  <div class="widget-container ticketDetailsList ">
    <div class="widget-header">
		
        <span class="widget-title">Would you like some Addons?</span>
		
		<!-- 	addnew-all, addnew-all-toggle
				- is bind with query 
				- no css is bind with it
		--> 
		<span class="addnew-all addnew-all-btn" name="" row-id="" id="" type="button">
			<span class="ui-button-text ui-c addnew-all-toggle">
				<button type="button" >Add All</button>
			</span>
		</span>		
		
    	</div>	
    <div class="widget-content">
      <div class="addOnsListContainer">
<?php 
		  
		 $tmp = [];
		 if( count( $addonscart ) ){ 
			foreach ($addonscart as $key => $value) {	
				$tmp[$key] = (object) $value;
			}
		 }
?>		  
		  
<script>		  
	
	var select_package = []; 
	var	get_quantity_price = []; 
	
	
	(select_package.length) ? $('.continue button[type="submit"]').attr('disabled', false) : $('.continue button[type="submit"]').attr('disabled', true);
	(select_package.length) ? $('.package_addon_total_price').show() : $('.continue button[type="submit"]').hide();
	
	//showItem();  // show default price 
	
	var temp_array = <?php echo json_encode($tmp); ?>;
	
	var package_price = <?php echo $package_addon_total; ?>;
	
	 (temp_array.length == 0) ? $('.continue button[type="submit"]').hide() : $('.continue button[type="submit"]').show();

	
	
	function showItem( param = 0, param1 = 0 ){
	
		param ? $('.total-price-div').show() : $('.total-price-div').hide();
		param ? $('.addon_total_price').html( '<i class="icon-rupee" aria-hidden="true"></i>' + parseFloat(param) ) : $('.addon_total_price').html( '<i class="icon-rupee" aria-hidden="true"></i>' + 0 );
		param ? $('.package_addon_total_price').html( '<i class="icon-rupee" aria-hidden="true"></i>' + ( parseFloat(param) + parseFloat(param1) ) ) : $('.package_addon_total_price').html( '<i class="icon-rupee" aria-hidden="true"></i>' + parseFloat(param1) );
	
	} 
	
	function getCalculatedPrice(param){
	
		if( param.length ){

			return param.map(function(item){ return item.qty * item.addon_price }).reduce( (x, y) => parseInt(x) + parseInt(y) );

		}else{

			return 0;
		}
	}
	
	function updateQuantity(val, id){
		
		var updated_price_array = '';
		var to_remove_index = get_quantity_price.map(function(item) { return parseInt(item.addon_id); }).indexOf( parseInt(id) );

		(to_remove_index == -1) ? '' : get_quantity_price[to_remove_index].qty = parseInt(val);

		updated_price_array = getCalculatedPrice(get_quantity_price);
		
		
		$('#final-selected-data').val( JSON.stringify(get_quantity_price) );

		showItem( updated_price_array, parseInt(package_price ) );
	
	}	
	

	
</script>		  
			  

 <?php  $countaddone = 0; 
		 foreach ($addonscart as $key => $value) {

     	 if($value=='Something Went Wrong') {echo "No Records";} else {

			 if($this->session->userdata('addonquantitys')!='') {

				 $addonqtyval= (explode(",",$this->session->userdata('addonquantitys')));
				 $addonqtyval[$countaddone]=$addonqtyval[$countaddone];
			 }
			 
         else{
			 
             $addonqtyval[$countaddone]=1;
         }

         ?>



         <script type="text/javascript">
function incrementValue<?php echo $value['addon_id']; ?>()
{
    //  alert('number<?php echo $value['addon_id']; ?>');
    var a = parseInt($("#mainnumbervalues<?php echo $value['addon_id']; ?>").val());
    var b = parseInt($("#mainnumber<?php echo $value['addon_id']; ?>").html());
    var c = parseInt($("#mainnumbervalue<?php echo $value['addon_id']; ?>").val());

    var value = parseInt(document.getElementById('number<?php echo $value['addon_id']; ?>').value);
	console.log( value );
    value = isNaN(value) ? 0 : value;
    if(value<19 && value>0){
        value++;
        b = b+a;
        c = value*a;

        $("#mainnumbervalue<?php echo $value['addon_id']; ?>").val(c);
		$("#categories-<?php echo $value['addon_id']; ?>").prop('disabled', false);		
		
		$(".categories-id-<?php echo $value['addon_id']; ?>").val('1');
		
        $("#mainnumber<?php echo $value['addon_id']; ?>").html(b);
		//toggleAddAll();
		updateQuantity( value, <?php echo $value['addon_id']; ?>);
		
        document.getElementById('number<?php echo $value['addon_id']; ?>').value = value;
		
		$('.categories-id-'+<?php echo $value['addon_id']; ?>).val( 1 );

    }
}
function decrementValue<?php echo $value['addon_id']; ?>()
{

    var a = parseInt($("#mainnumbervalues<?php echo $value['addon_id']; ?>").val());
    var b = parseInt($("#mainnumber<?php echo $value['addon_id']; ?>").html());
    var c = parseInt($("#mainnumbervalue<?php echo $value['addon_id']; ?>").val());
    var value = parseInt(document.getElementById('number<?php echo $value['addon_id']; ?>').value);
    value--;
	value = isNaN(value) ? 0 : value;
	
    if( value>0 ){
        b = b-a;
        c = value*a;
			if( value == 1 ){ 

				$("#categories-<?php echo $value['addon_id']; ?>").prop('disabled', true);
				$(".categories-id-<?php echo $value['addon_id']; ?>").val('0');
			}else{
				$(".categories-id-<?php echo $value['addon_id']; ?>").val('1');
			}		
        $("#mainnumbervalue<?php echo $value['addon_id']; ?>").val(c);
        $("#mainnumber<?php echo $value['addon_id']; ?>").html(b);
		
		//toggleAddAll();
		updateQuantity( value, <?php echo $value['addon_id']; ?>);		
		
        $('#number'+<?php echo $value['addon_id']; ?>).val( parseInt(value) );
		//$('.categories-id-'+<?php echo $value['addon_id']; ?>).val( parseInt(value) );
		
        document.cookie = "myJavascriptVar = " + value
    }

}
			 
			 
	 

			 
</script>          
		  <div class="row addOnsList each-row-<?php echo $value['addon_id']; ?>">
            <div class="col-xs-1 width1001-ski">
          	  <span data-target="#addOns_<?php echo $value['addon_id']; ?>" data-parent="#addOnsListBlock" data-toggle="collapse" class="view-info">
 				<img src="<?php if($value['addon_image']!='') { ?>assets/admin/images/<?php echo $value['addon_image']; ?><?php } else { ?>img/info.png<?php } ?>">

	                  
				</span>

					
            </div>
			  
            <div class="col-xs-2 width1001-ski">
              <span class="ticket-type1"> <?php echo $value['addon_name']; ?>
                  <input type="hidden" name="addonname[]" value="<?php echo $value['addon_name']; ?>"></span>
            </div>

            <div class="col-xs-2 txtcenter width1001-ski">
                        <div class="product_text">

                        	<h4>Qty</h4>
                        <div>
							
							<input type="button" class="incere " onclick="decrementValue<?php echo $value['addon_id']; ?>()" value="-">


						<div class="test-incre">
							 <input type="text" readonly id="number<?php echo $value['addon_id']; ?>" class="inen" name="addonqty[]" maxlength="2" value="<?php 
									 //echo $addonqtyval[$countaddone];
									echo $package_total_qty;																							
									?>"></div>
							  &nbsp;<input type="button" class="incere abouste-ski" onclick="incrementValue<?php echo $value['addon_id']; ?>()" value="+"></div>

						</div>
			</div>


            <div class="col-xs-3 txtright width1001-ski">

            <p class="reupespps1">
		       <span><i class="icon-rupee" aria-hidden="true"></i></span><span id="mainnumber<?php echo $value['addon_id']; ?>"><?php echo (int) $package_total_qty * (float) $value['addon_price']; ?></span> </p>
					<input type="hidden" name="addonprice[]" id="mainnumbervalue<?php echo $value['addon_id']; ?>" value="<?php echo (int) $package_total_qty * (float) $value['addon_price']; ?>">
					<input type="hidden" name="addonprice1[]" id="mainnumbervalues<?php echo $value['addon_id']; ?>" value="<?php echo (int) $value['addon_price']; ?>">
                </label>

              <span style="float: right; margin-top: 13px;">
				  
				<input type="hidden" name="addonid[]"  value="<?php echo $value['addon_id']; ?>">
			</span>
		  
            </div>

            <div class="col-xs-2 width1001-ski">
				<div style=" padding-top: 23px;">
					<button name="categories1[]" id="categories-<?php echo $value['addon_id']; ?>" class="hide ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only btn btn-info btn-add"  type="submit" role="button" aria-disabled="false" value = "<?php echo $value['addon_id']; ?>" onclick="this.form.submit();">
						<span class="ui-button-text ui-c" >Add</span>
					</button>
					
					<div class="addnew" name="" row-id="<?php echo $value['addon_id']; ?>" id="categories-<?php echo $value['addon_id']; ?>" type="button" >
						<span class="ui-button-text ui-c addnew-<?php echo $value['addon_id']; ?>" row-id="<?php echo $value['addon_id']; ?>" ><button type="button" >Add</button></span>
					</div>
					
						<input type="hidden" class="categories-id-<?php echo $value['addon_id']; ?>" name="categories1[]" value="0"/>
<!-- -<?php //echo $key; ?> -->
				</div>
            </div>
		  
            <div class="addOnsInfo collapse" id="addOns_<?php echo $value['addon_id']; ?>">The most useful apparel to keep your feet warm in such cold temperature. Its not compulsory but necessary.</div>
		  
          </div>
         <?php $countaddone++;}} ?>
<!--    </form>  -->
		<input type='hidden'id="final-selected-data" name="final_selected_addon_data" class="final-selected-data" value="" />
      </div>

    </div>
  </div>
			  
<!--  addon session loop end here -->			  
							
								  
					  
		<div class="row">
		  <div class="col-xs-12 txtcenter">
			<div class="continue">


	<button id="" name="submit" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only btn btn-custom btn-prime" type="submit" role="button" aria-disabled="false" value="cartses"><span class="ui-button-text ui-c">CONTINUE</span></button>


			</div>
		  </div>
		</div>

  </div>
</div>
<!-- loop end here --->					  
</form>

<!--<form id="addonFormId" action="" method="post" >	-->				  

</div>

</span></span>
        </div></span>

    </div>


<?php if($this->session->userdata('skiindia')>0)  { ?>
<div style="clear:both"></div>
    <section id="paymentBlock" class="row block relative"><span id="beforePanel3">
                <div id="summaryClose" class="stepHeader stepClose row summaryClose closeBlock">
                  <p class="fRight subHead"></p>
                  <h2 class="relative">
                    <span class="steps"><span class="stepNum">Step 4</span></span>
                  </h2>
                  <p class="steps-title title4">Summary</p>
                </div></span><span id="step3open"></span>

    </section>

    <section id="paymentBlock" class="row block relative hide"><span id="beforePanel3">
                <div id="summaryClose" class="stepHeader stepClose row summaryClose closeBlock">
                  <p class="fRight subHead"></p>
                  <h2 class="relative">
                    <span class="steps"><span class="stepNum">Step 5</span></span>
                  </h2>
                  <p class="steps-title title5">Payment</p>
                </div></span><span id="step3open"></span>

    </section>

<?php } else { ?>





    <section id="paymentBlock" class="row block relative"><span id="beforePanel3">
                <div id="summaryClose" class="stepHeader stepClose row summaryClose closeBlock">
                  <p class="fRight subHead"></p>
                  <h2 class="relative">
                    <span class="steps"><span class="stepNum">Step 4</span></span>
                  </h2>
                  <p class="steps-title title4">Login / Register</p>
                </div></span><span id="step3open"></span>

    </section>


    <section id="paymentBlock" class="row block relative"><span id="beforePanel3">
                <div id="summaryClose" class="stepHeader stepClose row summaryClose closeBlock">
                  <p class="fRight subHead"></p>
                  <h2 class="relative">
                    <span class="steps"><span class="stepNum">Step 5</span></span>
                  </h2>
                  <p class="steps-title title5">Summary</p>
                </div></span><span id="step3open"></span>

    </section>

    <section id="paymentBlock" class="row block relative hide"><span id="beforePanel3">
                <div id="summaryClose" class="stepHeader stepClose row summaryClose closeBlock">
                  <p class="fRight subHead"></p>
                  <h2 class="relative">
                    <span class="steps"><span class="stepNum">Step 6</span></span>
                  </h2>
                  <p class="steps-title title6">Payment</p>
                </div></span><span id="step3open"></span>

    </section>

<?php } ?>


</span>
</div>
</div>

</section>


<script>

$(document).ready(function(){
	
	showItem(0, <?php echo $package_addon_total; ?>);  // show default price
	
	
	$('.addnew-all').on('click', function(){
		
		var selected_addon = [];
		
		if( (select_package.length < temp_array.length && select_package.length >= 0) ){  //alert();
			 
				temp_array.map(function(item){
					
						if( item.qty = parseInt(document.getElementById('number'+item.addon_id).value) ){

							
    						  if( select_package.indexOf( item.addon_id.trim() ) == -1 ){
   										
										select_package.push( item.addon_id );
										get_quantity_price.push( item );
								  
										$('.each-row-'+item.addon_id).css('background-color', 'lightgrey');

										updateQuantity( item.qty, item.addon_id );							

										$('.addnew-'+item.addon_id).html('<button type="button" >Remove</button>');

										$('.addnew-all-toggle').html('<button type="button" >Remove All</button>');								  
								
                                }							



						}
					
					});
			
				 $('#final-selected-data').val( JSON.stringify(get_quantity_price) );
		}else{ 
			
		
		if( (select_package.length == temp_array.length) ){
			
				select_package.length = get_quantity_price.length = 0;
			
				temp_array.map(function(item){
					
						if( item.qty = parseInt(document.getElementById('number'+item.addon_id).value) ){
							
							$('.each-row-'+item.addon_id).css('background-color', '');

							$('.addnew-'+item.addon_id).html('<button type="button" >Add</button>');
							
							updateQuantity( item.qty, item.addon_id );							
							
							$('.addnew-all-toggle').html('<button type="button" >Add All</button>');			
						}
					
					});		
			
				$('#final-selected-data').val( JSON.stringify(get_quantity_price) );
			}
		}
		
	});
	
	$('.addnew').on('click', function(){  
		
		var row_id = $(this).attr('row-id');

		var package_id = row_id;
		var value = parseInt(document.getElementById('number'+row_id).value);

				if( ( select_package.indexOf(package_id) == -1 ) ) {

			
					temp_array.map(function(item){

						if( item.addon_id == (package_id) ){  

						select_package.push( item.addon_id );
							
						get_quantity_price.push( item );

						updateQuantity( value, package_id );

						$('.each-row-'+row_id).css('background-color', 'lightgrey');

						$('#final-selected-data').val( JSON.stringify(get_quantity_price) );

						$('.addnew-'+row_id).html('<button type="button" > Remove </button>');
						
						}

					}); 

				}else{

						var item_index = select_package.indexOf(package_id);
						select_package.splice( item_index, 1 );

						$('.each-row-'+row_id).css('background-color', '');

						to_remove_index = get_quantity_price.map(function(item) { return item.addon_id; }).indexOf(package_id);

						get_quantity_price.splice(to_remove_index, 1);

						updateQuantity( value, package_id );


						$('#final-selected-data').val( JSON.stringify(get_quantity_price) );
						$('.addnew-'+row_id).html('<button type="button" >Add</button>');

				}
				
			//	(select_package.length) ? $('.continue button[type="submit"]').attr('disabled', false) : $('.continue button[type="submit"]').attr('disabled', true);

		});
	
	});	

</script>

<!-- End Container -->
    