
    
 <div class="container margin_30">
    <div class="col-md-12 box_style_1">
      <h3><i class="icon-ticket"></i> Tickets Price</h3>
      <div class="col-md-8 nopadding">
              <div class="col-lg-12 ticket box_style_1">
                  <div class="col-lg-2 ticket-icon"> 
                    <img SRC="img/ticket_icon.jpg" alt="Ticket Icon" class="img-responsive img-rounded"/> 
                  </div>                  
                  <div class="col-lg-7">
                    <div class="ticket-type">Regular</div>
                    <p class="scifitext">This ticket includes all activities in Snow like Ski, Sledging, Toboggan, Bob Sled, Snow Fall, Ice Skating, Snow Boarding, Caves Frozen Walk Through, Igloo & Snow gears like Jackets, Hood, Gloves, Boots, Ice Skating Shoes, Ski & Ski Sticks.</p>
                  </div>                  
                  <div class="col-lg-3 text-center ticket-price-secion">
                    <div class="rupee"><i class="icon-rupee"></i> 1,150/-</div>
                    <div class="visitorsCount">( Included Taxes )</div>
                  </div>                              
                </div> 
                
                <!--<div class="col-lg-12 ticket box_style_1">
                  <div class="col-lg-2 ticket-icon"> 
                    <img src="img/ticket_icon.jpg" alt="Ticket Icon" class="img-responsive img-rounded"/> 
                  </div>                  
                  <div class="col-lg-7">
                    <div class="ticket-type">Corporate</div>
                    <p class="scifitext">This ticket includes all activities in Snow like Ski, Sledging, Toboggan, Bob Sled, Snow Fall, Ice Skating, Snow Boarding, Caves Frozen Walk Through, Igloo & Snow gears like Jackets, Hood, Gloves, Boots, Ice Skating Shoes, Ski & Ski Sticks.</p>
                  </div>                  
                  <div class="col-lg-3 text-center ticket-price-secion">
                    <div class="rupee"><i class="icon-rupee"></i> 1,000/-</div>
                    <div class="visitorsCount">( Included Taxes )</div>
                  </div>                              
                </div> 
                
                <div class="col-lg-12 ticket box_style_1">
                  <div class="col-lg-2 ticket-icon"> 
                    <img src="img/ticket_icon.jpg" alt="Ticket Icon" class="img-responsive img-rounded"/> 
                  </div>                  
                  <div class="col-lg-7">
                    <div class="ticket-type">Student</div>
                    <p class="scifitext">This ticket includes all activities in Snow like Ski, Sledging, Toboggan, Bob Sled, Snow Fall, Ice Skating, Snow Boarding, Caves Frozen Walk Through, Igloo & Snow gears like Jackets, Hood, Gloves, Boots, Ice Skating Shoes, Ski & Ski Sticks.</p>
                  </div>                  
                  <div class="col-lg-3 text-center ticket-price-secion">
                    <div class="rupee"><i class="icon-rupee"></i> 700/-</div>
                    <div class="visitorsCount">( Included Taxes )</div>
                  </div>                              
                </div>-->     
            </div>
            
            <div class="col-md-4">
                <div class="box_style_2">
          <i class="icon-phone"></i>
          <h4>Need help? Call us</h4>
          <a href="tel://01202595150"> +91 120 259 51 50</a> / 
                            <a href="tel://01202595151">51</a> / 
                            <a href="tel://01202595152">52</a> / 
                            <a href="tel://01202595153">53</a><br>
          <small>Monday to Sunday 11.00am - 10.00pm</small>
        </div>
            </div><!-- End col-md-4 -->
      </div>       
    </div>  