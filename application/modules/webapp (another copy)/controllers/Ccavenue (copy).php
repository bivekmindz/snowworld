<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ccavenue extends MX_Controller {
	
	private $current_order_payment_flag = 0 ; 
	
	private $credit_debit_flag = 'D' ;
	
	private $agent_wallet_last_insert_id = 0 ;
	
	
    public function __construct() {
        $this->load->model("supper_admin");
        $this->load->library('session');
         session_start();
    }
	
	
	public function agentPayWallet( ) {
		
		//p( $this->session->userdata );
		
/* agnet payemnt start here */
		
	   		$total_promo_price = '';
				
			$final_selected_package_data_array = getPackageTotalPrice( $this->session->userdata( 'final_selected_package_data' ) );
	   
	   			$total_promo_price	= $this->session->userdata( 'total_promo_price' ) ;
		// p( $this->session->userdata ) ; 
	   
	   			$user_id = $this->session->userdata( 'user_id' );
		
       // $user_id = $this->session->userdata( 'ppid' );
				 $prepare_proc_param_package_addon_promocode = [

					    'param' => 'agent_payment' ,
						'param1' => $this->session->userdata( 'package_total_qty' ) ,
						'param2' => $this->session->userdata( 'package_total_price' ) ,
						'param3' => $this->session->userdata( 'addon_total_price' ),
						'param4' => $this->session->userdata( 'addon_total_qty' ),
						'param5' => implode( '#', $final_selected_package_data_array['package_name_array'] ) ,
						'param6' => implode( '#', $final_selected_package_data_array['get_package_img_array'] ) , 
						'param7' => implode( '#', $final_selected_package_data_array['package_price_array'] ) , 
						'param8' => empty( $this->session->userdata( 'total_promo_price' ) ) ? $this->session->userdata( 'before_payment_total_purchase_amount' ) : $this->session->userdata( 'total_promo_price' ),
						'param9' => '',
						'param10' => ! empty( $this->session->userdata( 'saving_price_promocode' ) ) ? $this->session->userdata( 'saving_price_promocode' ) : 0,
						'param11' => ! empty( $this->session->userdata( 'promocode_name' ) ) ? $this->session->userdata( 'promocode_name' ) : 0 ,    // 11
						'param12' => $this->session->userdata( 'handling_charge_with_no_of_person' ),
						'param13' => $this->session->userdata( 'final_cost_package_addon_handling' ),
						'param14' => ! empty( $this->session->userdata( 'totalvalue' ) ) ? 0 : 0 ,
						'param15' => ! empty( $this->session->userdata( 'orderlastinsertid' ) ) ? 0 : 0 
					 
				 		];

				$get_inventory_id = explode('-', $this->session->userdata( 'destinationType' ) ) [ 0 ];

				$prepare_proc_param_billingid_orderid_userid_urls = [

						'param16' => $this->session->userdata( 'titlewallet' ),
						'param17' => $this->session->userdata( 'billing_namewallet' ) ,
						'param18' => $this->session->userdata( 'billing_emailwallet' ) ,
						'param19' => $this->session->userdata( 'billing_telwallet' ) ,
						'param20' => $this->session->userdata( 'billing_cometoknow' ) ,
						'param21' => $this->session->userdata( 'billing_addresswallet' ) ,
						'param22' => $this->session->userdata( 'billing_citywallet' ) ,
						'param23' => $this->session->userdata( 'billing_statewallet' ) ,
						'param24' => $this->session->userdata( 'billing_zipwallet' ) ,
						'param25' => $this->session->userdata( 'billing_countrywallet' ) ,
						'param26' => $this->session->userdata( 'CaptchaInputwallet' ) ,
						'param27' => $this->session->userdata( 'txtDepartDate' ) ,
						'param28' => ! empty( $this->session->userdata( 'tid' ) ) ? $this->session->userdata( 'tid' ) : 0 ,
						'param29' => ! empty( $this->session->userdata( 'merchant_id' ) ) ? $this->session->userdata( 'merchant_id' ) : 0 ,
						'param30' => ! empty( $this->session->userdata( 'order_id' ) ) ? 0 : 0 ,
						'param31' => ! empty( $this->session->userdata( 'order_idval' ) ) ? 0 : 0 ,
						'param32' => '' ,
						'param33' => $this->session->userdata( 'amount' ) ,
						'param34' => ! empty( $this->session->userdata( 'currency' ) ) ? $this->session->userdata( 'currency' ) : 0,
						'param35' => ( $get_inventory_id ) ? $get_inventory_id : 0 ,
						'param36' => $this->session->userdata( 'cancel_url' )
						];				
				//p( $prepare_proc_param_billingid_orderid_userid_urls );
					$time_slot_array = getTimeSlotInArray( $this->session->userdata( 'destinationType' ) );
				
					$final_selected_addon_data_array = getAddonkeyValue( $this->session->userdata( 'final_selected_addon_data' ) );
				    
					$payment_flag = empty( $this->session->userdata( 'payment_flag' ) ) ? 0 :  array_sum( $this->session->userdata( 'payment_flag' ) ) ;
		
	   // 0 =  wallet
	   // 2 = ccavenue 
	   // 3	= wallet + ccavenue
	   				$payment_mode = ( $payment_flag == 0 ) ? 'Wallet' : 'NULL';
	   				$payment_mode = ( $payment_flag == 2 ) ? 'Ccavenue' : 'NULL';		
	   				$payment_mode = ( $payment_flag == 3 ) ? 'Wallet & Ccavenue' : 'NULL';

	   
	  				$get_formatted_hr_min_am_pm_array =	explode('-', completeTimeSlotWithAmPmAgent( getTimeSlotInArray( $this->session->userdata( 'destinationType' ) ) ) );
		
	  				$get_formatted_timeslot_start =	explode(':', $get_formatted_hr_min_am_pm_array[ 0 ] );
	   				$get_formatted_timeslot_end =	explode(':', $get_formatted_hr_min_am_pm_array[ 1 ] );
		
	   		#before_payment_after_wallet_deduction_to_pay_ccavenue
					$prepare_proc_param_future_param = [ 
						'param37' => $this->session->userdata( 'before_payment_after_wallet_deduction_to_pay_wallet' ),    // wallet amount
						'param38' => $this->session->userdata( 'final_cost_package_addon_handling' ),      // subtotal
						'param39' => $this->session->userdata( 'countryid' ),
						'param40' => $this->session->userdata( 'stateid' ),
						'param41' => $this->session->userdata( 'cityid' ),
						'param42' => $this->session->userdata( 'branch_id' ),
						'param43' => $this->session->userdata( 'locationid' ),
						'param44' => $this->session->userdata( 'uniqid' ),
						
						'param45' => trim( $get_formatted_timeslot_start[ 0 ] ),
						
						'param46' => trim( $get_formatted_timeslot_start[ 1 ] ),
						
						'param47' => trim( $get_formatted_timeslot_end[ 0 ] ),
						'param48' => trim( $get_formatted_timeslot_end[ 1 ] ),
						'param49' => trim( $get_formatted_timeslot_start[ 2 ] ),
						'param50' => trim( $get_formatted_timeslot_end[ 2 ] ),
						'param51' => $this->session->userdata( 'ddAdult' ),
						// used later 
						'param52' => ( ( $payment_flag == 0 ) || ( $payment_flag == 2 ) || ( $payment_flag == 3 ) ) ? 1 : 0 ,
						'param53' => $payment_mode ,
						'param54' => '' ,
						'param55' => implode( '#', $final_selected_package_data_array['package_qty_array'] ) ,
						'param56' => '' ,
						'param57' => '' ,
						
						'param58' => uniqid() ,
						'param59' => 1 ,
						'param60' => 'paid successfully' ,
						'param61' => '' ,
						'param62' => '' ,
						'param63' => 'wallet',
						'param64' => empty( $this->session->userdata( 'total_promo_price' ) ) ? $this->session->userdata( 'before_payment_total_purchase_amount' ) : $this->session->userdata( 'total_promo_price' ) ,
						'param65' => $user_id 
						];
	   
					$agent_payment_response = $agent_agentcustomer_inserted_id = '';
	   
					$agent_customer_response_object = $agent_payment_response = $agent_agentcustomer_inserted_id = $agent_commission_inserted_id = '';	
	   
					$agent_payment_param  = array_merge( $prepare_proc_param_package_addon_promocode, $prepare_proc_param_billingid_orderid_userid_urls, $prepare_proc_param_future_param ) ;
			
	   // p( $agent_payment_param ); 
	   // p( $this->session->userdata );
	   // exit;
		
		//		try{			
					
//					$this->db->trans_begin();	
		
				   if( $user_id ){
					
					  /* insert agent customer details  */

					   		$agent_payment_param[ 'param' ] = 'insert_agent_customer_details' ;
														
							$agent_customer_response_object = $this->supper_admin->call_procedureRow( 'proc_agent_payment', $agent_payment_param );
					   
							$agent_agentcustomer_inserted_id = (int) $agent_customer_response_object->lastinsert_agentcustomer_id;

				   }	   
	   
	   				/*  this param32 is acutal agent customer id */
	   
		/*
						switch( $paymentmode ){
								
							case 1 :	
								
							break;	
								
							case 2 :
								
							break;	
								
							case 3 :									
								
							break;	
						
						}		
						
		*/
	   				
	   
	   				if( $agent_agentcustomer_inserted_id ) {
						
	   						$agent_payment_param[ 'param' ] = 'agent_payment';
						
			   				$agent_payment_param[ 'param64' ] = (int) empty( $agent_agentcustomer_inserted_id ) ? 0 : $agent_agentcustomer_inserted_id ;
						
//			   				$agent_payment_param[ 'param64' ] = (int) empty( $agent_agentcustomer_inserted_id ) ? 0 : $agent_agentcustomer_inserted_id ;
					
							 /*  insert - tbl_orderpackage  order details saved  */	   
							$agent_payment_response = $this->supper_admin->call_procedureRow( 'proc_agent_payment', $agent_payment_param );				
					}
	   
					// hold addon last insert ids 
					$agent_payment_addon_inserted_id = [];
		
					$agent_payment_addon_response_object = '';
		
	   				$order_status = 0;
		
	   				$agent_payment_param[ 'param64' ] = $agent_payment_param[ 'param8' ];
				
					if( isset( $agent_payment_response->lastinsert_id ) ) {
						
						// set order status flag ( 1 ( success ), 0 ( failed ) )  
						
						$this->session->set_userdata( 'agent_order_last_insert_id', $order_status = $agent_payment_response->lastinsert_id );
						
						foreach( $final_selected_addon_data_array['addon_name_array'] as $kk => $vv ) {

							$agent_payment_param[ 'param' ] = 'insert_addon' ;
							$agent_payment_param[ 'param52' ] = $final_selected_addon_data_array['addon_name_array'][ $kk ] ; 
							$agent_payment_param[ 'param32' ] = $agent_commission_inserted_id ; 
							$agent_payment_param[ 'param53' ] = $final_selected_addon_data_array['addon_qty_array'][ $kk ] ; 
							$agent_payment_param[ 'param54' ] = $final_selected_addon_data_array['addon_price_array'][ $kk ] ;
							$agent_payment_param[ 'param55' ] = $final_selected_addon_data_array['addon_image_array'][ $kk ] ;
							$agent_payment_param[ 'param56' ] = $final_selected_addon_data_array['addon_id_array'][ $kk ] ;
							$agent_payment_param[ 'param57' ] = ( $agent_payment_response->lastinsert_id ) ? $agent_payment_response->lastinsert_id : 0 ;
							  
							$agent_payment_addon_response_object = $this->supper_admin->call_procedureRow( 'proc_agent_payment', $agent_payment_param );
							
							$agent_payment_addon_inserted_id[] = $agent_payment_addon_response_object->lastinsert_addon_id;
						}
						
					}
	   
					// hold last insert id
   				    $agent_payment_promo_inserted_id = $response_object = '';
	   				
				  /*
				   * if promo code is applied
				   * insert promo code details 
				   */		
		
				   if( $total_promo_price ){
					

					   		$agent_payment_param[ 'param' ] = 'insert_promo_data' ;
					        $agent_payment_param[ 'param56' ] = $this->session->userdata( 'promo_id' ) ;
					   
							$response_object = $this->supper_admin->call_procedureRow( 'proc_agent_payment', $agent_payment_param );
							$agent_payment_promo_inserted_id = $response_object->lastinsert_userpromo_id;					   
				   	}
					
//					$this->db->trans_commit();
		
		//		}catch (Exception $e) {
					
//					$this->db->trans_rollback();
					
		//		}
									
				  /*
				   * insert agent agent commission details 
				   * not in used 
				   */
		
			 		$insert_agentcommission_data = function ( $agent_payment_param ) use ( &$agent_commission_inserted_id, $order_status ) {
						
								$agent_payment_param[ 'param' ] = 'insert_agentcommission_details' ;
						
						   		$agent_payment_param[ 'param32' ] = $order_status ;	
						
								$response_object = $this->supper_admin->call_procedureRow( 'proc_agent_payment', $agent_payment_param );

								$agent_commission_inserted_id = $response_object->lastinsert_agentcommission_id;						
					
					};
		
		
				
					// wallet redirect
		
					if( ( $payment_flag == 0 ) ) {
						
						if( $order_status ) { 
							
								// $insert_agentcommission_data( $agent_payment_param );

								// $agent_commission_inserted_id ? redirect('/agentamountsucess') : exit ;
								
							//redirect('/agentamountsucess') ;
							redirect('/ccavResponseHandler') ;
							exit;
							
						} else {
							
							redirect('/agentfail') ;
							exit;
						}

					}	  

					// wallet + ccavenue redirect
					if ( $payment_flag == 3 ) {
						
						// $insert_agentcommission_data( $agent_payment_param );
//ECHO $agent_commission_inserted_id;
//						( $agent_commission_inserted_id ) ? redirect( '/ccavRequestHandler' ) : exit ;
													redirect('/ccavResponseHandler') ;
						//redirect( '/ccavRequestHandler' ) ; 
						exit;
						
						// $agent_commission_inserted_id ? redirect( '/agentamountsucess' ) : exit ;
						
						//redirect( '/agentfail' ) ;
					}
		
					// ccavenue redirect
					if ( $payment_flag == 2 ) { 

						redirect('/ccavRequestHandler'); 
						exit;
						// redirect('/agentamountsucess');
						 //redirect('/agentamountfail1');

					} 		
		
				//  p( $this->session->userdata );
				//	exit;
				
				//	 $agent_paymenthistory_inserted_id = '';
				/*  
				   if( $this->session->userdata( 'user_id' ) ){
					
					  
					   #insert agent payment history details 
					   
					   		$agent_payment_param[ 'param' ] = 'insert_payment_history' ;
														
							$response_object = $this->supper_admin->call_procedureRow( 'proc_agent_payment', $agent_payment_param );
							$agent_paymenthistory_inserted_id = $response_object->lastinsert_paymenthistory_id;					   
				   }
	   
	   			*/
				
				// fetch agent wallet amount
				/*
							$agent_payment_param[ 'param' ] = 'get_wallet_amount' ;
														
							$response_object = $this->supper_admin->call_procedureRow( 'proc_agent_payment', $agent_payment_param );
				*/
							// p( $response_object );				
							// $response_object->total_amount;
				/*	
							if( $response_object->total_amount ) {
								
								$agent_payment_param[ 'param' ] = 'update_wallet_amount' ;
								$agent_payment_param[ 'param33' ] =  ( $response_object->total_amount - $this->session->userdata( 'amount' ) );
								
								$agent_payment_param[ 'param63' ] = $response_object->wid; // wallet id   must will change to user id 
								// remember 'param63' => 'wallet' 

								$response_object = $this->supper_admin->call_procedureRow( 'proc_agent_payment', $agent_payment_param );								
							  p( $response_object );
							}
	   			*/
		
	   			/*
					   if( $user_id && ( $agent_payment_response->lastinsert_id ) ? $agent_payment_response->lastinsert_id : 0 ) {
				*/
						  /*
						   * insert agent agent commission details 
						   */
				/*
								$agent_payment_param[ 'param' ] = 'insert_agentcommission_details' ;
						   		$agent_payment_param[ 'param32' ] = ( $agent_payment_response->lastinsert_id ) ? $agent_payment_response->lastinsert_id : 0 ;

								$response_object = $this->supper_admin->call_procedureRow( 'proc_agent_payment', $agent_payment_param );

								$agent_commission_inserted_id = $response_object->lastinsert_agentcommission_id;					   

					   }	
		
				*/
	   
	   // echo $this->session->userdata( 'user_id' ) ;
	  //p( $this->session->userdata ); 
	 
	/* agent end here	     */
		
	
	}

    public function ccavResponseHandler(){
		
      /*
	   * agent order payment response
	   * 	success handler for wallet payment (1) 
	   *    success handler for ccavenue payment (2)
	   *    success handler for wallet + ccavenue payment (3)	    
	   */
	   
	   $agent_order_last_insert_id = '' ;

	   	if( empty( $this->session->userdata('final_selected_package_data') ) ) {
            
			redirect( base_url('/agentpackagesstep') );
        }
	   
	   	if( empty( $this->session->userdata('agent_order_last_insert_id') ) ){
			
			echo 'you order request failed ... please try again.' ;
		
		} 
			
		
	   
		$siteurl= base_url();
        $parameterbranch=array(
            'act_mode' =>'selectbranch',
            'weburl' =>$siteurl,
            'type'=>'web',

        );

        $path=api_url().'selectsiteurl/branch/format/json/';
        $data['branch']=curlpost($parameterbranch,$path);


        $parameterccgatway=array(
            'act_mode' =>'selectccavenue',
            'branchid' =>$data['branch']->branch_id,
            'type'=>'web',

        );

        $path=api_url().'ccavenue/ccavRequestHandler/format/json/';
		
		#  get working key 
        $data['ccavRequestHandler']=curlpost($parameterccgatway,$path);   
		
		
	   
	   
	   /*  $data['ccavRequestHandler']
	   
			stdClass Object
			(
				[pg_id] => 2
				[pg_branchid] => 1
				[pg_merchant_id] => 49646
				[pg_access_key] => AVUS71EG70AF13SUFA
				[pg_working_key] => B01CEF5819806E9315FF0CADC5D94051
				[pg_created] => 2017-07-03 04:46:02
				[pg_updated] => 2018-01-04 15:56:35
				[pg_status] => 1
				[pg_sucess_link] => http://192.168.1.65/snowworldnew/ordersucess
				[pg_fail_link] => http://192.168.1.65/snowworldnew/orderfail
				[pg_currency] => INR
				[pg_language] => EN
				[pg_prefix] => Snow
				[pg_agent_prefix] => Snowagentorder
				[pg_order_sucess_url] => http://192.168.1.65/snowworldnew/agentsucess
				[pg_order_fail_url] => http://192.168.1.65/snowworldnew/fail
				[pg_wallet_sucess_url] => http://192.168.1.65/snowworldnew/agentwalletsucess
				[pg_wallet_fail_url] => http://192.168.1.65/snowworldnew/agentwalletfail
				[pg_prefix_agent_wallet] => Snowagentwallet
			)
	*/
	   
        $workingKey = $data['ccavRequestHandler']->pg_working_key;
	   
        $encResponse = $_POST["encResp"];
	   
        /*
		* pend($encResponse);//This is the response sent by the CCAvenue Server
		* decrupt response and get data, flag etc.
		*/
	   
        $rcvdString = decrypt($encResponse, $workingKey);        //Crypto Decryption used as per the specified working key.
	   
        $order_status = "";
	   
        $decryptValues = explode('&', $rcvdString);
		
		$decryptValues = array(
			0 => 'order_id=Snow_11',
			1 => 'tracking_id=107321264680',
			2 => 'bank_ref_no=null',
			3 => 'order_status=Aborted',
			4 => 'failure_message=',
			5 => 'payment_mode=null',
			6 => 'card_name=null',
			7 => 'status_code=',
			8 => 'status_message=I have second thoughts about making this payment',
			9 => 'currency=INR',
			10 => 'amount=610.0',
			11 => 'billing_name=bivke',
			12 => 'billing_address=sdfsdf',
			13 => 'billing_city=delhi',
			14 => 'billing_state=delhi',
			15 => 'billing_zip=110014',
			16 => 'billing_country=India',
			17 => 'billing_tel=9810668829',
			18 => 'billing_email=bivek11@gmail.com',
			19 => 'delivery_name=',
			20 => 'delivery_address=',
			21 => 'delivery_city=',
			22 => 'delivery_state=',
			23 => 'delivery_zip=',
			24 => 'delivery_country=',
			25 => 'delivery_tel=',
			26 => 'merchant_param1=',
			27 => 'merchant_param2=',
			28 => 'merchant_param3=',
			29 => 'merchant_param4=',
			30 => 'merchant_param5=',
			31 => 'vault=N',
			32 => 'offer_type=null',
			33 => 'offer_code=null',
			34 => 'discount_value=0.0',
			35 => 'mer_amount=610.0',
			36 => 'eci_value=',
			37 => 'retry=null',
			38 => 'response_code=',
			39 => 'billing_notes=',
			40 => 'trans_date=null',
			41 => 'bin_country='
		);		
	   
        $dataSize = sizeof($decryptValues);
	   
        for ($i = 0; $i < $dataSize; $i++) {
			
            $information = explode('=', $decryptValues[$i]);
			
            if ($i == 3) $order_status = $information[1];
        }
        
		
		//p( $decryptValues ); exit;
		
			$arr1 = explode("=", $decryptValues[1]);
			$arr2 = explode("=", $decryptValues[3]);
			$arr3 = explode("=", $decryptValues[8]);
			$arr4 = explode("=", $decryptValues[5]);
			$arr5 = explode("=", $decryptValues[0]);
			$arr6 = explode("=", $decryptValues[35]);       
			$arr7 = explode("=", $decryptValues[18]);
		//echo '<pre>';
		//var_dump ( $arr1, $arr2, $arr3, $arr4, $arr5, $arr6, $arr7 );
		
		/*
		
array(2) {
  [0]=>
  string(11) "tracking_id"
  [1]=>
  string(12) "107321264680"
}
array(2) {
  [0]=>
  string(12) "order_status"
  [1]=>
  string(7) "Aborted"
}
array(2) {
  [0]=>
  string(14) "status_message"
  [1]=>
  string(48) "I have second thoughts about making this payment"
}
array(2) {
  [0]=>
  string(12) "payment_mode"
  [1]=>
  string(4) "null"
}
array(2) {
  [0]=>
  string(8) "order_id"
  [1]=>
  string(7) "Snow_11"
}
array(2) {
  [0]=>
  string(10) "mer_amount"
  [1]=>
  string(5) "610.0"
}
array(2) {
  [0]=>
  string(13) "billing_email"
  [1]=>
  string(17) "bivek11@gmail.com"
}

		*/
		
		
		
		
  		$siteurl = base_url();
        $parameterbranch = array(
            'act_mode' => 'selectbranch',
            'weburl' => $siteurl,
            'type' => 'web',

        );

        $path = api_url() . 'selectsiteurl/branch/format/json/';
        $data['branch'] = curlpost($parameterbranch, $path);
         $parameterbanner = array(
            'act_mode' => 'selectbannerimages',
            'branchid' => $data['branch']->branch_id,
            'type' => 'web',

        );

        $path = api_url() . 'selectsiteurl/banner/format/json/';
        $data['banner'] = curlpost($parameterbanner, $path);
        
		$parameterccgatway=array(
			'act_mode' =>'selectccavenue',
			'branchid' =>$data['branch']->branch_id,
			'type'=>'web',

		);

		$path=api_url().'ccavenue/ccavRequestHandler/format/json/';
		$data['ccavRequestHandler']=curlpost($parameterccgatway,$path);
		
		

      
     		//p($decryptValues);
            //agentamount_add_new_credit
            //agentamount_add_new_debit
		
		
		

       /* agnet Mail start here and order update and  */
		
       $data['orderlastinsertid'] = $this->session->userdata['agent_order_last_insert_id'];
		
       $orderdisplay = array('act_mode' => 'select_order',
           'orderid' => $data['orderlastinsertid'],
            'type' => 'web', );
       $path = api_url() . "Ordersucess/selectorder/format/json/";
		
       $data['orderdisplaydataval'] = curlpost($orderdisplay, $path);

//p(  $data['orderdisplaydataval'] ) ;  
/*		
stdClass Object
(
    [pacorderid] => 29
    [userid] => 28
    [billing_by] => 7
    [promocodeprice] => 0
    [total] => 1660
    [waletAmount] => 1660
    [subtotal] => 1660
    [addedon] => 2018-01-11 11:45:25
    [countryid] => 1
    [stateid] => 1
    [cityid] => 1
    [branch_id] => 1
    [locationid] => 1
    [departuredate] => 12-01-2018
    [paymentstatus] => 1
    [paymentmode] => 0
    [ticketid] => 5a5700fdaef69
    [tracking_id] => NULL
    [packproductname] => Regular#Combo
    [packimg] => 1c73516272f31851c4b2e9c2a09f7957.jpg#af264b35318be1024a8eec44cad64b0d.jpg
    [package_qty] => 1#1
    [package_price] => 500#1000
    [packpkg] => 2
    [packprice] => 1500
    [orderstatus] => 1
    [op_ticket_print_status] => 0
    [op_printed_date] => 0000-00-00 00:00:00
    [internethandlingcharges] => 50
    [op_usertype] => Partner
    [order_status] => NULL
    [status_message] => NULL
    [paymenttype] => NULL
    [title] => Mr
    [billing_name] => Raju Gupta
    [billing_email] => raju@gmail.com
    [billing_tel] => 9810668829
    [billing_address] => MANTRIPUKHEI,NEAR RPF CAMP,IMPHAL
    [billing_city] => IMPHAL
    [billing_state] => MANIPUR
    [billing_zip] => 897878
    [billing_country] => India
    [billing_pdf] => 1515653763download.pdf
    [ordersucesmail] => 0
    [ordermailstatus] => 0
    [departuretime] => 07
    [frommin] => 30
    [tohrs] => 08
    [tomin] => 30
    [txtfromd] => PM
    [txttod] => PM
    [p_codename] => 0
    [op_ordertrackingupdate] => 0
    [order_status_date_time] => 2018-01-11 11:45:25
    [op_orderjason] => 0
    [op_billing_cometoknow] => DD
    [billing_promocode] => 
    [order_statuspayment] => 
)
	*/
		
      // $arr = (array)$data['orderdisplaydataval'];

       $date_array1 = explode("-", $data['orderdisplaydataval']->addedon); // split the array
       $var_day1 = $date_array1[2]; //day seqment
       $var_month1 = $date_array1[1]; //month segment
       $var_year1 = $date_array1[0]; //year segment
       $new_date_format1 = strtotime("$var_year1-$var_month1-$var_day1"); // join them together

       $d1 = date(' jS F Y', $new_date_format1);
       
       $d2 = date('h:i:s a ');

       $date_array = explode("/", $data['orderdisplaydataval']->departuredate); // split the array
       $var_day = $date_array[0]; //day seqment
       $var_month = $date_array[1]; //month segment
       $var_year = rtrim($date_array[2]," "); //year segment
       $new_date_format = strtotime("2017-$var_month-$var_day"); // join them together

       $input = ("$var_year$var_month$var_day");

       $d3 = date("D", strtotime($input)) . "\n";


       //$d4= date(' jS F Y', $new_date_format);

       $row['date'] = trim($var_year, " ") . "-" . $var_month . "-" . $var_day; // this is for example only - comment out when tested
		
       $d4 = date("F j, Y", strtotime($row['date']));

//echo date("H:i:s") . "\n";)

       $to_email = $data['orderdisplaydataval']->billing_email;
       $rrrval = $data['orderdisplaydataval']->pacorderid;
       $rproduct = $data['orderdisplaydataval']->packproductname;
       $parametertearms = array(
           'act_mode' => 'selecttearms',
           'branchid' => $data['branch']->branch_id,
           'type' => 'web',

       );

       $path = api_url() . 'selectsiteurl/bannern/format/json/';
       $data['tearmsgatway'] = curlpost($parametertearms, $path);
       $mess = '<table width="90%" style="line-height: 28px; font-family: sans-serif;" >
<tr><td>Dear  ' . $data['orderdisplaydataval']->billing_name . ',</td></tr>

<tr><td>Greetings from ' . $data['banner']->bannerimage_top3 . '.!</td></tr>

<tr><td>
Thank you for choosing our Services. The  details are as follows:
Transaction ID:' . $data['orderdisplaydataval']->ticketid . '
</td></tr>
<tr><td>All guests are requested to report 30 minutes prior to the Session Time and collect your Entry Pass from the Ticket Counter.</td></tr>

<tr><td>We request the guest to carry a print out of this E &minus; voucher if possible. Should you have any queries, please feel free to write to us on ' . $data['banner']->bannerimage_branch_email . ' or call us on ' . $data['banner']->bannerimage_branch_contact . '.</td></tr>


<tr><td>We look forward to welcoming you the next time you visit us at ' . $data['banner']->bannerimage_top3 . '.</td></tr>




<tr><td>Yours sincerely,<br>
' . $data['banner']->bannerimage_top3 . ' Team</td></tr>
</table>';

       $date_array1 = explode("-", $data['orderdisplaydataval']->departuredate); // split the array
       $var_day1 = $date_array1[2]; //day seqment
       $var_month1 = $date_array1[1]; //month segment
       $var_year1 = rtrim($date_array1[0]," "); //year segment
       $new_date_format1 = strtotime("$var_year1-$var_month1-$var_day1"); // join them together


       $d1 = date(' jS M Y', strtotime($data['orderdisplaydataval']->departuredate) );
       $data[ 'd1' ] = $d1;

       $date = '19:24:15 ';
       $d2 = date('h:i:s a ');

       $date_array = explode("/", $data['orderdisplaydataval']->departuredate); // split the array
       $var_day = $date_array[0]; //day seqment
       $var_month = $date_array[1]; //month segment
       $var_year = rtrim($date_array[2]," "); //year segment
       $new_date_format = strtotime( date('Y-m-d', strtotime($data['orderdisplaydataval']->addedon) ) ); // join them together

       $input = ("$var_year$var_month$var_day");

       $d3 = date( "D", strtotime( date('Y-m-d', strtotime($data['orderdisplaydataval']->departuredate) ) ) ) . "\n";
       $data['d3'] = $d3 ;


       $prepare_time_slot_from_date = $data['orderdisplaydataval']->departuretime .':'.$data['orderdisplaydataval']->frommin .' '.$data['orderdisplaydataval']->txtfromd .' - '.
           $data['orderdisplaydataval']->tohrs.':'.$data['orderdisplaydataval']->tomin.' '.$data['orderdisplaydataval']->txttod;
       $data['prepare_time_slot_from_date'] =$prepare_time_slot_from_date;

       $d4 = date(' jS M Y', $new_date_format);
       $data['d4'] = $d4;


       $message_pdf = '<html>
<head>
    <title> Booking</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
    <!-- Save for Web Slices (Untitled-1) -->
    <table width="953" height="967" cellpadding="0" cellspacing="0"  style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;">
    <tr>
    <td colspan="3">
    <table width="953">
    <tr>
    <td border="0" width="250" valign="top" style="padding-top:20px;padding-left:20px;"><img src="' . base_url("assets/admin/images/") . "/" . $data['banner']->bannerimage_logo . '" alt="" width="220" ></td>
    <td border="0" width="700" style="text-align:center;">
    <table width="800" style="margin:20px 20px;line-height:25px;">
    <tr>
    <td style="color: #37ACE1;font-weight:700;padding:0px 0px;font-size:29px;text-align:center;">
    <strong>' . $data['banner']->bannerimage_top1 . '</strong>
                
                </td>
                </tr>
                <tr><td style="padding:8px 0px 5px;font-size:18px;font-weight:700;"><strong>' . $data['banner']->bannerimage_top2 . '</strong></td></tr>
                
                <tr><td style="color: #444;font-weight:700;padding:0px 0px 0px;font-size:22px;"><strong>' . $data['banner']->bannerimage_top4 . '</strong></td></tr>
               <tr><td style="color: #444;font-weight:700;padding:0px 0px 0px;font-size:22px;"><strong>' . $data['banner']->bannerimage_gstno . '</strong></td></tr>
                </table>
                </td>
                
                </tr>
                </table>
                </td>
                
    </tr>
    <tr><td colspan="3" style="color: #37ACE1;font-weight:600;padding:8px 0px 8px;font-size:25px;text-align:center"><strong>Booking Confirmation Details</strong></td></tr>
    <tr><td colspan="3" style="border-top:dashed 3px #37ace1;"><td></tr>
      <tr>
      <td colspan="3">
      <table width="1000" cellpadding="0" cellspacing="0" style="margin:15px 0;">
      <tr>
      <td width="475">
      <table style="margin:35px 35px 35px 35px;">
      <tr>
      <td style="line-height:25px;">
                    <h3 style="font-size:23px;">Venue Details</h3>
                    
                    </td>
                    </tr>
                    <tr><td style="font-weight:normal;line-height:23px;font-size:18px;"> ' . $data["branch"]->branch_add . '</td></tr>
                    <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Email :</span> ' . $data['banner']->bannerimage_branch_email . '</td></tr>
                    <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Phone :</span> ' . $data['banner']->bannerimage_branch_contact . '</td></tr>
                    
                </table>
                </td>
      <td style="border-left:dashed 3px #37ace1;"></td>
      <td width="475">
      <table style="margin:35px 35px 35px 35px;">
      <tr>
      <td style="line-height:25px;">
                    <h3 style="font-size:23px;">Guest Details</h3>
                    
                    </td>
                    </tr>   
                    <tr><td style="font-weight:normal;line-height:23px;font-size:18px;">' . $data['orderdisplaydataval']->billing_name . ' <br>'.$data['orderdisplaydataval']->billing_address.' </td></tr>
                    <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Email :</span> ' . $data['orderdisplaydataval']->billing_email . '</td></tr>
                    <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Phone :</span> ' . $data['orderdisplaydataval']->billing_tel . '</td></tr>
                </table>
                </td>
      </tr>
      <tr>
      <td colspan="3">
       <table width="1000" cellpadding="0" cellspacing="0" style="margin:15px 0px;">
       <tr>
       <td colspan="3" style="border-top:dashed 3px #37ace1;"></td>
       </tr>
       <tr>
       <td><table width="475" style="margin:15px 0;border-right:dashed 3px #37ace1;">
                
                <tr><td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Booking No.</td>
                <td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Booking Date.</td></tr>
                
                <tr><td style="text-align:center;margin-top:5px;font-size:13px;">' . $data['orderdisplaydataval']->ticketid . '</td>
                <td style="text-align:center;margin-top:5px;font-size:13px;">' . $d4 . '</td></tr></table></td>
                <td width="3"></td>
                <td><table width="475" style="margin:15px 0;">
                
                <tr><td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Visit Date.</td>
                <td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Session Time</td></tr>
                <tr><td style="text-align:center;margin-top:5px;font-size:13px;">' . $d1 . '</td>
                <td style="text-align:center;margin-top:5px;font-size:13px;">' . $data['prepare_time_slot_from_date'] . '</td></tr></table></td>
       </tr>
       <tr>
       <td colspan="3" style="border-top:dashed 3px #37ace1;"></td>
       </tr>
       </table>
      </td></tr>
      </table>
      </td>
      </tr>
      <tr>
      <td colspan="3">
        <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 10px 50px; ">
        <tr>
        <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">
        Items
</td>
        <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">Qty
</td>
        <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">Cost</td>
        <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">Total</td>
        </tr>
       
        ';


       $message_pdf1 = $package_total_price = $addon_total_price_with_quantity = "";
       $total_array = $addon_data = [];
       $total = 0;

       $package_name_array = explode('#', $data['orderdisplaydataval']->packproductname );
       $package_img_array = explode('#', $data['orderdisplaydataval']->packimg );
       $package_qty_array = explode('#', $data['orderdisplaydataval']->package_qty );
       $package_price_array = explode('#', $data['orderdisplaydataval']->package_price );

       $package_total_price = $total_array[] = getSumAllArrayElement( calculatePriceByQty( explode('#', $data['orderdisplaydataval']->package_price ), explode('#', $data['orderdisplaydataval']->package_qty ) ) );

       $addon_data =  getAddonTotalPricePrint( $data['orderaddonedisplaydata'] ) ;
       //p( $addon_data['addon_price_array'] );
//p($addon_data);
       $data['addon_price_with_quantity'] = 0;
       $addon_total_price_with_quantity = $total_array[] =  getSumAllArrayElement( calculatePriceByQty( $addon_data['addon_price_array'], $addon_data['addon_qty_array'] ) );
//p( $addon_total_price_with_quantity );
       $data['addon_price_with_quantity'] = $addon_total_price_with_quantity;
//p( $data['orderdisplaydataval']->internethandlingcharges );
       $total_array[] = $data['orderdisplaydataval']->internethandlingcharges;
       //echo '<pre>';
       //print_r($data['orderdisplaydataval']);
       $total = ($data['orderdisplaydataval']->promocodeprice) ?   ( getSumAllArrayElement( $total_array ) - $data['orderdisplaydataval']->promocodeprice )  :  getSumAllArrayElement( $total_array );
//p($total_array);
       foreach( $package_name_array as $ky => $val){

           $message_pdf1 .= '<tr><td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px">
       ' . $val . ' Package
</td>
        <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px">' . ( $package_qty_array[ $ky ] ) . '
</td>
        <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"> ₹ ' . ( $package_price_array[ $ky ] ) . '</td>
        <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"> ₹ ' . ( $package_qty_array[ $ky ] * $package_price_array[ $ky ] ) . '</td>
        </tr>';
       }

///p($data['orderaddonedisplaydata']);
       foreach ($data['orderaddonedisplaydata'] as $a => $b) {
           if ($b != 'Something Went Wrong') {
               $message_pdf1 .= '<tr><td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px">
        ' . $b["addonename"] . '
</td><td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px">' . $b["addonqty"] . '
</td>
        <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"> ₹ ' . $b["addoneprice"] . '</td>
        <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"> ₹ ' . ($b["addoneprice"] * $b["addonqty"]) . '</td>
        </tr>';
           }
       }

       $message_pdf2 = '<tr>
        <td colspan="4">
        <table width="760" cellpadding="0" cellspacing="0" style="border-top:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:10px 50px 10px 50px; ">
        <tr>
        <td width="380" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Price:</td>
        <td width="380" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"> ₹ ' . ($package_total_price + $addon_total_price_with_quantity) . '</td>
        </tr>
        </table>
        </td>
        </tr>
        
        </table>
      </td>
      </tr>
      
      <tr>
      <td colspan="3">
      <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 10px 50px; ">
     <tr><td width="430" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Discount Amount</td><td width="430" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"> ₹ ' . $data['orderdisplaydataval']->promocodeprice . '</td></tr>
      </table>
      </td>
      </tr>
      
      <tr>
      <td colspan="3">
      <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 10px 50px; ">
     <tr><td width="430" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Internet Handling Charges</td><td width="430" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"> ₹ ' . $data['orderdisplaydataval']->internethandlingcharges . '</td></tr>
      </table>
      </td>
      </tr>
      <tr>
      <td colspan="3">
      <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 30px 50px; ">
     <tr><td width="430" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Total</td><td width="430" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"> ₹ ' . $total . '</td></tr>
      </table>
      </td>
      </tr>   
      <tr>
     <td colspan="3">
      <table width="475" style="margin:15px 0px 15px 50px;">
      <tr><td style="font-size:17px;font-weight:bold;color:#000;padding:4px 0px;"></td></tr>
      <tr><td style="font-size:17px;font-weight:bold;color:#000;padding:4px 0px;">PAN No. : AAFFC9989B</td></tr>
      <tr><td style="font-size:17px;font-weight:bold;color:#000;padding:4px 0px;">Encl. Terms and Conditions</td></tr>
      </table>
     </td>
      </tr>
    </table>
    <table width="953" cellpadding="0" cellspacing="0"  style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;">
    <tr>
   <td>
    <table>
     <tr>
     <td width="250" valign="top" style="padding:15px 0px 0px 15px;"><img src="' . base_url("assets/admin/images/") . "/" . $data['banner']->bannerimage_logo . '" alt="" width="220"></td>
     <td style="text-align:center;">
    <table width="453" style="margin:20px 20px;line-height:25px;">
    <tr>
    <td width="453" style="color: #37ACE1;font-weight:700;padding:0px 0px;font-size:29px;text-align:center;">
    <strong>' . $data['banner']->bannerimage_top1 . '</strong>
                
                </td>
                </tr>
                <tr><td style="padding:8px 0px 5px;font-size:18px;font-weight:700;"><strong>' . $data['banner']->bannerimage_top2 . '</strong></td></tr>
               
               
                <tr><td style="color: #37ACE1;font-weight:600;padding:15px 0px 0px;font-size:25px;"><strong>Terms and Conditions</strong></td></tr>
                </table>
                </td>
                 <td width="250" valign="top" style="padding:15px 15px 0px 0px;"><img src="' . base_url("assets/admin/images/") . "/" . $data['banner']->bannerimage_logo . '" alt="" width="220"></td>
                </tr>
                <tr>
                <td colspan="3">
                <table width="940" style="margin-left:50px;margin-top:10px;margin-bottom:80px;color:#000;font-size:16px;line-height:25px;">';


       $i=1;foreach ($data['tearmsgatway'] as $key => $value11) {
           $message_pdf2 .= '
   <tr>
                                    <td width="30" valign="top">'.$i.'.</td>
                                    <td width="900" valign="top">'.$value11['term_name'].'</td>
                                </tr>';
           $i++;}
       $message_pdf2 .= '
   
    
    <tr>
    <td width="30" valign="top">&nbsp;</td>
    <td width="900" valign="top">
    <table style="font-size:16px;line-height:25px;">
    
    </table>
    </td>
    </tr>
    
    
    </table>
                </td>
                </tr>
                </table>
     </td>
    
     </tr>
     </table>

   </td>
    </tr>
    
    
   
    </table>
    <!-- End Save for Web Slices -->
</body>
</html>';


       $pdf_name = time() . "download.pdf";
       base_url("assets/admin/pdfstore/" . $pdf_name);
       $this->load->helper('file');
        $pdfFilePath = FCPATH . "assets/admin/pdfstore/" . $pdf_name;

       $this->load->library('m_pdf');

       $this->m_pdf->pdf->WriteHTML($message_pdf . $message_pdf1 . $message_pdf2);
       $a = $this->m_pdf->pdf->Output($pdfFilePath, 'F');


       $from_email = $data['banner']->bannerimage_from;

    //  p($pdfFilePath."kkkkkkkkkkk");

       //Load email library
       $this->load->library('email');
       $this->email->from($from_email, $data['banner']->bannerimage_top3);
       $this->email->reply_to($from_email, $data['banner']->bannerimage_top3);
       // $this->email->reply_to($from_email, $data['banner']->bannerimage_top3);
       $this->email->to($to_email);
       $this->email->cc($from_email);
       // $this->email->to($from_email);
       $this->email->$data['banner']->bannerimage_apikey;
       $this->email->subject('' . $data['banner']->bannerimage_top3 . ' - Booking Confirmation Voucher ');

       $this->email->message($mess);

       $this->email->attach(FCPATH . "assets/admin/pdfstore/" . $pdf_name);
       $this->email->send();
     //  show_error($this->email->print_debugger());
       //var_dump($this->email->send()); exit;
       //Send mail
       $orderpdfupdate = array('act_mode' => 'orderpdfupdatesucess',
           'orderid' => $data['orderlastinsertid'],
           'tracking_id' => $pdf_name,
           'order_status' => '',
           'status_message' => '',
           'paymentmode' => '',
           'paymentstatus' => '',
           'ordersucesmail' => '',
           'ordermailstatus' => '',
           'type' => 'web',
       );

       $path = api_url() . "Ordersucess/selectorderdata/format/json/";
		
       $data['orderpdf'] = curlpost($orderpdfupdate, $path);



    //   pend($this->session->userdata('agent_order_last_insert_id'));
		



/* agnet payemnt start here */
	   		$total_promo_price = '';

     
			$final_prepare_data_array = [] ;   // tmp

				   $final_selected_package_data_array = [] ;
		
						$agentamount_add_new_debit_array = [] ;
		
				   $paymentmode = '' ;
		
					$session_to_pay_wallet = $total_promo_price = $user_id = '' ;
		
				   

				   $session_to_pay_wallet = $session_agent_wallet_amount = $session_to_pay_ccavenue = $session_before_payment_to_pay_ccavenue = '' ;

				   $payment_flag = $agent_order_last_insert_id = '' ;  //order id - order package	  	   

					// here
				   //$orderdata = explode( $data['ccavRequestHandler']->pg_agent_prefix.'_', $arr5[1] );

				   $payment_flag_int = '' ; 

				   $agent_id_wallet = $user_id = '' ;
		
				
			$final_selected_package_data_array = getPackageTotalPrice( $this->session->userdata( 'final_selected_package_data' ) );

				$total_promo_price	= $this->session->userdata( 'total_promo_price' ) ;

				$user_id = $this->session->userdata( 'user_id' );

				//p( $this->session->userdata );
				$session_to_pay_wallet = $this->session->userdata('before_payment_after_wallet_deduction_to_pay_wallet');

				$session_agent_wallet_amount = $this->session->userdata('before_payment_after_wallet_deduction_agent_wallet_amount') ;

				$session_to_pay_ccavenue = $this->session->userdata('before_payment_after_wallet_deduction_to_pay_ccavenue') ;
		
				$session_before_payment_to_pay_ccavenue = $this->session->userdata('before_payment_to_pay_ccavenue') ;
		
				$agent_order_last_insert_id = $this->session->userdata('agent_order_last_insert_id');

				$payment_flag_int = array_sum( $this->session->userdata('payment_flag') );			    

				$agentamount_add_new_debit_array = [
										'agent_id' => $user_id ,
										'to_pay_wallet' => $session_to_pay_wallet ,
										'credit_debit_flag' => $this->credit_debit_flag , 
										 ] ; 		
				
	   			// empty array required as per procedure
				 $prepare_proc_param_package_addon_promocode = [

					    'param' => '' ,
						'param1' => '' ,
						'param2' => '' ,
						'param3' => '' ,
						'param4' => '' ,
						'param5' => '' ,
						'param6' => '' , 
						'param7' => '' , 
						'param8' => '' ,
						'param9' => '' ,
						'param10' => '' ,
						'param11' => '' ,    
						'param12' => '' ,
						'param13' => '' ,
						'param14' => '' ,
						'param15' => ''  
				 		];

	   			// empty array required as per procedure	   
				$prepare_proc_param_billingid_orderid_userid_urls = [

						'param16' => '' ,
						'param17' => '' ,
						'param18' => '' ,
						'param19' => '' ,
						'param20' => '' ,
						'param21' => '' ,
						'param22' => '' ,
						'param23' => '' ,
						'param24' => '' ,
						'param25' => '' ,
						'param26' => '' ,
						'param27' => '' ,
						'param28' => '' ,
						'param29' => '' ,
						'param30' => '' ,
						'param31' => '' ,
						'param32' => '' ,
						'param33' => '' ,
						'param34' => '' ,
						'param35' => '' ,
						'param36' => '' 
						];			
	   
	   			// empty array required as per procedure	   
					$prepare_proc_param_future_param = [ 
						'param37' => '',    // wallet amount
						'param38' => '',      // subtotal
						'param39' => '',
						'param40' => '',
						
						'param41' => '',
						'param42' => '' ,
						'param43' => '' ,
						'param44' => '' ,
						'param45' => '' ,
						'param46' => '' ,
						'param47' => '' ,
						'param48' => '' ,
						'param49' => '' ,
						'param50' => '' ,
						
						'param51' => '' ,
						// used later 
						'param52' => '' ,
						'param53' => '' ,
						'param54' => '' ,
						'param55' => '' ,
						'param56' => '' ,
						'param57' => '' ,
						'param58' => '' ,
						'param59' => '' ,
						'param60' => '' ,
						
						'param61' => '' ,
						'param62' => '' ,
						'param63' => 'wallet',
						'param64' => empty( $this->session->userdata( 'total_promo_price' ) ) ? $this->session->userdata( 'before_payment_total_purchase_amount' ) : $this->session->userdata( 'total_promo_price' ) ,
						'param65' => $this->session->userdata( 'user_id' ) 
						];
	   
	   
					$response_object = $agent_commission_inserted_id = '';	
	   
					$agent_payment_param  = array_merge( $prepare_proc_param_package_addon_promocode, $prepare_proc_param_billingid_orderid_userid_urls, $prepare_proc_param_future_param ) ;
	   
//					   if( $user_id ) {

						  /*
						   * insert agent commission details 
						   */
								$agent_payment_param[ 'param' ] = 'insert_agentcommission_details' ;
						   		$agent_payment_param[ 'param32' ] = ( $agent_order_last_insert_id ) ? $agent_order_last_insert_id : 0 ;
								$agent_payment_param[ 'param8' ] = empty( $this->session->userdata( 'total_promo_price' ) ) ? $this->session->userdata( 'before_payment_total_purchase_amount' ) : $this->session->userdata( 'total_promo_price' ) ;						   

								$response_object = $this->supper_admin->call_procedureRow( 'proc_agent_payment', $agent_payment_param );

								$agent_commission_inserted_id = $response_object->lastinsert_agentcommission_id;					   
		
		
						   
//					   	}	   
	   
		/* agent end here	     */
	   
				   /*
					*  only set order payment status ( 0 / 1)
					*/	   	
		$set_order_payment_status = $prepare_log_msg = '' ;
		
			$set_order_payment_status = function( $response ) {

				 if( isset( $response->last_id ) && ( $response->last_id ) ) {

					 $this->current_order_payment_flag = 1 ;   // ***

					 $this->agent_wallet_last_insert_id = $response->last_id ;

				 }else{

					 $prepare_log_msg = ['log_created_date' => date('d-m-Y-h:i:s') ]; 

					 $prepare_log_msg = array_merge( $prepare_log_msg,  $this->session->userdata ); 

					 log_message( 'info', json_encode( $prepare_log_msg ) ) ;

				 }				

			};
		
			$get_ccavenue_response_data = '' ;
		
			
		
		   $get_ccavenue_response_data = function( $payment_flag_int ) use ( $arr1, $arr2, $arr3, $arr4, $arr5, $arr6, $arr7, $user_id ) {
			   
					$tmp_param = array(
								'tracking_id' => '' ,
								'order_status' => '' ,
								'status_message' => '' ,
								'paymentmode' => '' ,
								'agent_amount' => '' ,
								'agent_id' => '' ,
								'agent_wallet_last_insert_id' => '' ,
								'session_to_pay_wallet' => '' ,
								'credit_debit_flag_wallet' => '' ,
								'to_pay_amount' => '' ,
								'order_status_wallet' => '' , 
								'status_message_wallet' => '' , 
								'agent_id_wallet' => '' ,
								'tracking_id_wallet' => '' ,
								'paymentmode_wallet_ccavenue' => '' 
							);  
			   
					switch ( $payment_flag_int ) {

						#  wallet 				
						case 0 : 

						break;

						#  ccavenue 
						case 2 :  
							
							//var_dump( $arr1, $arr2, $arr3, $arr4, $arr5, $arr6, $arr7, $user_id );
							
							isset( $arr1[1] ) ? $tmp_param['tracking_id'] = $arr1[1] : '' ;    // two in one  // get from ccavenue 
							
							isset( $arr2[1] ) ? $tmp_param['order_status'] = $arr2[1] : '' ;   // two in one  # current order  0 = failed,1 = success, 2 = pending
							
							isset( $arr3[1] ) ? $tmp_param['status_message'] = $arr3[1] : '' ; // two in one  // current order message
							
							isset( $arr4[1] ) ? $tmp_param['paymentmode'] = $arr4[1] : '' ; // two in one  // current order payment mode
							
							isset( $arr6[1] ) ? $tmp_param['agent_amount'] = $arr6[1] : '' ;	//  // current order amount	

						break;				

						#  wallet + ccavenue	
						case 3 : 

							isset( $arr1[1] ) ? $tmp_param['tracking_id'] = $arr1[1] : '' ;    // two in one  // get from ccavenue 
							
							isset( $arr2[1] ) ? $tmp_param['order_status'] = $arr2[1] : '' ;   // two in one  # current order  0 = failed,1 = success, 2 = pending
							
							isset( $arr3[1] ) ? $tmp_param['status_message'] = $arr3[1] : '' ; // two in one  // current order message
							
							isset( $arr4[1] ) ? $tmp_param['paymentmode'] = $arr4[1] : '' ; // two in one  // current order payment mode
							
							isset( $arr6[1] ) ? $tmp_param['agent_amount'] = $arr6[1] : '' ;	//  // current order amount			

						break;				

					}
			   
			   return $tmp_param ;
		   
		   } ;
					
		
		
		 //$payment_flag_int = 3 ;
	   
		   if ( ( $payment_flag_int == 0 ) ) {
			   
			   $get_ccavenue_response_data_array = [] ; 
			   
			   $parameter = array('act_mode' => 'agentamount_add_new_debit',
							'Param1' => $agentamount_add_new_debit_array['agent_id']  ,
							'Param2' => $agentamount_add_new_debit_array['to_pay_wallet']  ,
							'Param3' => $agentamount_add_new_debit_array['credit_debit_flag']  ,
							'Param4' => '0',
							'Param5' => 'Ag',
							'Param6' => '',
							'Param7' => '',
							'Param8' => '',
							'Param9' => '');
// pend($parameter);
			   
				$response = $this->supper_admin->call_procedurerow( 'proc_agent_s', $parameter ); 			   
//			   p( $response );
			   	$set_order_payment_status( $response ); 
			   
			   // prepare param to save history details 
			   
					$get_ccavenue_response_data_array[ 'agent_id' ] = $user_id;

					// required for history table entry
					$get_ccavenue_response_data_array[ 'agent_wallet_last_insert_id' ] = $this->agent_wallet_last_insert_id ;  // current order table last insert id

					$get_ccavenue_response_data_array[ 'to_pay_wallet' ] = $agentamount_add_new_debit_array['to_pay_wallet'] ;   // to pay by wallet amount
					
					$get_ccavenue_response_data_array[ 'credit_debit_flag_wallet' ] = 'D';   // to pay by wallet amount	
					
					$get_ccavenue_response_data_array[ 'to_pay_ccavenue' ] = $session_to_pay_wallet ;
			   
					$get_ccavenue_response_data_array[ 'order_status' ] = 1;
					
					$get_ccavenue_response_data_array[ 'paymentmode' ] = 1;  //ccavenue
			   
					$get_ccavenue_response_data_array[ 'status_message' ] = 'payment success';
			   
					$get_ccavenue_response_data_array[ 'agent_amount' ] = $session_to_pay_wallet ;
					
					$get_ccavenue_response_data_array[ 'agent_id_wallet' ] = $user_id;
					
					$get_ccavenue_response_data_array[ 'tracking_id_wallet' ] = '';
			   
			   		$final_prepare_data_array = $get_ccavenue_response_data_array ;
			   
			   //p( $final_prepare_data_array ); 
			   
		   }
	   
		/*
		 * 	ccavenue + wallet  transaction
		 */
	   
		
	   	  if ( $payment_flag_int == 3 ) {
			  
			  if( count( $agentamount_add_new_debit_array ) ) {
			  
			   $parameter = array('act_mode' => 'agentamount_add_new_debit',
							'Param1' => $agentamount_add_new_debit_array['agent_id']  ,
							'Param2' => $agentamount_add_new_debit_array['to_pay_wallet']  ,
							'Param3' => $agentamount_add_new_debit_array['credit_debit_flag']  ,
							'Param4' => '0',
							'Param5' => 'Ag',
							'Param6' => '',
							'Param7' => '',
							'Param8' => '',
							'Param9' => '');
			  
				$response = $this->supper_admin->call_procedurerow('proc_agent_s', $parameter); 
			  
					$set_order_payment_status( $response ); 

						$this->current_order_payment_flag = 0 ;			  // do not remove this line
			  }
			  
			  
			  $get_ccavenue_response_data_array = [] ;
			  
			  $get_ccavenue_response_data_array = $get_ccavenue_response_data( $payment_flag_int ) ;
			  
				if( count( $get_ccavenue_response_data_array ) && ( $get_ccavenue_response_data_array['order_status'] != 'failed' || $get_ccavenue_response_data_array['order_status'] != 'fail' ) ) {   // payment success to check on live data 

					// prepare cutom parameter to save into db				  	
					$get_ccavenue_response_data_array[ 'agent_id' ] = $user_id;

					// required for history table entry
					$get_ccavenue_response_data_array[ 'agent_wallet_last_insert_id' ] = $agent_order_last_insert_id;  // current order table last insert id

					$get_ccavenue_response_data_array[ 'to_pay_wallet' ] = $session_to_pay_wallet;   // to pay by wallet amount

					$get_ccavenue_response_data_array[ 'credit_debit_flag_wallet' ] = 'D';   // to pay by wallet amount	

					$get_ccavenue_response_data_array[ 'to_pay_ccavenue' ] = abs( $session_to_pay_wallet );    // param8
					
					$get_ccavenue_response_data_array[ 'agent_amount' ] = abs( ( $session_to_pay_ccavenue + $session_to_pay_wallet ) ) ;  // param6

					$get_ccavenue_response_data_array[ 'order_status_wallet' ] = 1;
					

					$get_ccavenue_response_data_array[ 'paymentmode' ] = 3;  //ccavenue

					$get_ccavenue_response_data_array[ 'status_message_wallet' ] = 'payment successful';

					$get_ccavenue_response_data_array[ 'agent_id_wallet' ] = $user_id;

					$get_ccavenue_response_data_array[ 'tracking_id_wallet' ] = '';

					$response = ( object ) [ 'last_id' => 1 ];   

					$set_order_payment_status( $response );
					
				}else {
				
					// send failed email
					
					// render failed template
					$response = ( object ) [ 'last_id' => 0 ];   

					$set_order_payment_status( $response );

				}	
			  
					$final_prepare_data_array = $get_ccavenue_response_data_array ; 				

		   }
	   
		/*
		 * 	only ccavenue transaction
		 *
		 */	   	
		
	   	  if ( $payment_flag_int == 2 ) {
			  
			    //$agent_id_wallet = $user_id;
			  $get_ccavenue_response_data_array = [] ;
			  
			  
			  	// get ccavenue data 
			  	$get_ccavenue_response_data_array = $get_ccavenue_response_data( $payment_flag_int ) ;
			  
			  	if( count( $get_ccavenue_response_data_array ) ) {
			  
					// prepare cutom parameter to save into db				  	
					$get_ccavenue_response_data_array[ 'agent_id' ] = $user_id;

					// required for history table entry
					$get_ccavenue_response_data_array[ 'agent_wallet_last_insert_id' ] = $agent_order_last_insert_id;  // current order table last insert id

					$get_ccavenue_response_data_array[ 'to_pay_wallet' ] = $session_to_pay_ccavenue;   // to pay by wallet amount
					
					$get_ccavenue_response_data_array[ 'credit_debit_flag_wallet' ] = 'D';   // to pay by wallet amount	
					
					$get_ccavenue_response_data_array[ 'to_pay_ccavenue' ] = $this->session->userdata('before_payment_to_pay_ccavenue') ;
					
					$get_ccavenue_response_data_array[ 'order_status_wallet' ] = 1;
					
					$get_ccavenue_response_data_array[ 'paymentmode' ] = 2;  //ccavenue
					
					$get_ccavenue_response_data_array[ 'status_message_wallet' ] = 'success';
					
					$get_ccavenue_response_data_array[ 'agent_id_wallet' ] = $user_id;
					
					$get_ccavenue_response_data_array[ 'tracking_id_wallet' ] = '';
					

					$response = ( object ) [ 'last_id' => 1 ];   

					$set_order_payment_status( $response );
					
				}else {
				
					// send failed email
					
					// render failed template
					$response = ( object ) [ 'last_id' => 0 ];   

					$set_order_payment_status( $response );
					
								
				}
			  
			  // assign to comman variable
			  
			  $final_prepare_data_array = $get_ccavenue_response_data_array ;
			  
		   }	 

		/*
		*	Agent history is saved for all successfull orders
		*
		*/
	   
	   //if( $response->last_id ) { 
		
	// p( $this->session->userdata );
		
	// p( $final_prepare_data_array )	;	
		   
		 $parameter = array(
			 		'act_mode' => 'agentamount_add_new_history' ,
					'Param1' => $final_prepare_data_array['agent_id_wallet'] ,
					'Param2' => $final_prepare_data_array['tracking_id'] , 
					'Param3' => $final_prepare_data_array['order_status'] , 
					'Param4' => $final_prepare_data_array['status_message'] , 
					'Param5' => $final_prepare_data_array['paymentmode'] , 
					'Param6' => $final_prepare_data_array['agent_amount'] , 
					'Param7' => $final_prepare_data_array['agent_wallet_last_insert_id'] , 
					'Param8' => $final_prepare_data_array['to_pay_ccavenue'] , 
					'Param9' => ''
		 			);
		
		/*
		INSERT INTO tbl_agent_payment_history( `ph_agent_id`, `ph_tracking_id`, `ph_order_status`, `ph_status_message`,`ph_paymentmode`,`ph_mer_amount`, `ph_wallet_amount`, `ph_wid`) 
		VALUES ( Param1, Param2, Param3, Param4, Param5, Param6, Param8, Param7);
		*/
	   
		 		$response = $this->supper_admin->call_procedurerow( 'proc_agent_s', $parameter );
		
		
	   
	 	p( $parameter ); exit;
	   
	   //}
		
		// echo  $payment_flag_int . '            /            '  .  $this->current_order_payment_flag ;
		
		// die;		
		
		
		
		
	    // unset all user data start here
		
        if( !empty( $this->session->userdata('package_total_qty') ) ){  //echo 1;
            
            $this->session->unset_userdata('package_total_qty');
        }
		
        if( !empty( $this->session->userdata('package_total_price') ) ){  //echo 1;
            
            $this->session->unset_userdata('package_total_price');
        }		
        
        if( !empty( $this->session->userdata('final_selected_package_data') ) ){  //echo 1;
            
            $this->session->unset_userdata('final_selected_package_data');
        }   
		   
		if( !empty( $this->session->userdata('final_selected_addon_data') ) ){   //echo 2;

		   $this->session->unset_userdata('final_selected_addon_data');
		}
		
		if( ! empty( $this->session->userdata( 'addon_total_price' ) ) ) {

			$this->session->unset_userdata( 'addon_total_price' );
		}						

		if( ! empty( $this->session->userdata( 'addon_total_qty' ) ) ) {

			$this->session->unset_userdata( 'addon_total_qty' );
		}		
		   
		   
		// sessioon destroy start here 
		if( !empty( $this->session->userdata('orderlastinsertid') ) ){

			$this->session->unset_userdata('orderlastinsertid');

		}		

		if( ! empty( $this->session->userdata( 'before_payment_agent_wallet_amount' ) ) ) {

			$this->session->unset_userdata( 'before_payment_agent_wallet_amount' );
		}

		if( ! empty( $this->session->userdata( 'before_payment_total_purchase_amount' ) ) ) {

			$this->session->unset_userdata( 'before_payment_total_purchase_amount' );
		}

		if( ! empty( $this->session->userdata( 'before_payment_after_wallet_deduction_to_pay_ccavenue' ) ) ) {

			$this->session->unset_userdata( 'before_payment_after_wallet_deduction_to_pay_ccavenue' );
		}

		if( ! empty( $this->session->userdata( 'before_payment_after_wallet_deduction_to_pay_wallet' ) ) ) {

			$this->session->unset_userdata( 'before_payment_after_wallet_deduction_to_pay_wallet' );
		}

		if( ! empty( $this->session->userdata( 'before_payment_after_wallet_deduction_agent_wallet_amount' ) ) ) {

			$this->session->unset_userdata( 'before_payment_after_wallet_deduction_agent_wallet_amount' );
		}

		if( ! empty( $this->session->userdata( 'payment_flag' ) ) ) {

			$this->session->unset_userdata( 'payment_flag' );
		}			
		// sessioon destroy end here
		
		// promo variable unset start here 
		if( ! empty( $this->session->userdata( 'total_promo_price' ) ) ) {

			$this->session->unset_userdata( 'total_promo_price' );
		}	

		if( ! empty( $this->session->userdata( 'promo_id' ) ) ) {

			$this->session->unset_userdata( 'promo_id' );
		}			

		if( ! empty( $this->session->userdata( 'saving_price_promocode' ) ) ) {

			$this->session->unset_userdata( 'saving_price_promocode' );
		}				

		if( ! empty( $this->session->userdata( 'promocode_name' ) ) ) {

			$this->session->unset_userdata( 'promocode_name' );
		}				

		if( ! empty( $this->session->userdata( 'handling_charge_with_no_of_person' ) ) ) {

			$this->session->unset_userdata( 'handling_charge_with_no_of_person' );
		}						

		if( ! empty( $this->session->userdata( 'final_cost_package_addon_handling' ) ) ) {

			$this->session->unset_userdata( 'final_cost_package_addon_handling' );
		}	   
	    // unset all user data end here			
		
		
		/*
		 * final order status redirect 
		 *
		 *
		 */
		
		switch ( $payment_flag_int ) {
			
			#  wallet 				
			case 0 : 
			
			( $this->current_order_payment_flag ) ? redirect('/agentsucess')  : redirect('/agentfail') ;
				
			break;
				
			#  ccavenue 
			case 2 :  
				
				
			( $this->current_order_payment_flag ) ? redirect('/agentsucess')  :redirect('/agentfail') ;		
			
				
			break;				
				
			#  wallet + ccavenue	
			case 3 : 
			
			( $this->current_order_payment_flag ) ? redirect('/agentsucess')  : redirect('/agentfail') ;
				
			break;				
				
		}
		
		
		
		


		//p( $agent_payment_param ) ; exit;
		


    }
	
	
	
    public function ccavRequestHandler(){
		
		$payment_flag_int = empty( $this->session->userdata( 'payment_flag' ) ) ? 0 :  array_sum( $this->session->userdata( 'payment_flag' ) ) ;
		//exit; 
		// 0 wallet , 1 wallet + ccavenue , 2 ccavenue
		
		/*
		if( $payment_flag_int == 0 || $payment_flag_int == 3 ){
			redirect('/agentamountsucess');
			exit;
		}
		*/
		

        $siteurl= base_url();
        $parameterbranch=array(
            'act_mode' =>'selectbranch',
            'weburl' =>$siteurl,
            'type'=>'web',

        );

        $path=api_url().'selectsiteurl/branch/format/json/';
        $data['branch']=curlpost($parameterbranch,$path);


        $parameterccgatway=array(
            'act_mode' =>'selectccavenue',
            'branchid' =>$data['branch']->branch_id,
            'type'=>'web',

        );

        $path=api_url().'ccavenue/ccavRequestHandler/format/json/';
        $data['ccavResponse'] = curlpost($parameterccgatway,$path);
		
        $data['userid']=($this->session->userdata['skiindia']>0) ? $this->session->userdata['skiindia'] : $this->session->userdata['skiindia_guest'];
        $parameter=array(
            'act_mode' =>'memusersesiid',
            'userid' =>($this->session->userdata['skiindia']>0) ? $this->session->userdata['skiindia'] : $this->session->userdata['skiindia_guest'],
            'type'=>'web',
        );

        $path=api_url().'userapi/usersesion/format/json/';
        $data['memuser']=curlpost($parameter,$path);

        $parameter = array('act_mode'=>'select_order',

            'orderid'=>$this->session->userdata('orderlastinsertid'),
            'type'=>'web',

        );
        $path1 = api_url()."Cart/getpayment_package/format/json/";
        $data['paymentpac']= curlpost($parameter,$path1);



        // Userlog
        $parameter4=array(
            'act_mode' =>'orderuserupdate',
            'user_id' =>$data['memuser']->user_id,
            'order_id'=>$data['paymentpac']->pacorderid,
            'title' =>'Mr',

            'billing_name' =>$data['memuser']->user_firstname."".$data['memuser']->user_lastname,
            'billing_email' =>$data['memuser']->user_emailid,
            'billing_tel'=>$data['memuser']->user_mobileno,
            'billing_address' =>$data['memuser']->user_Address,
            'billing_city'=>$data['memuser']->user_city,
            'billing_state' =>$data['memuser']->user_state,

            'billing_zip'=>$data['memuser']->user_zip,
            'billing_country' =>$data['memuser']->user_country,

            'type'=>'web',
        );

        $path=api_url().'userapi/userorderupdate/format/json/';
        $data['userregister']=curlpost($parameter4,$path);

        ?><html>
    <head>
        <title> Custom Form Kit </title>
    </head>
    <body>
    <center>


        <?php

        error_reporting(0);

        $merchant_data=87391;
        $working_key='E3A67D9E57171B5BAE2D2608A8CBF034';//Shared by CCAVENUES
        $access_code='AVJV70ED35BP45VJPB';//Shared by CCAVENUES

        $prepare_request_param = array(
            'title' => 'Mr',
            'billing_name' => 'sfsdfd',
            'billing_email' => 'nikhil.rane@gmail.com',
            'billing_tel' => 9810668829,
            'billing_address' => 'fhfghf',
            'billing_city' => 'fghfg',
            'billing_state' => 'hfhfg',
            'billing_zip' => 444444,
            'billing_country' => 'India',
            'tid' => 1513601010767,
            'merchant_id'=> 87391,
            'order_id'=> 'Snow_55',
            'order_idval' => 55,
            'user_id' => 232,
            'amount' => 1,
            'currency' => 'INR',
            'redirect_url' => 'http://115.124.98.243/~skiindia/snowworldnew/ordersucess',
            'cancel_url' => 'http://115.124.98.243/~skiindia/snowworldnew/orderfail',
            'language' => 'EN',
            'submit'=> 'cartses'
        );
        // echo '<pre>';
        // print_r( $prepare_request_param );

        // print_r( $_POST)
        // exit;

        foreach ($this->session->userdata('ccavenue_param') as $key => $value){
            $merchant_data.=$key.'='.urlencode($value).'&';
        }
        //pend($merchant_data);
        $encrypted_data=encrypt($merchant_data,$working_key); // Method for encrypting the data.

        ?>
        <form method="post" name="redirect" action="https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction">
            <?php
            echo "<input type=hidden name=encRequest value=$encrypted_data>";
            echo "<input type=hidden name=access_code value=$access_code>";
            ?>
        </form>

    <script language='javascript'>document.redirect.submit();</script>
    </body>
        </html>



        <?php



    }
}//end of class
?>