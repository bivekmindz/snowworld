<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cronejob extends MX_Controller {

   public function __construct() {
      $this->load->model("supper_admin");
   $this->load->library('session');
   $this->load->helpers('helpers/my_helper');
   }
  //Index page
public function timeslotadd(){

    $siteurl= base_url();
    $parameterbranch=array(
        'act_mode' =>'selectbranch',
        'weburl' =>$siteurl,
        'type'=>'web',

    );

    $path=api_url().'selectsiteurl/branch/format/json/';
    $data['branch']=curlpost($parameterbranch,$path);

if($data['branch']->branch_cronejob!='1') {

    $parametertimeslot = array(
        'act_mode' => 'selecttimeslot',
        'branchid' => $data['branch']->branch_id,
        'destinationType' => date("d/m/Y"),
        'timeslot_date' => '',

    );

  //  $path = api_url() . 'selecttimesloturl/timeslot/format/json/';
  //  $data['timeslotses'] = curlpost($parametertimeslot, $path);
    $data['timeslotses']=$this->supper_admin->call_procedure('proc_select_timeslot_v',$parametertimeslot);

    $arr = (array)$data['timeslotses'];

    $currentdate = date("Y-m-d");
    $date = strtotime(date("Y-m-d"));
    $nextDate = date("Y-m-d", strtotime("+".$data['branch']->branch_cronjobrunvalue." day", $date));
    $date1 = date_create($currentdate);
    $date2 = date_create($nextDate);
    $diff = date_diff($date1, $date2);
    $diff1 = $diff->format("%R%a");


    $diff1 = ltrim($diff1, "+");

    for ($i = 0; $i <= $diff1; $i++) {
        $nextDate = date("Y-m-d", strtotime("+$i day", $date));
        $nextDateval = explode("-", $nextDate);


        //$datedata = "" . $nextDateval[2] . "/" . $nextDateval[1] . "/" . $nextDateval[0] . "";



        foreach ($data['timeslotses'] as $count => $timeslot) {

            $nextDate = date("Y-m-d", strtotime("+$i day", $date));


$packagedata=explode(",",$timeslot->timeslot_packageid);





            $parameterselectdate = array(
                'act_mode' => 'selectdate',
                'nextDate' => $nextDate,
                'starttime' => $timeslot->timeslot_from,
                'branchid' => $data['branch']->branch_id,

                'dailyinventory_to' => '',
                'dailyinventory_seats' => '',
                'dailyinventory_status' => '',
                'dailyinventory_createdon' => '',
                'dailyinventory_modifiedon' => '',
                'dailyinventory_minfrom' => '',
                'dailyinventory_minto' => '',


                'type' => 'web',);

            $path = api_url() . 'Cronejob/timeslotselect/format/json/';
            $data['selectdate'] = curlpost($parameterselectdate, $path);
		

            if ($data['selectdate']->scalar == 'Something Went Wrong') {

                $parameterinsertdate = array(
                    'act_mode' => 'insertdate',
                    'nextDate' => $nextDate,
                    'starttime' => $timeslot->timeslot_from,
                    'branchid' => $data['branch']->branch_id,

                    'dailyinventory_to' => $timeslot->timeslot_to,
                    'dailyinventory_seats' => $timeslot->timeslot_seats,
                    'dailyinventory_status' => $timeslot->timeslot_status,
                    'dailyinventory_createdon' => $timeslot->timeslot_createdon,
                    'dailyinventory_modifiedon' => $timeslot->timeslot_modifiedon,
                    'dailyinventory_minfrom' => $timeslot->timeslot_minfrom,
                    'dailyinventory_minto' => $timeslot->timeslot_minto,

                    'type' => 'web',

                );

                $path = api_url() . 'Cronejob/timeslotselect/format/json/';
                $data['selectdate'] = curlpost($parameterinsertdate, $path);

				
				
			    $parameterupdatedate = array(
                    'act_mode' => 'updateaddonedate',
                    'nextDate' => $data['selectdate']->dd,
                    'starttime' =>  $timeslot->timeslot_addonsid,
                    'branchid' => '',

                    'dailyinventory_to' => '',
                    'dailyinventory_seats' => '',
                    'dailyinventory_status' => '',
                    'dailyinventory_createdon' => '',
                    'dailyinventory_modifiedon' => '',
                    'dailyinventory_minfrom' => '',
                    'dailyinventory_minto' => '',

                    'type' => 'web',

                );	
			
			 $path = api_url() . 'Cronejob/timeslotselect/format/json/';
                $data['selectdateaddons'] = curlpost($parameterupdatedate, $path);	
				
	
foreach($packagedata as $countd => $timeslotd)
{

     $parameterinsertdate = array(
                    'act_mode' => 'insertdatepackage',
                    'nextDate' => $data['selectdate']->dd,
                    'starttime' => $timeslotd,
                    'branchid' => '',

                    'dailyinventory_to' => '',
                    'dailyinventory_seats' => '',
                    'dailyinventory_status' => '',
                    'dailyinventory_createdon' => '',
                    'dailyinventory_modifiedon' => '',
                    'dailyinventory_minfrom' => '',
                    'dailyinventory_minto' => '',

                    'type' => 'web',

                );

                $path = api_url() . 'Cronejob/timeslotselect/format/json/';
                $data['selectdatepackage'] = curlpost($parameterinsertdate, $path);


}

               

//pend( $data['selectdate']->dd);


                echo "Insert";
            }


        }

    }

}
else{
    echo "Crone job Inactive";

}


}

}//end of class
?>