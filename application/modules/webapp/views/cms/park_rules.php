

 <div class="container margin_30">
      <h3>Safety Rules</h3>
      <div class="row">         
          <div class="col-md-12">
        <ol class="text-justify" style="margin-left: -15px; padding-bottom:10px;">
            <li>Snow is slippery in nature. So mind your footsteps.</li>
            <li>Socks are mandatory. Carry along or purchase from a counter available inside Ski India reception.</li>
            <li>For your safety and comfort, you are advised not to enter inside snow chamber in case of:
                <ul>
                    <li>Pregnancy</li>
                    <li>Suffering from any medical problems related to heart, cold or allergy</li>
                    <li>Feeling sick</li>
                    <li>Suffering from any condition that could be aggravated by this experience. While every reasonable effort has been made to ensure your safety and comfort, the management won’t be liable for any injuries caused.</li>
                </ul>
            <li>Ski India Management / Company or its Employees will not be responsible for loss, theft or any damage of any of your valuables.</li>
            <li>First Aid Service is available on request or emergency.</li>
            <li>Rights of Admission are reserved by the Management of Ski India.</li>
        </ol>        
            </div><!-- End col-md-12 -->
        </div><!-- End row -->
    
    <h3>Terms &amp; Conditions</h3>
      <div class="row">         
          <div class="col-md-12">
        <ol class="text-justify" style="margin-left: -15px;">
          <li>Admission ticket permits admission for one individual and needs to be retained at the respective time slot.</li>
          <li>Children's below 2 years are exempted from ticket charges.</li>
          <li>Total Duration: 1 Hour​ 30 mins​ ( Snow Room : ​1 hour​ + Reception : ​30 mins ​mins)</li>
      <li>Parka Jackets, Gloves, Hood and Boots will be provided with each admission.</li>
      <li>Tickets once purchased will not be RETURNED / REFUNDED back.</li>
      <li>Children's below 10 years and senior citizens need to be accompanied by guardian.</li>
      <li>All accessories are not of accurate sizes and are subject to availability.</li>
      <li>Eatables and beverages of any kind are not allowed into the snow area.</li>
    <li>The following articles are strictly prohibited: Eatables (Including Gutkha, Pan-Masala, Chewing Gum, Chocolate, Chips), Knife, and Cigarette, Lighter, Match-Box, Firearms and all kind of inflammable objects.</li>
    <li>Smoking is an offence & is strictly prohibited inside Ski India premises and will be fined according to Management’s discretion.</li>
    <li>All images & terminology used in all communication is for creative, descriptive and representation purpose only.</li>
    <li>Ticket package, rates and governing conditions are subject to change without notice.</li>
    <li>Management reserves the right to cancel any session due to unforeseen circumstances.</li>
    <li>The Management shall not be liable for any losses/damages arising with such suspension.</li>
    <li>No two offers can be clubbed together.</li>
    <li>Any purposeful act to damage the environment, surrounding or assets will be fined according to Management’s discretion.</li>
    <li>On the purchase of this ticket the holder confirms that he/she has read, understood and agreed to all the terms and conditions in the theme park and voluntarily assumes all the risks.</li>
    <li>Booking on this website authorizes Ski India to contact the customer, on the Phone Number provided, for any booking related communication.</li>
        </ol>       
            </div><!-- End col-md-12 -->
        </div><!-- End row -->
    </div><!-- End Container -->
    