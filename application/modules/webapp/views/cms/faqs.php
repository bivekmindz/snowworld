

  <div class="container margin_30">
      <div class="col-md-12 textcontainer">      
        <h1 class="headertext">FAQs</h1>
     <div class="panel-group" id="accordion">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
            1. What is the Ticket Price?<i class="indicator icon-angle-circled-up pull-right"></i></a>
                      </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                      <div class="panel-body">The ticket price is Rs.1000/- + Service Tax for total one hour duration which will include activities like Snow Play Area, I-slip, Multi- coaster, Icy Luge, Drift on Ice, Revolution, Knock a pod, Frozen Odyssey etc.</div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">2. Are infants allowed in Ski India?<i class="indicator icon-angle-circled-down pull-right"></i></a>
                      </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                      <div class="panel-body">Yes Kids of all age group are allowed at Ski India. Kids till age 2 are free and above 2 years would be charged at an actual price.</div>
                    </div>
                  </div>
          
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapsefour">3. How many hours we are allowed to stay inside the Park?<i class="indicator icon-angle-circled-down pull-right"></i></a>
                      </h4>
                    </div>
                    <div id="collapsefour" class="panel-collapse collapse">
                      <div class="panel-body">The total duration of per session is 1 hour 30 mins which is bifurcated as<br/>15 mins - Entry Waiting Area<br/>60 mins - Snow Room<br/>15 mins - Exit Waiting Area</div>
                    </div>
                  </div>
          
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapsefive">4. Can we carry our own pair of socks?<i class="indicator icon-angle-circled-down pull-right"></i></a>
                      </h4>
                    </div>
                    <div id="collapsefive" class="panel-collapse collapse">
                      <div class="panel-body">Socks are mandatory to stay in freezing temperature. So you can either carry a pair of socks along with you or can purchase from the socks unit we have it inside the Park.</div>
                    </div>
                  </div> 
          
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapsesix">5. Are senior citizens allowed in Ski India? If yes, what would be their ticket price?<i class="indicator icon-angle-circled-down pull-right"></i></a>
                      </h4>
                    </div>
                    <div id="collapsesix" class="panel-collapse collapse">
                      <div class="panel-body">Yes definitely all age groups are allowed at Ski India, even senior citizens. And so the ticket price remains same for all.</div>
                    </div>
                  </div>  
          
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseseven">6. Are Pregnant women's allowed in Ski India?<i class="indicator icon-angle-circled-down pull-right"></i></a>
                      </h4>
                    </div>
                    <div id="collapseseven" class="panel-collapse collapse">
                      <div class="panel-body">Snow is slippery by nature. So we regret to inform you that we do not allow entry of Pregnant women's at Ski India.</div>
                    </div>
                  </div> 
          
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseEight">7. Do we get all suitable gears to stay in minus degree temperature?<i class="indicator icon-angle-circled-down pull-right"></i></a>
                      </h4>
                    </div>
                    <div id="collapseEight" class="panel-collapse collapse">
                      <div class="panel-body">Your health is our concern. So we provide you with all the necessary accessories to stay and enjoy the freezing ambience. i.e. Parka Jackets, Safety shoes, Hand gloves, Hood etc.</div>
                    </div>
                  </div>
                </div>               
          
      </div> 
    </div>  