   
<table width="90%" style="line-height: 28px; font-family: sans-serif;" >
	<tr>
		<td>You missed the FUN at <?php echo $banner->bannerimage_top3 ?> !</td>
	</tr>

	<tr>
		<td>Greetings from <?php echo $banner->bannerimage_top3 ?>.</td>
	</tr>

	<tr>
		<td>Oops! You left your booking mid-way and missed out all the fun activities - Ice Skating, Snow Boarding, Snow Sledging & lots more. Grab the thrill, action & fun,<a href="<?php echo base_url() . 'paclanding?bookingid=' . $orderdisplaydataval->ticketid ?>">Click here</a> to continue to book your tickets now!!!</td>
	</tr>
	<tr>
		<td>Book now and join the fun at <?php echo $banner->bannerimage_top3 ?> !</td>
	</tr>

	<tr>
		<td>Yours sincerely,<br>
			<?php echo $banner->bannerimage_top3 ?> Team
		</td>
	</tr>
</table>