  
                                        <html>
                                        <head>
                                            <title>Booking</title>
                                            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                                        </head>
                                        <body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
                                        <!-- Save for Web Slices (Untitled-1) -->
                                        <table width="953" height="967" cellpadding="0" align="center" cellspacing="0"  style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;">
                                            <tr>
                                                <td colspan="3">
                                                    <table width="953">
                                                        <tr>
                                                            <td  width="350" valign="top" style="padding:40px 0px 0px 15px !important;">
                                                                <img src="assets/admin/images/<?php echo $banner->bannerimage_logo; ?>"  width="80px" alt="">
                                                            </td>
                                                            <td border="0" width="800" style="text-align:center;">
                                                                <table width="600" style="margin:20px 20px;line-height:25px;">
                                                                    <tr>
                                                                        <td style="color: #37ACE1;font-weight:700;padding:0px 0px;font-size:29px;text-align:center;">
																			
                                                                            <strong><?php echo $banner->bannerimage_top1; ?></strong>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding:8px 0px 5px;font-size:18px;font-weight:700;"><strong><?php echo $banner->bannerimage_top2; ?> </strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding:4px 0px;font-size:22px;font-weight:700;"><strong><?php echo $banner->bannerimage_top3; ?> </strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="color: #444;font-weight:700;padding:0px 0px 0px;font-size:22px;"><strong><?php echo $banner->bannerimage_top4; ?> </strong></td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="color: #444;font-weight:700;padding:0px 0px 0px;font-size:22px;"><strong><?php echo $banner->bannerimage_gstno; ?> </strong></td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="color: #37ACE1;font-weight:600;padding:15px 0px 0px;font-size:25px;"><strong>Booking Confirmation Details</strong></td>
                                                                    </tr>
                                                                </table>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </td>

                                            </tr>
                                            <tr><td  style="border-top:dashed 3px #37ace1;"><td></tr>
                                            <tr>
                                                <td colspan="3">
                                                    <table width="1000" cellpadding="0" cellspacing="0" style="margin:15px 0;">
                                                        <tr>
                                                            <td width="475">
                                                                <table style="margin:35px 35px 35px 35px;">
                                                                    <tr>
                                                                        <td style="line-height:25px;">
                                                                            <h3 style="font-size:23px;">Venue Details</h3>

                                                                        </td>
                                                                    </tr>
                                                                    <tr><td style="font-weight:normal;line-height:23px;font-size:18px;"><?php echo $branch->branch_add; ?></td></tr>
                                                                    <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Email :</span><?php echo $banner->bannerimage_branch_email; ?></td></tr>
                                                                    <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Phone :</span><?php echo $banner->bannerimage_branch_contact; ?></td></tr>

                                                                </table>
                                                            </td>
                                                            <td style="border-left:dashed 3px #37ace1;"></td>
                                                            <td width="475">
                                                                <table style="margin:35px 35px 35px 35px;">
                                                                    <tr>
                                                                        <td style="line-height:25px;">
                                                                            <h3 style="font-size:23px;">Guest Details</h3>

                                                                        </td>
                                                                    </tr>
                                                                    <tr><td style="font-weight:normal;line-height:23px;font-size:18px;"><?php  echo $orderdisplaydataval->billing_name; ?><br><?php echo $orderdisplaydataval->billing_address; ?></td></tr>
                                                                    <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Email :</span> <?php echo $orderdisplaydataval->billing_email; ?></td></tr>
                                                                    <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Phone :</span> <?php echo $orderdisplaydataval->billing_tel; ?></td></tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <table width="953" cellpadding="0" cellspacing="0" style="margin:15px 0px;">
                                                                    <tr>
                                                                        <td colspan="3" style="border-top:dashed 3px #37ace1;"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><table width="475" style="margin:15px 0;border-right:dashed 3px #37ace1;">

                                                                                <tr>
                                                                                    <td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Booking No.</td>
                                                                                    <td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Booking Date.</td></tr>
                                                                                <tr>
                                                                                    <td style="text-align:center;margin-top:5px;font-size:13px;"><?php  echo $orderdisplaydataval->ticketid; ?> </td>
                                                                                    <td style="text-align:center;margin-top:5px;font-size:13px;"><?php  echo $d4; ?></td></tr></table></td>
                                                                        <td width="3"></td>
                                                                        <td>
                                                                            <table width="475" style="margin:15px 0;">
                                                                                <tr>
                                                                                    <td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Visit Date.</td>
                                                                                    <td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Session Time</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="text-align:center;margin-top:5px;font-size:13px;"><?php  echo $d1; ?>.</td>
                                                                                    <td style="text-align:center;margin-top:5px;font-size:13px;"><?php echo $prepare_time_slot_from_date;  ?></td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3" style="border-top:dashed 3px #37ace1;"></td>
                                                                    </tr>
                                                                </table>
                                                            </td></tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 10px 50px; ">
                                                        <tr>
                                                            <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">
                                                                Items
                                                            </td>
                                                            <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">Qty
                                                            </td>
                                                            <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">Cost</td>
                                                            <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">Total</td>
                                                        </tr>
                                                        <?php 	//p($orderdisplaydataval);
														
                                                        foreach( $package_name_array as $ky => $val){
                                                            ?>
                                                            <tr>
                                                                <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><?php echo $val . ' Package'; ?> </td>
                                                                <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><?php echo $package_qty_array[ $ky ];  ?></td>
                                                                <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><?php echo $package_price_array[ $ky ]; ?></td>
                                                                <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px">
                                                                    <span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo ( $package_qty_array[ $ky ] * $package_price_array[ $ky ] ) ?> </td>
                                                            </tr>
                                                        <?php	}	?>

                                                        <?php
                                                        //p($orderaddonedisplaydata);

                                                        foreach ($orderaddonedisplaydata as $a => $b) {
															
                                                            if($b!='Something Went Wrong' ){

                                                                ?>
                                                                <tr>
																	<td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px">
																	<?php echo $b["addonename"]; ?>
                                                                    </td>
                                                                    <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px">
																		<?php echo $b["addonqty"]; ?>
																	</td>
                                                                    <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px">
                                                                        <?php echo ($b["addoneprice"]); ?>
                                                                    </td>
                                                                    <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px">
																		<span>
																		<i class="icon-rupee" aria-hidden="true"></i>
																		</span>
																		<?php echo ($b["addoneprice"] * $b["addonqty"]); ?>
                                                                    </td>
                                                                </tr>
                                                            <?php  } } ?>


                                                        <tr>
                                                            <td colspan="4">
                                                                <table width="760" cellpadding="0" cellspacing="0" style="border-top:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:10px 50px 10px 50px; ">
                                                                    <tr>
                                                                        <td width="380" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Price:</td>
                                                                        <td width="380" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo ( $package_total_price + $addon_total_price_with_quantity ); ?></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 10px 50px; ">
                                                        <tr><td width="430" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Discount Amount</td><td width="430" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo $orderdisplaydataval->promocodeprice; ?></td></tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 10px 50px; ">
                                                        <tr><td width="430" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Internet Handling Charges</td><td width="430" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo $orderdisplaydataval->internethandlingcharges	; ?></td></tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 30px 50px; ">
                                                        <tr><td width="430" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Total</td>
															<td width="430" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px">
																<span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo $total; ?>
															</td>
														</tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <table width="475" style="margin:15px 0px 15px 50px;">
                                                        <tr><td style="font-size:17px;font-weight:bold;color:#000;padding:4px 0px;"></td></tr>
                                                        <tr><td style="font-size:17px;font-weight:bold;color:#000;padding:4px 0px;">PAN No. : AAFFC9989B</td></tr>
                                                        <tr><td style="font-size:17px;font-weight:bold;color:#000;padding:4px 0px;">Encl. Terms and Conditions</td></tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>

                                        <br>
                                        <br>  <br>  <br>  <br>  <br>


                                        <table width="953" cellpadding="0" align="center" cellspacing="0"  style="margin-bottom:40px;border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;">
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td width="150" valign="top" style="padding:15px 0px 0px 15px;"><img src="assets/admin/images/<?php echo $banner->bannerimage_logo; ?>"  alt="" width="220"></td>
                                                            <td style="text-align:center;">
                                                                <table width="480" style="margin:20px 20px;line-height:25px;">
                                                                    <tr>
                                                                        <td width="653" style="color: #37ACE1;font-weight:700;padding:0px 0px;font-size:29px;text-align:center;">
                                                                            <strong><?php echo $banner->bannerimage_top1; ?></strong>

                                                                        </td>
                                                                    </tr>
                                                                    <tr><td style="padding:8px 0px 5px;font-size:18px;font-weight:700;"><strong><?php echo $banner->bannerimage_top2; ?></strong></td></tr>


                                                                    <tr><td style="color: #37ACE1;font-weight:600;padding:15px 0px 0px;font-size:25px;"><strong>Terms and Conditions</strong></td></tr>
                                                                </table>
                                                            </td>
                                                            <td width="150" valign="top" style="padding:15px 15px 0px 0px;"><img src="assets/admin/images/<?php echo $banner->bannerimage_logo; ?>"  alt="" width="220"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <table width="940" style="margin-left:50px;margin-top:10px;margin-bottom:80px;color:#000;font-size:16px;line-height:25px;">


                                                                    <?php  $i=1;foreach ($tearmsgatway as $key => $value11) {  ?>

                                                                        <tr>
                                                                            <td width="30" valign="top"><?php echo  $i; ?>.</td>
                                                                            <td width="900" valign="top"><?php echo $value11['term_name']; ?></td>
                                                                        </tr>

                                                                        <?php  $i++;} ?>

                                                                    <tr>
                                                                        <td width="30" valign="top">&nbsp;</td>
                                                                        <td width="900" valign="top">
                                                                            <table style="font-size:16px;line-height:25px;">

                                                                            </table>
                                                                        </td>
                                                                    </tr>


                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                            </tr>
                                        </table>

                                        </td>
                                        </tr>



                                        </table>
