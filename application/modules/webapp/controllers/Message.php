<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Message extends MX_Controller {

    public function __construct() {
        $this->load->model("supper_admin");
        $this->load->library('session');
    }



    public function payment(){
//Select Time slot 
        if($this->session->userdata('packageval')==''){
            redirect(base_url());
        } else {
//Select branch
            $siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);



            $parameterccgatway=array(
                'act_mode' =>'selectccavenue',
                'branchid' =>$data['branch']->branch_id,
                'type'=>'web',

            );

            $path=api_url().'ccavenue/ccavRequestHandler/format/json/';
            $data['ccavRequestHandler']=curlpost($parameterccgatway,$path);

//select banner images
            $parameterbanner=array(
                'act_mode' =>'selectbannerimages',
                'branchid' =>$data['branch']->branch_id,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/banner/format/json/';
            $data['banner']=curlpost($parameterbanner,$path);




            $parameter=array(
                'act_mode' =>'memusersesiid',
                'userid' =>$this->session->userdata['skiindia'],
                'type'=>'web',
            );

            $path=api_url().'userapi/usersesion/format/json/';
            $data['memuser']=curlpost($parameter,$path);




            $parametertimeslot=array(
                'act_mode' =>'selectsestimeslot',
                'branchid' =>$data['branch']->branch_id,
                'destinationType' =>$this->session->userdata('destinationType'),
                'type'=>'web',

            );

            $path=api_url().'selecttimesloturl/timeslotses/format/json/';
            $data['timeslotses']=curlpost($parametertimeslot,$path);


            $parameter = array('act_mode'=>'select_order',

                'orderid'=>$this->session->userdata('orderlastinsertid'),
                'type'=>'web',

            );
            $path1 = api_url()."Cart/getpayment_package/format/json/";
            $data['paymentpac']= curlpost($parameter,$path1);

            $this->load->view("helper/header");
            $this->load->view("helper/topbar",$data);
            $this->load->view("payment",$data);
            $this->load->view("helper/footer");
        }}

    public function paymentagent(){
//Select Time slot
        if($this->session->userdata('packageval')==''){
            redirect(base_url());
        } else {
//Select branch
            $siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);



//select banner images
            $parameterbanner=array(
                'act_mode' =>'selectbannerimages',
                'branchid' =>$data['branch']->branch_id,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/banner/format/json/';
            $data['banner']=curlpost($parameterbanner,$path);

            $parameterccgatway=array(
                'act_mode' =>'selectccavenue',
                'branchid' =>$data['branch']->branch_id,
                'type'=>'web',

            );

            $path=api_url().'ccavenue/ccavRequestHandler/format/json/';
            $data['ccavRequestHandler']=curlpost($parameterccgatway,$path);


            $parameter=array(
                'act_mode' =>'patrnusersesiid',
                'userid' =>$this->session->userdata['ppid'],
                'type'=>'web',
            );

            $path=api_url().'userapi/usersesion/format/json/';
            $data['memuser']=curlpost($parameter,$path);



            $parametertimeslot=array(
                'act_mode' =>'selectsestimeslot',
                'branchid' =>$data['branch']->branch_id,
                'destinationType' =>$this->session->userdata('destinationType'),
                'type'=>'web',

            );

            $path=api_url().'selecttimesloturl/timeslotses/format/json/';
            $data['timeslotses']=curlpost($parametertimeslot,$path);


            $parameter = array('act_mode'=>'select_order',

                'orderid'=>$this->session->userdata('orderlastinsertid'),
                'type'=>'web',

            );
            $path1 = api_url()."Cart/getpayment_package/format/json/";
            $data['paymentpac']= curlpost($parameter,$path1);

            $this->load->view("helper/header");
            $this->load->view("helper/topbar",$data);
            $this->load->view("paymentagent",$data);
            $this->load->view("helper/footer");
        }}

    public function adduser(){





        // Userlog
        $parameter4=array(
            'act_mode' =>'orderuserupdate',
            'user_id' =>$this->input->post('user_id'),
            'order_id'=>$this->input->post('order_idval'),
            'title' =>$this->input->post('title'),

            'billing_name' =>$this->input->post('billing_name'),
            'billing_email' =>$this->input->post('billing_email'),
            'billing_tel'=>$this->input->post('billing_tel'),
            'billing_address' =>$this->input->post('billing_address'),
            'billing_city'=>$this->input->post('billing_city'),
            'billing_state' =>$this->input->post('billing_state'),

            'billing_zip'=>$this->input->post('billing_zip'),
            'billing_country' =>$this->input->post('billing_country'),

            'type'=>'web',
        );

        $path=api_url().'userapi/userorderupdate/format/json/';
        $data['userregister']=curlpost($parameter4,$path);


    }

    public function addagentwalet(){





        // Userlog
        $parameter4=array(
            'act_mode' =>'orderagentupdate',
            'user_id' =>$this->input->post('user_id'),
            'order_id'=>$this->input->post('order_idval'),
            'title' =>$this->input->post('title'),

            'billing_name' =>$this->input->post('billing_namewallet'),
            'billing_email' =>$this->input->post('billing_emailwallet'),
            'billing_tel'=>$this->input->post('billing_telwallet'),
            'billing_address' =>$this->input->post('billing_addresswallet'),
            'billing_city'=>$this->input->post('billing_citywallet'),
            'billing_state' =>$this->input->post('billing_statewallet'),

            'billing_zip'=>$this->input->post('billing_zipwallet'),
            'billing_country' =>$this->input->post('billing_countrywallet'),

            'type'=>'web',
        );

        $path=api_url().'userapi/userorderupdate/format/json/';
        $data['userregister']=curlpost($parameter4,$path);


    }
    public function addagent(){





        // Userlog
        $parameter4=array(
            'act_mode' =>'orderagentupdate',
            'user_id' =>$this->input->post('user_id'),
            'order_id'=>$this->input->post('order_idval'),
            'title' =>$this->input->post('title'),

            'billing_name' =>$this->input->post('billing_name'),
            'billing_email' =>$this->input->post('billing_email'),
            'billing_tel'=>$this->input->post('billing_tel'),
            'billing_address' =>$this->input->post('billing_address'),
            'billing_city'=>$this->input->post('billing_city'),
            'billing_state' =>$this->input->post('billing_state'),

            'billing_zip'=>$this->input->post('billing_zip'),
            'billing_country' =>$this->input->post('billing_country'),

            'type'=>'web',
        );

        $path=api_url().'userapi/userorderupdate/format/json/';
        $data['userregister']=curlpost($parameter4,$path);


    }

  

    public function addguest(){





        $parameter6=array(
            'act_mode' =>'guestregister',
            'title' =>$this->input->post('title'),

            'billing_name' =>$this->input->post('billing_name'),
            'billing_email' =>$this->input->post('billing_email'),
            'billing_tel'=>$this->input->post('billing_tel'),
            'billing_address' =>$this->input->post('billing_address'),
            'billing_city'=>$this->input->post('billing_city'),
            'billing_state' =>$this->input->post('billing_state'),

            'billing_zip'=>$this->input->post('billing_zip'),
            'billing_country' =>$this->input->post('billing_country'),


            'type'=>'web',

        );
//pend($parameter6);

        $path=api_url().'userapi/guestregister/format/json/';
        $data['register']=curlpost($parameter6,$path);

//pend($data['register']);


        $arr = (array)$data['register'];

// Userlog
        $parameter4=array(
            'act_mode' =>'orderguestupdate',
            'user_id' =>$arr[0]['ls'],
            'order_id'=>$this->input->post('order_idval'),
            'title' =>$this->input->post('title'),

            'billing_name' =>$this->input->post('billing_name'),
            'billing_email' =>$this->input->post('billing_email'),
            'billing_tel'=>$this->input->post('billing_tel'),
            'billing_address' =>$this->input->post('billing_address'),
            'billing_city'=>$this->input->post('billing_city'),
            'billing_state' =>$this->input->post('billing_state'),

            'billing_zip'=>$this->input->post('billing_zip'),
            'billing_country' =>$this->input->post('billing_country'),

            'type'=>'web',
        );
        //p($parameter4);

        $path=api_url().'userapi/guestorderupdate/format/json/';
        $data['userlog']=curlpost($parameter4,$path);

//pend($data['userlog']);




        //$data = array(
        //    'skiindia' => $arr[0]['ls'],
        //        );

        //    $this->session->set_userdata($data);

        //  $data['skiindases'] = ($arr[0]['ls']);





    }

}//end of class
?>